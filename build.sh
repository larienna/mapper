#!/bin/bash

#create directory if not exist
mkdir -p build

#remove any files previously created
cd build && rm -r *

#build the project with cmake
cmake ..

#build all targets
make

#copy executables (maybe to replace with install instructions)
cp src/demo/demo ..
