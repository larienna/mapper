# Maze Projection Room (MaPpeR)

Programmer: Eric Pietrocupo

License: Apache 2.0

A simple 3D maze engine used by first person RPG that allow illogical constructions like one way doors and walls. This project has been extracted from the Wizardry Legacy project and is rebuilt entirely to be reusable in other projects. 

The programming language will only be plain C. This project requires Allegro 4.4 as dependencies since it has the 2D and 3D drawing features. Many libraries like SDL does not have 3D texture projections and camera matrix computation. 

The engine also comes with an editor to draw maps and save/load them in binary format. Since I am now going to use real projection matrix, animations could be supported but it's not a priority. Some maze events will handled by mapper, if it does not imply displaying text and dialogs on the screen (ex: teleport, rotator, etc). Else it's up to the programmer to use the xyz coordinates of the player to determine how to resolve the event in the current position. 

## How does it works

The principle is simple. 

1. A maze projection room is created in memory. This room contains all possible polygon in 3D coordinates that will ever be used.

2. The maze layout will make certain polygons appear in order to reproduce the map layout. Unused polygons are kept hidden.

3. The camera matrix projection is made to calculate the 2D coordinates of the 3D polygons.

4. The polygons are drawn in a buffer that could be copied on the screen.

Since a real camera will now be used it is possible to have intermediary frames allowing movement animations.

## Limitations

Allegro is not a state of the art 3D engine, so the number of polygons will be relatively low. It will only be used for walls and floors so there will not be any 3D model support in the maze. If maze objects are added to the engine, it will be simple pictures always facing the player (Like in doom, heretic, hexen, etc). Maze objects are currently not a priority.

In order to support one way wall, I cannot draw real thick walls. Therefore a patch is applied at the end of walls to simulate thickness.

## How to build the demo

There is currently a Cmake build script, so if you have a C compiler, git and Cmake, you can run this script that will build the software

```./build.sh```

Else the following commands after a successful clone can do the same thing.

```
mkdir build
cd build
cmake ..
make
cp src/demo/demo ..
```

Then you can launch the demo from the root of the project by simply typing:

```demo```

It will require the file "stonewall.bmp" in the same folder to run for now.

## Making some software

The static library file is located in 

```/build/src/lib/libmpr.a```

The installation has not been configured yet. Simply include all the .h you can find in 

```/src/lib```

except: "mapper-private.h". A master .h file will eventually be made "to rule them all". Moft of the documentation is in mapper.h and other .h files.

The build process is not perfect, a lot of work is still required.
