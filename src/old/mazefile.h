/**
   mazefile.h

   @author Eric Pietrocupo
   @since  March 2021
   @license GNU General Public License

   This module is used to save load and store the content of the maze. It will be used by
   the editor and the the maze drawing routines. The maze content is stored in a global
   variable with a fixed size.

   The file format is a custom binary format which is composed of a small header and a
   dumping of the 3D array of structure. The main challenge is basically to save and load the
   tiles in the right order.

*/

#ifndef MAZEFILE_H_INCLUDED
#define MAZEFILE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/** Those are the constant that parametrise the size of the maze. The size is fixed and the
maze is allocated statically at game start. Size is used for width and height.
*/

#define MAZE_SIZE             100
#define MAZE_DEPTH             20


extern s_mazetile mazetile [ MAZE_DEPTH ] [ MAZE_SIZE ] [ MAZE_SIZE ];

/** There is a small header in the binary maze file, it is mandatory, but the information could
almost be ignored if for example, you use the maximum size of the maze all the time. It's there
for expansion and compatibility reasons
   */

typedef struct s_mazefile_header
{
   int version; // maze version. changes how the tiles are read
   int width; // Width and height of the maze (square). Placed there in case of variable size.
   int depth; // Number of levels in the maze. Placed there in case of variable size.
   int reserved; // standby for future use.
   //int f_sky;
   //int f_rclevel; //TODO: Maybe above rc level, solid becomes sky. You still die from a teleport.
}s_mazefile_header;

/** Finally, there is 2 simple routines that load and save the maze information from and to a
binary mazefile.
*/

bool load_mazefile ( const char* filename );
bool save_mazefile ( const char* filename );

#ifdef __cplusplus
}
#endif


#endif // MAZEFILE_H_INCLUDED
