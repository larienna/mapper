/**
   Wizardry Legacy
   texturepalette.c

   @author: Eric Pietrocupo
   @date: February 15th, 2021
   @license: GNU general public license
*/

#ifndef TEXTUREPALETTE_C_INCLUDED
#define TEXTUREPALETTE_C_INCLUDED

/* Implementation notes: I decided for this small module to precede all private
   material ( methods, globals, structures ) with an underscore. That could make
   it easier to find the code, altough, there are some functions in code::blocks
   do to so. Else it could help find internal sudivising methods.
*/



#include <allegro.h>
#include <stdbool.h>
#include <stdio.h>
#include <texturepalette.h>
#include <assets.h>


/*----------------------------------------------------------------------*/
/*-                     Private Constants                              -*/
/*----------------------------------------------------------------------*/

//Use a linear array for easier initialisation and storage in database.
#define _INDEX_WALL       0
#define _INDEX_FLOOR      1
#define _INDEX_CEILING    2
                            // 3 is reserved
#define _INDEX_DOOR       4  // 4-7
#define _INDEX_GRID       8  // 8-11
#define _INDEX_MWALL      12  // 12-27
#define _INDEX_MFLOOR     28 // 28-44
#define _INDEX_FOBJECT    44 // 44-59
#define _INDEX_COBJECT    60 //60-75


/*----------------------------------------------------------------------*/
/*-                    Private Global Variables                        -*/
/*----------------------------------------------------------------------*/

//array that contains a bitmap reference for all textures of all palettes
BITMAP* _texturepal [TEXTUREPAL_NB_PALETTE][TEXTUREPAL_NB_TEXTURE];

//index of the currently active palette. Stored internally to avoid passing in parameter.
int _activepal;

/*---------------------------------------------------------------------*/
/*-                   Private Method                                  -*/
/*---------------------------------------------------------------------*/

/*This is a debugging palette to be able to test various feature of the game
without having to load/save from the database. Only palette 0 is initialised*/
void _init_debug_palette ( void )
{  _texturepal [0][_INDEX_WALL] = asset_texture("WALL_STONEROUGH_ICE_BMP");
   //_texturepal [0][_INDEX_FLOOR] = asset_texture("FLOOR_TILE_LATEIRREGULAR_ICE_BMP");
   //_texturepal [0][_INDEX_FLOOR] = asset_texture("FLOOR_PAVEMENT_STONE5_2_BMP");
   _texturepal [0][_INDEX_CEILING] = asset_texture("SURFACE_BLUE_STAINED_BMP");
   _texturepal [0][_INDEX_DOOR] = asset_texture("DOOR_TEST_A_WIZ_BMP");
   _texturepal [0][_INDEX_GRID] = asset_texture("DOOR_TEST_A_WIZ_BMP");
   //TODO: set a better grid when some work will be done.
   _texturepal [0][_INDEX_MWALL] = asset_texture("WALL_STONEROUGH_ICEANIMAL_BMP");
   _texturepal [0][_INDEX_MWALL+1] = asset_texture("WALL_STONEROUGH_ICE_PATCH_BMP");

   //add masked floor when anything will be ready.
}

/*---------------------------------------------------------------------*/
/*-                   Public Method                                   -*/
/*---------------------------------------------------------------------*/


void init_texpal ( void )
{  for ( int j = 0 ; j < TEXTUREPAL_NB_PALETTE; j++ )
   {  for ( int i = 0 ; i < TEXTUREPAL_NB_TEXTURE; i++)
      {  _texturepal [j][i] = asset_default_bitmap256();
      }
   }

   //TODO: TEMPCODE fpor DEBUG, remove once working.
   _init_debug_palette();
}

void load_texpal ( void )
{  printf ("DEBUG: load_texpal: method un-implemented\n");
   //TODO: create a new table in the database, but this time it will have no patch.
}

void save_texpal ( void )
{  printf ("DEBUG: save_texpal: methos un-implemented\n");
}


int get_texpal_id ( void )
{  return _activepal;
}

void set_texpal_id ( int palette_id )
{  if ( palette_id >= 0 && palette_id < TEXTUREPAL_NB_PALETTE ) _activepal = palette_id;
}


BITMAP *texpal_wall ()
{  return _texturepal [ _activepal] [_INDEX_WALL];
}

BITMAP *texpal_floor ()
{  return _texturepal [ _activepal] [_INDEX_FLOOR];
}

BITMAP *texpal_ceiling ()
{  return _texturepal [ _activepal] [_INDEX_CEILING];
}

BITMAP *texpal_door ( int index )
{  if ( index >= 0 && index < TEXTUREPAL_NB_DOOR )
   {  return _texturepal [_activepal][_INDEX_DOOR + index];
   }
   else return asset_default_bitmap256();
}

BITMAP *texpal_grid ( int index )
{  if ( index >= 0 && index < TEXTUREPAL_NB_GRID )
   {  return _texturepal [_activepal][_INDEX_GRID + index];
   }
   else return asset_default_bitmap256();
}

BITMAP *texpal_maskwall (int index)
{  if ( index >= 0 && index < TEXTUREPAL_NB_MASKEDWALL )
   {  return _texturepal [_activepal][_INDEX_MWALL + index];
   }
   else return asset_default_bitmap256();
}

BITMAP *texpal_maskfloor (int index)
{  if ( index >= 0 && index < TEXTUREPAL_NB_MASKEDFLOOR )
   {  return _texturepal [_activepal][_INDEX_MFLOOR + index];
   }
   else return asset_default_bitmap256();
}

BITMAP *texpal_ceilingobject (int index)
{  if ( index >= 0 && index < TEXTUREPAL_NB_CEILINGOBJECT )
   {  return _texturepal [_activepal][_INDEX_COBJECT + index];
   }
   else return asset_default_bitmap256();
}

BITMAP *texpal_floorobject (int index)
{  if ( index >= 0 && index < TEXTUREPAL_NB_FLOOROBJECT )
   {  return _texturepal [_activepal][_INDEX_FOBJECT + index];
   }
   else return asset_default_bitmap256();
}




#endif // TEXTUREPALETTE_C_INCLUDED
