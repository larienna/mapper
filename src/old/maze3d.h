/**
   maze3d.h

   @author Eric Pietrocupo
   @since  March 2021
   @license GNU General Public License

   This module is used to draw the maze with a 3D projection. Unfortunately, it is not
   a real 3D engine, walls are prepositions and drawn in a specific order.
*/

#ifndef MAZE_H_INCLUDED
#define MAZE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/** First, there will be a structure to hold the party position which is in a certain way
the coordinates of the camera used for the projection
*/

typedef struct s_camera
{
   int x;
   int y;
   int z;
   int facing;
}s_camera;

//TODO: Move elsewhere. Create the projection room independent of maze coordinates.

void draw_3dmaze ( s_camera *camera, bool demo );



#ifdef __cplusplus
}
#endif


#endif // MAZE_H_INCLUDED
