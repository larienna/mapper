/**
   mazemap.h

   @author Eric Pietrocupo
   @since  March 2021
   @license GNU General Public License

   This module is designed to drap maps of the maze on the screen. It is mainly used by the
   editor and by some spells in order to avoid duplicate code.

*/

#ifndef MAZEMAP_H_INCLUDED
#define MAZEMAP_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#define TILE_SIZE       16
#define ADJUST_GRID     1

/** In order to make parameter passing easier for external and internal usage, a structure will
    be used to hold the format of the maze to be drawn. It comes with an initialisation method;
*/

typedef struct s_viewport
{  //int width;    // Width in tile to be drawn
   //int height;   // height in tiles
   int size;     //size of the viewport
   int x_pos;    // X screen pixel position
   int y_pos;    // Y screen pixel position
   int xscroll;  // X offset of the map viewport
   int yscroll;  // Y offset of the map viewport
   bool special; //draw special flags
   bool filling; //draw filling
   bool events;  //draw event icons
   bool objects; //draw presence of objects
   bool masked;  //draw presence of masked texture
}s_viewport;

void init_viewport ( s_viewport *viewport, int size, int xpos, int ypos );

/** The main drawing method is draw maps which takes many parameter. All the parameters
are store in a structure for more convenience.
*/

void draw_map ( s_cursor *cursor, s_viewport *viewport );

/** The following method will draw a less detailed map, but will draw the entire map
screen. Again, it gives a better idea of the whole world.
*/

void draw_full_tiny_map ( void );

#ifdef __cplusplus
}
#endif


#endif // MAZEMAP_H_INCLUDED
