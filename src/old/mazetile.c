/**
*
* @author Eric Pietrocupo
* @date   February 11th, 2018
* @license Apache 2 license
*
* In order to avoid bugs and unwanted issues with the maze tile system, I am using
* a C style module to centralise all read and write operations at the same place to
* make sure they are beign done properly. In order to optimise space and speed,
* classes cannot be used, so the tile structure is passed to each call.
*/
#include <stdbool.h>
#include <mazetile.h>


s_mazetile new_mazetile_empty ( void )
{  s_mazetile tile;
   set_mazetile_empty( &tile );

   return tile;
}

void set_mazetile_empty ( s_mazetile *tile )
{  tile->event = 0;
   tile->special = 0;
   tile->solid = 0;
   tile->objectpic = 0;
   tile->wobjpalette = 0;
   tile->objposition = 0;
   tile->masked = 0;
   tile->maskedposition = 0;
}

//-------------------------- Byte 1 ----------------------------

void set_mazetile_wall ( s_mazetile *tile, int side, int type )
{  int i;
   unsigned char mask = MAZETILE_SOLID_WEST_MASK;
   //shift the type left a number of times according to the direction
   for ( i = 3 ; i > side; i--)
   {  type = type << 2;
      mask = mask << 2;
   }

   //Clear out the area to place the new value
   tile->solid = tile->solid & ~mask;

   //assign the new value
   tile->solid = tile->solid | type;

}

bool is_mazetile_wall_oftype ( s_mazetile tile, int side, int type )
{  return ( get_mazetile_wall ( tile, side ) == type );
}


unsigned char get_mazetile_wall ( s_mazetile tile, int side )
{  int i;
   //shift the solid right a number of times according to the direction
   for ( i = 3 ; i > side; i--)
   {  tile.solid = tile.solid >> 2;
   }
   return ( tile.solid & MAZETILE_SOLID_WEST_MASK );
}

//-------------------------- Byte 2 ----------------------------

void set_mazetile_event (s_mazetile *tile, unsigned char eventid )
{  tile->event = eventid;
}

unsigned char get_mazetile_event ( s_mazetile tile )
{  return ( tile.event );
}


//-------------------------- Byte 3 ----------------------------

void set_mazetile_special ( s_mazetile *tile, int flag )
{  tile->special = tile->special | flag ;
}

void unset_mazetile_special ( s_mazetile *tile, int flag )
{  tile->special = tile->special & ~flag;
}

bool is_mazetile_special ( s_mazetile tile, int flag )
{  return ( (tile.special & flag) == flag );
}

void set_mazetile_filling ( s_mazetile *tile, int filling )
{  unsigned char special_bits = tile->special & MAZETILE_SPECIAL_MASK;
   tile->special = special_bits + filling;
}

int get_mazetile_filling ( s_mazetile tile )
{  return (tile.special & MAZETILE_FILLING_MASK );
}


//-------------------------- Byte 4 ----------------------------

//-------------------------- Byte 5 ----------------------------

void set_mazetile_palette ( s_mazetile *tile, int paletteid )
{  //Zero the palette value first, then add the new palette number
   tile->wobjpalette = tile->wobjpalette & MAZETILE_WALLOBJIMG_MASK;
   tile->wobjpalette = tile->wobjpalette + paletteid;
}

int get_mazetile_palette ( s_mazetile tile )
{  return ( tile.wobjpalette & MAZETILE_PALETTE_MASK );
}

//-------------------------- Byte 6 ----------------------------

//-------------------------- Byte 7 ----------------------------

int get_mazetile_maskedfloortex (s_mazetile tile)
{  return ( (tile.masked & MAZETILE_MASKTEX_FLOOR_MASK ) >> MAZETILE_MASKTEX_FLOOR_SHIFT );
}

void set_mazetile_maskedfloortex (s_mazetile *tile, int textureid )
{  tile->masked = ( tile->masked & ~MAZETILE_MASKTEX_FLOOR_MASK )
      + ( textureid << MAZETILE_MASKTEX_FLOOR_SHIFT );
}

int get_mazetile_maskedwalltex (s_mazetile tile)
{  return ( tile.masked & MAZETILE_MASKTEX_WALL_MASK );
}

void set_mazetile_maskedwalltex (s_mazetile *tile, int textureid )
{  tile->masked = ( tile->masked & ~MAZETILE_MASKTEX_WALL_MASK ) + textureid;
}


//-------------------------- Byte 8 ----------------------------
void set_mazetile_maskedposition (s_mazetile *tile, int position )
{  tile->maskedposition = (tile->maskedposition | position );
}

bool is_mazetile_maskedposition (s_mazetile tile, int position )
{  return ( (tile.maskedposition & position) == position );
}

int get_mazetile_gridtexid ( s_mazetile tile)
{  return ( tile.maskedposition & MAZETILE_MASKPOS_GRID_MASK);
}
