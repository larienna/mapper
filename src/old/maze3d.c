/**
   maze3d.h

   @author Eric Pietrocupo
   @since  March 2021
   @license GNU General Public License
*/

#include <allegro.h>
#include <texturepalette.h>
#include <stdbool.h>
#include <maze3d.h>
#include <mazetile.h>
#include <mazefile.h>
#include <video.h>

//_____________________________________________________________________________//
//                            Private constants                                //
//_____________________________________________________________________________//

#define NB_VERTEX 4

//                          Center > -----------+
//                            Left > --------+  |   +--------- < Right
//                        Far left > -----+  |  |   |  +----- < Far Right
//                       Away left > --+  |  |  |   |  |  +-- < Away Right
//                                     |  |  |  |   |  |  |
// s_Maze_wall id       Front 3 ->     -- -- -- -- -- -- --
//                      Side  3 ->       |  |  |  |  |  |
//                      Front 2 ->        -- -- -- -- --
//                      Side  2 ->          |  |  |  |
//                      Front 1 ->           -- -- --
//                      Side  1 ->             |  |

//TODO: Away walls should never be drawnsince outside light range, probably remove them
#define Maze_WALL_AWAY_LEFT_FRONT_3       0
#define Maze_WALL_AWAY_RIGHT_FRONT_3      1
#define Maze_WALL_FAR_LEFT_FRONT_3        2
#define Maze_WALL_FAR_RIGHT_FRONT_3       3
#define Maze_WALL_LEFT_FRONT_3            4
#define Maze_WALL_RIGHT_FRONT_3           5
#define Maze_WALL_CENTER_FRONT_3          6

#define Maze_WALL_AWAY_LEFT_SIDE_3        7
#define Maze_WALL_AWAY_RIGHT_SIDE_3       8
#define Maze_WALL_FAR_LEFT_SIDE_3         9
#define Maze_WALL_FAR_RIGHT_SIDE_3        10
#define Maze_WALL_LEFT_SIDE_3             11
#define Maze_WALL_RIGHT_SIDE_3            12

#define Maze_WALL_FAR_LEFT_FRONT_2        13
#define Maze_WALL_FAR_RIGHT_FRONT_2       14
#define Maze_WALL_LEFT_FRONT_2            15
#define Maze_WALL_RIGHT_FRONT_2           16
#define Maze_WALL_CENTER_FRONT_2          17

#define Maze_WALL_FAR_LEFT_SIDE_2         18
#define Maze_WALL_FAR_RIGHT_SIDE_2        19
#define Maze_WALL_LEFT_SIDE_2             20
#define Maze_WALL_RIGHT_SIDE_2            21

#define Maze_WALL_LEFT_FRONT_1            22
#define Maze_WALL_RIGHT_FRONT_1           23
#define Maze_WALL_CENTER_FRONT_1          24

#define Maze_WALL_LEFT_SIDE_1             25
#define Maze_WALL_RIGHT_SIDE_1            26

// floor constant

#define Maze_FLOORCEILING_FAR_LEFT_3              0
#define Maze_FLOORCEILING_FAR_RIGHT_3             1
#define Maze_FLOORCEILING_LEFT_3                  2
#define Maze_FLOORCEILING_RIGHT_3                 3
#define Maze_FLOORCEILING_CENTER_3                4

#define Maze_FLOORCEILING_FAR_LEFT_2              5
#define Maze_FLOORCEILING_FAR_RIGHT_2             6
#define Maze_FLOORCEILING_LEFT_2                  7
#define Maze_FLOORCEILING_RIGHT_2                 8
#define Maze_FLOORCEILING_CENTER_2                9

#define Maze_FLOORCEILING_LEFT_1                 10
#define Maze_FLOORCEILING_RIGHT_1                11
#define Maze_FLOORCEILING_CENTER_1               12

//_____________________________________________________________________________//
//                            Private Globals                                  //
//_____________________________________________________________________________//

const int DARKNESS [4] = { 255, 140, 70, 0 };

const V3D FLOOR_POLYGON [13][4] =
{  //NOTE: The << 16 seems to be used to convert integers to fix.
   //Since you can probably not call the function before startup.
   //TODO: see if more simple to use the V3D_f structure that uses float instead.
    // Maze_FLOOR_FAR_LEFT_3
      { { -140<<16, 320<<16, 3, 0,       TEXTURE_SIZE<<16,          56/*104*/ },
      {   30<<16, 290<<16,   4, 0,       0,                         7/*37*/ },
      {  146<<16, 290<<16,   4, TEXTURE_SIZE<<16, 0,                47/*70*/ },
      {   44<<16, 320<<16,   3, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 110/*130*/} },
   // Maze_FLOOR_FAR_RIGHT_3
      { { 596<<16, 320<<16, 3, 0,       TEXTURE_SIZE<<16,           110 /*130*/ },
      { 494<<16, 290<<16,   4, 0,       0,                          47/*70*/ },
      { 610<<16, 290<<16,   4, TEXTURE_SIZE<<16, 0,                 7/*37*/ },
      { 780<<16, 320<<16,   3, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16,  56/*104*/ } },
   // Maze_FLOOR_LEFT_3
      { {  44<<16, 320<<16, 3, 0,       TEXTURE_SIZE<<16,           110/*130*/ },
      { 146<<16, 290<<16,   4, 0,       0,                          47/*70*/ },
      { 262<<16, 290<<16,   4, TEXTURE_SIZE<<16, 0,                 65/*90*/ },
      { 228<<16, 320<<16,   3, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16,  146/*156*/ } },
   // Maze_FLOOR_RIGHT_3
      { { 412<<16, 320<<16, 3, 0,       TEXTURE_SIZE<<16,           146/*156*/ },
      { 378<<16, 290<<16,   4, 0,       0,                          65/*90*/ },
      { 494<<16, 290<<16,   4, TEXTURE_SIZE<<16, 0,                 47/*70*/ },
      { 596<<16, 320<<16,   3, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16,  110/*130*/ } },
   // Maze_FLOOR_CENTER_3
      { { 228<<16, 320<<16, 3, 0,       TEXTURE_SIZE<<16,           146/*156*/ },
      { 262<<16, 290<<16,   4, 0,       0,                          65/*90*/ },
      { 378<<16, 290<<16,   4, TEXTURE_SIZE<<16, 0,                 65/*90*/ },
      { 412<<16, 320<<16,   3, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16,  146/*156*/ } },
   // Maze_FLOOR_FAR_LEFT_2
      { { -600<<16, 400<<16, 2, 0,       TEXTURE_SIZE<<16,          101/*158*/ },
      { -140<<16, 320<<16,   3, 0,       0,                         56/*104*/ },
      {   44<<16, 320<<16,   3, TEXTURE_SIZE<<16, 0,                110/*130*/ },
      { -232<<16, 400<<16,   2, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 164/*190*/ } },
   // Maze_FLOOR_FAR_RIGHT_2
      { {  872<<16, 400<<16, 2, 0,       TEXTURE_SIZE<<16,          164/*190*/ },
      {  596<<16, 320<<16,   3, 0,       0,                         110/*130*/ },
      {  780<<16, 320<<16,   3, TEXTURE_SIZE<<16, 0,                56/*104*/ },
      { 1240<<16, 400<<16,   2, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 101/*158*/ } },
   // Maze_FLOOR_LEFT_2
      { { -232<<16, 400<<16, 2, 0,       TEXTURE_SIZE<<16,          164/*190*/ },
      {  44<<16,  320<<16,   3, 0,       0,                         110/*130*/ },
      {  228<<16, 320<<16,   3, TEXTURE_SIZE<<16, 0,                146/*156*/ },
      {  136<<16, 400<<16,   2, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 205/*222*/ } },
   // Maze_FLOOR_RIGHT_2
      { { 504<<16, 400<<16, 2, 0,       TEXTURE_SIZE<<16,           209/*222*/ },
      { 412<<16, 320<<16,   3, 0,       0,                          146/*156*/ },
      { 596<<16, 320<<16,   3, TEXTURE_SIZE<<16, 0,                 110/*130*/ },
      { 872<<16, 400<<16,   2, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16,  164/*190*/ } },
   // Maze_FLOOR_CENTER_2
      { { 136<<16, 400<<16, 2, 0,       TEXTURE_SIZE<<16,           209/*222*/ },
      { 228<<16, 320<<16,   3, 0,       0,                          146/*156*/ },
      { 412<<16, 320<<16,   3, TEXTURE_SIZE<<16, 0,                 146/*156*/ },
      { 504<<16, 400<<16,   2, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16,  209/*222*/ } },
   // Maze_FLOOR_LEFT_1
      { { -680<<16, 520<<16, 1, 0,       TEXTURE_SIZE<<16,          182/*217*/ },
      {  -232<<16, 400<<16,  2, 0,       0,                         164/*190*/ },
      {  136<<16,  400<<16,  2, TEXTURE_SIZE<<16, 0,                209/*222*/ },
      {  -10<<16,  520<<16,  1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 } },
   // Maze_FLOOR_RIGHT_1
      { {  650<<16, 520<<16, 1, 0,       TEXTURE_SIZE<<16,          255 },
      {  504<<16, 400<<16,   2, 0,       0,                         209/*222*/ },
      {  872<<16, 400<<16,   2, TEXTURE_SIZE<<16, 0,                164/*190*/ },
      { 1320<<16, 520<<16,   1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 182/*217*/ } },
   // Maze_FLOOR_CENTER_1
      { {  -10<<16, 520<<16, 1, 0,       TEXTURE_SIZE<<16,          255 },
      {  136<<16, 400<<16,   2, 0,       0,                         209/*222*/ },
      {  504<<16, 400<<16,   2, TEXTURE_SIZE<<16, 0,                209/*222*/ },
      {  650<<16, 520<<16,   1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 } }
};



//_____________________________________________________________________________//
//                            Private methods                                  //
//_____________________________________________________________________________//

int _get_light_level ( s_camera *camera, bool demo )
{  int fog = 0;
   int light_level = 0;

   s_mazetile tmptile = mazetile [ camera->z ] [ camera->y ] [ camera->x ] ;

   if ( get_mazetile_filling (tmptile) == MAZETILE_FILLING_FOG ) fog = 1;

   if ( get_mazetile_filling (tmptile) != MAZETILE_FILLING_DARKNESS )
   {
      //TODO: uncomment when active effect will be available
      /*if ( party.spell_duration ( Party_SPELL_LIGHT ) > 0 )
         light ( Maze_LIGHT_LEVEL_3 - fog );
      else*/
      if ( demo == false) light_level =  2 - fog;
      else light_level = 3 - fog;
   }
   return light_level;
}

void _draw_floorceiling_rotate_texture ( V3D *vertex, int nb_rotation )
{  int texu;
   int texv;

   for ( int j = nb_rotation ; j > 0 ; j-- )
   {
      texu = vertex [ 0 ] . u;
      texv = vertex [ 0 ] . v;
      for ( int i = 0 ; i < 3 ; i++ )
      {  vertex [ i ] . u = vertex [ i + 1 ] . u;
         vertex [ i ] . v = vertex [ i + 1 ] . v;
      }
      vertex [ 3 ] . u = texu;
      vertex [ 3 ] . v = texv;
   }
}

//TODO: Maybe include draw_masked_floor which has the same logic, or combine in same function.
void _draw_floorceiling ( const V3D *vertex, BITMAP *texture, int nb_rotation )
{
   V3D tmpvertex [ 4 ];

   /*int i;
   int j;

   fixed texu;
   fixed texv;
   int tmpval;
   int tmpx;
   int tmpy;*/
  // int tmpcolor;
   //int swpcolor = makecol ( 255, 255, 255 );
   //int swpcolor = makecol ( 125, 125, 125 );
   //printf ("Debug: draw floor: before light\n");
   for (int i = 0 ; i < NB_VERTEX ; i++ )
   {
      tmpvertex [ i ] = vertex [ i ];
      //tmpvertex [i].c = makecol24 (150,150,150);
      //tmpvertex [i].c -= 64; //TODO: 64 seems relatively nice for shading
      // if ( tmpvertex [i].c < 0 ) tmpvertex [i].c = 0;

      //int tmpval = tmpvertex [i].c;
      /*tmpval = ( tmpvertex [ i ] . c ) - 64;//32;
      if ( tmpval < 0 ) tmpval = 0;*/
      //tmpvertex [i].c = makecol (tmpval, tmpval, tmpval);

      //NOTE: it seems the color value a temporary value used to create the real value
      //as makecol24 cannot be called before init is called.
      //TODO: Could pre-render those values in the constant structure, less confusing code.
      //Would only work if there is no dynamic lighting.

      //TODO: The documentation say that the blender mode needs to be enabled, but I don't see any
      //pieces of code that does that. set_trans_blender does nothing.

      //TODO: Could remove dynamic lighting, not used in any of the wizardrys. Mainly effective
      //when using lighted drawing. Else allow see a tile in range 3 only. It's the only non
      //cosmetic feature.
   }


      //___________________________ CODE REFACTORING ______________________________


      /*tmpx = LIGHT_DISTANCE [ floorinfo.ltile ] . tablex;
      tmpy = LIGHT_DISTANCE [ floorinfo.ltile ] . tabley;
      tmpv [ i ].c = p_light_table [ tmpy ] [ tmpx ] [ i ];
      //tmpv [ i ].x += System_X_OFFSET<<16;
      //tmpv [ i ].y += System_Y_OFFSET<<16;

      if ( p_adjust_color == true )
      {
         if ( p_polytype == POLYTYPE_FLAT )
         {
            if ( tmpv [ i ] . c != 0 )
               tmpval = makecol (50, 50, 50);// ( tmpv [ i ] . c  / 4 );// + 24;//96;
            else
               tmpval = 0;
            //tmpcolor = makecol ( tmpval, tmpval, tmpval );
            //if ( tmpcolor < swpcolor )
            //   swpcolor = tmpcolor;
            //tmpv [ i ] . c = swpcolor;
            // note : color will be saved at the end ??? what!

            tmpv [ i ] . c = tmpval;
         }
         else
         {
            tmpval = ( tmpv [ i ] . c ) - 64;//32;
            if ( tmpval < 0 )
               tmpval = 0;
            tmpv [ i ] . c = makecol24 ( tmpval, tmpval, tmpval );
         }
      }
   }*/


//printf ("Debug: draw Maze: before texture rotation\n");

   // texture rotation //TODO: Externalise, it's probably reused.

//printf ("Debug: draw Maze: before draw quad\n");

   _draw_floorceiling_rotate_texture ( &tmpvertex[0], nb_rotation );

   //if ( tmpvertex[0].c != 0 || tmpvertex[1].c != 0 || tmpvertex[2].c != 0 || tmpvertex[3].c != 0 )
      quad3d ( background, POLYTYPE_PTEX_LIT, texture
         , &tmpvertex[ 0 ], &tmpvertex [ 1 ], &tmpvertex [ 2 ], &tmpvertex [ 3 ] );


   //TODO: POLYGON LIGHTING: can be optional, to turn off progressive light. POLYTYPE_PTEX_LIT

   /*V3D tmpv [ 4 ];
   for ( int i = 0 ; i < 4 ; i++) tmpv [i] = vertex [i];

      quad3d ( background, POLYTYPE_PTEX_LIT, texture
         , &tmpv[ 0 ], &tmpv [ 1 ], &tmpv [ 2 ], &tmpv [ 3 ] );*/

//printf ("Debug: draw Maze: after draw quad\n");
}

//_____________________________________________________________________________//
//                            Public methods                                   //
//_____________________________________________________________________________//

void draw_3dmaze ( s_camera *camera, bool demo )
{  int light_level = 3;

   short tmpx;
   short tmpy;
   s_mazetile tmptile;
   int i;
   int j;
   int fog;
   unsigned char tmpmask;
   //unsigned short tmpbitset;
   //unsigned char tmpspec;
   short tmpval;
   bool tmpodd; // result of moduloed oddeven
   int oddeven; // accumulate and then modulo to determin if odd drawing
   bool thick_translucency= false; // draw thickier transparancy when inside it.
   bool drawmwall;
   //int walltex_value;
   //int tmpobjpic;

//printf ("Debug: draw maze: after init\r\n");

   // adjust lighting according to filling.

   //TODO: restore when light active again
   //int light_level = _get_light_level( camera, demo );


   //TODO: RESTORE Does multiple things besides drawing
   //check_palette();

   clear ( background );

   //TODO: Light deactivated for now, too much to deal with
   //evaluate_light ();

   //draw_sky ();

//printf ("Debug: draw maze: after drawsky\r\n");

   if ( get_mazetile_filling( mazetile [ camera->z ] [ camera->y ] [ camera->x ] ) > 0 )
   {  thick_translucency = true;
   }



   //TODO: TEMP Currently a dull loop that draw all floor tiles
   for ( i = 0 ; i < 13 ; i++ )
   {
      _draw_floorceiling ( FLOOR_POLYGON [ i ], texpal_floor(),
               //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 1 ],
               0 );
               //Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
   }


   //TODO: DEBUG code
   BITMAP *bmp = texpal_floor();
   //int c = makecol (255, 255, 255);
   V3D tmpoly [4] =
   { { 0<<16, 256<<16, 1, 0,       TEXTURE_SIZE<<16, 0 },
      {   0<<16, 0<<16,   1, 0,       0,       128 },
      {  256<<16, 0<<16,   1, TEXTURE_SIZE<<16, 0,       64 },
      {   256<<16, 256<<16, 1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 } };

   quad3d ( background, POLYTYPE_PTEX_LIT, bmp
         , &tmpoly[ 0 ], &tmpoly [ 1 ], &tmpoly [ 2 ], &tmpoly [ 3 ] );

   blit(bmp, background, 0,0,320,0, bmp->w, bmp->h );

   //blit_background();

//TODO: Considering using 2D array with empty operation for easier looping and matching
// No need to have cross references from index of xy coordinates.
// In fact you want to covert or copy all the information in a @D projection array what ever
// the facing. You calculate the facing once this way.



//_________________________ CODE REFACTORING _____________________________________

   // loop that draws floors, then walls then objects
/*   for ( i = 0 ; i < 15 ; i++ )
   {
      //This is the drawing order of floors and walls. Maybe make a 3x3 grid
      //printf ("Maze: Draw_maze(): Pass 1");
      tmpx = tpos.x + Maze_DRAW_INFO.test [tpos.facing].offset [ i ].xoff;
      tmpy = tpos.y + Maze_DRAW_INFO.test [tpos.facing].offset [ i ].yoff;

//printf ("Debug: draw maze: drawloop tile %d\r\n", i);
      if ( tmpx >=0 && tmpx < Maze_MAXWIDTH && tmpy >=0 && tmpy < Maze_MAXWIDTH )
      {
         tmptile = mazetile [ tpos.z ] [ tmpy ] [ tmpx ];

         tmpodd = false;
         oddeven = 0;
          // set odd even values to be used by drawwall for texture flipping
         if ( tmpx % 2 > 0 )
            oddeven++;
         if ( tmpy % 2 > 0 )
            oddeven++;
         if ( tpos.facing == Maze_FACE_EAST || tpos.facing == Maze_FACE_WEST )
            oddeven++;

         if ( oddeven % 2 > 0 )
            tmpodd = true;
         else
            tmpodd = false;
//printf ("Debug: draw Maze: before floor and ceiling\n");
         // drawing floor and ceiling

         //REFACTORING: Could restore this option, NOT if masked tex
         //if ( config_old.get ( Config_FLOOR_CEILING ) == Config_FAC_YES )
         //{
             //printf ("Debug: draw Maze: before draw floor\n");
            draw_floor ( FLOOR [ i ], texpal_floor(),
               //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 1 ],
               Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
               //printf ("Debug: draw Maze: after draw floor\n");

            if ( //p_sky == -1 ||
                  tpos.z != 0 )
            {
               //printf ("Debug: draw Maze: before draw ceiling\n");
               draw_floor ( CEILING [ i ], texpal_ceiling(),
                  //p_texset [ tmptile.texture & Maze_TEXSET_MASK] [ 3 ],
                  Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
               //printf ("Debug: draw Maze: after draw ceiling\n");
            }
         //}
//printf ("Debug: draw Maze: before special floor\n");
         // drawing special floor and ceiling
         //if ( ( tmptile.maskposition & MAZETILE_MASKPOS_FLOOR_MASK ) > 0 )
         //{
            //if ( ( tmptile.texture & Maze_FLOOR_MTEX_DOWN ) > 0 )
            if ( is_mazetile_maskedposition( tmptile, MAZETILE_MASKTEXPOS_FLOOR ) )

               draw_masked_floor ( FLOOR [ i ] ,
                     texpal_maskfloor( get_mazetile_maskedfloortex( tmptile ) ),
                     //+ ( ( tmptile.masked & MAZETILE_MASKPOS_FLOOR_MASK ) >> 4 ) ],
                  //p_masktex [ tmptile.floortex ],
                  Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );

            //if ( ( tmptile.texture & Maze_FLOOR_MTEX_UP ) > 0 )
            if ( is_mazetile_maskedposition( tmptile, MAZETILE_MASKTEXPOS_CEILING) )

               draw_masked_floor ( CEILING [ i ] ,
                  texpal_maskfloor( get_mazetile_maskedfloortex( tmptile ) ),

                     //+ ( ( tmptile.masked & MAZETILE_MASKPOS_FLOOR_MASK ) >> 4 ) ],
                  //p_masktex [ tmptile.floortex ],
                  Maze_DRAW_INFO.test [tpos.facing].ftex_rotation );
         //}

//printf ("Debug: draw Maze: before wall and door\n");
         // walls and doors drawing
         for ( j = 0 ; j < 3 ; j++ )
         {
            //printf ("Maze: Draw_maze(): Pass 2");
            // draw wall
            tmpmask = Maze_DRAW_INFO.test [tpos.facing].wall [j];
            if ( ( tmptile.solid & tmpmask ) == tmpmask &&
               Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 )
            {
               draw_wall ( WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                  //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 0 ],
                  texpal_wall (), tmpodd );
               // drawing wall thickness
               if ( WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ].thickwallID
                  != -1 )
                  draw_thickness ( THICKWALL [
                     WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ].thickwallID ],
                        texpal_wall() );
                        //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 0 ] );
            }

//printf ("Debug: draw Maze: before door\n");
            // draw door
            tmpmask = Maze_DRAW_INFO.test [tpos.facing].door [j];
            if ( ( tmptile.solid & tmpmask ) == tmpmask &&
               Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 )
            {
               draw_masked_wall (
                  WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                  //TODO: MULTI-DOOR: will require an index when 4 door types will be available.
                  texpal_door( 0 ) );
                  //p_texset [ tmptile.texture & Maze_TEXSET_MASK ] [ 2 ] );
            }
            // draw grid
            else
            {
               tmpmask = Maze_DRAW_INFO.test [tpos.facing].grid [j];
               if ( ( tmptile.solid & tmpmask ) == tmpmask &&
                  Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 )
               {
                  draw_masked_wall (
                     WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                     texpal_maskwall( get_mazetile_gridtexid( tmptile ) ) );
               }

            }

//printf ("Debug: draw Maze: before masked wall\n");
            // draw masked wall
            drawmwall = false;

            switch ( Maze_DRAW_INFO.test [tpos.facing].wall [j] )
            {
               case WNORTH :
                  if ( is_mazetile_maskedposition( tmptile, MAZETILE_MASKTEXPOS_NORTH) )
                     drawmwall = true;
               break;
               case WEAST :
                  if ( is_mazetile_maskedposition( tmptile, MAZETILE_MASKTEXPOS_EAST) )
                     drawmwall = true;
               break;
               case WSOUTH :
                  if ( is_mazetile_maskedposition( tmptile, MAZETILE_MASKTEXPOS_SOUTH) )
                     drawmwall = true;
               break;
               case WWEST :
                  if ( is_mazetile_maskedposition( tmptile, MAZETILE_MASKTEXPOS_EAST) )
                     drawmwall = true;
               break;
            }
            if ( drawmwall == true
               && ( Maze_DRAW_INFO.tested_wallID [ i ] [ j ] != -1 ) )
               draw_masked_wall (
                  WALL [ Maze_DRAW_INFO.tested_wallID [ i ] [ j ] ],
                     texpal_maskwall( get_mazetile_maskedwalltex( tmptile ) ) );
                  //+ ( tmptile.masked & MAZETILE_MASKPOS_WALL_MASK ) ] );
                  //p_masktex [ tmptile.walltex ] );



         }

         // place objects in the maze
         //tmpval = ( tmptile.objposition & Maze_OBJECT_MASK ) >> 6;

         // object display testing code
         //for ( j = 0 ; j < 9 ; j++ )
         //{
            //draw_floor_object ( i, 4, 1024 );

            //draw_ceiling_object ( i, 4, 1024 );

         //}

         tmpval = 0; // new comparation to make since 3 type of objects
         if ( tmpval > 0 )
         {
            //commented because was not used
            //tmpbitset = Maze_DRAW_INFO.test [ tpos.facing ].object [ tmpval ];
            //tmpobjpic = tmptile.objectpic; // need to adapt according to wall ceiling, floor

            for ( j = 0 ; j < 9 ; j++ )
            {
               //if ( j == 4)
               //   draw_floor_object ( FLOOR_OBJECT [ i ] [ j ], 1024,
               //      p_light_table [ LIGHT_DISTANCE [ i ].tabley ]
               //         [ LIGHT_DISTANCE [ i ].tablex ] [ 4 ] );
               // replace with new object drawing functions
               //if ( tmpbitset & ( 256 >> j ) )
               //   draw_object ( OBJECT [ i ] [ j ],
               //      p_objimg [ tmpobjpic ],
               //      p_light_table [ LIGHT_DISTANCE [ i ].tabley ]
               //         [ LIGHT_DISTANCE [ i ].tablex ] [ 4 ] );
            }
         }

         // drawing transparent wall
         //tmpspec = tmptile.special & MAZETILE_FILLING_MASK;
         if ( get_mazetile_filling( tmptile ) > 0 )
            for ( j = 0 ; j < 2 ; j++ )
            {
               if ( Maze_DRAW_INFO.trans_wallID [ i ] [ j ] != -1 )
                  draw_transparent_wall (
                     WALL [ Maze_DRAW_INFO.trans_wallID [ i ] [ j ] ],
                     get_mazetile_filling( tmptile ),
                     Maze_DRAW_INFO.trans_wallID [ i ] [ j ], thick_translucency );
            }

         if ( get_mazetile_filling( tmptile )  == MAZETILE_FILLING_DARKNESS)
            for ( j = 0 ; j < 2 ; j++ )
            {
               if ( Maze_DRAW_INFO.trans_wallID [ i ] [ j ] != -1 )
                  draw_transparent_wall (
                     WALL [ Maze_DRAW_INFO.trans_wallID [ i ] [ j ] ],
                     8 , Maze_DRAW_INFO.trans_wallID [ i ] [ j ]
                     , false );
            }

      }
   }

   // debuging display not to keep
   //textprintf_old ( mazebuffer, text_font, 0, 0, General_COLOR_TEXT,
   //            "Encounter Counter: %d", encount.counter() );


  // printf ("before draw screen\n");
   draw_screen (); //Should be drawing the translucency filling
   //printf ("after draw screen\n");

  // textprintf_old ( mazebuffer, text_font, 0,0, General_COLOR_TEXT, "Palette: %d", mazepalid);

   blit_mazebuffer();
   */
}
