/**
   mazemap.c

   @author Eric Pietrocupo
   @since  March 2021
   @license GNU General Public License
*/

#include <stdbool.h>
#include <allegro.h>
#include <assets.h>
#include <mazetile.h>
#include <mazefile.h>
#include <editor.h>
#include <mazemap.h>
#include <video.h>


//-------------------------------------------------------------------------//
//-                         Private constant                              -//
//-------------------------------------------------------------------------//

#define COORDINATE_ADJUST  5

//--------------------------------------------------------------------------//
//-                          Public Methods                                -//
//--------------------------------------------------------------------------//


void _draw_map_coordinate ( s_cursor *cursor, s_viewport *viewport )
{  //int basey = VIEWPORT_SIZE * TILE_SIZE;
   //int basex = 0
   int y = viewport->y_pos;
   for ( int i = viewport->yscroll + viewport->size - 1; i >= viewport->yscroll ; i-- )
   {  textprintf_ex ( background, font, viewport->x_pos, y + COORDINATE_ADJUST, lgray, black, "%d", i );
      y += TILE_SIZE;
   }

   y = (viewport->size * TILE_SIZE) + viewport->y_pos;
   int x = TILE_SIZE + viewport->x_pos;
   for ( int i = viewport->xscroll ; i < viewport->xscroll + viewport->size ; i++ )
   {
      textprintf_ex ( background, font, x, y + COORDINATE_ADJUST, lgray, black, "%d", i );
      x += TILE_SIZE;
   }

   textprintf_ex ( background, font, viewport->x_pos, y + COORDINATE_ADJUST,
                  black, white, "%02d", cursor->z );
//TODO: Check impact of reinverting coordinates, does it screw up the maze. There is multiple
// aspects to consider: A) Map display, B) Storage in array, C) 3D maze display
}

void _draw_map_background ( s_viewport *viewport )
{  static BITMAP* icon_tile_empty = NULL;
   if (icon_tile_empty == NULL ) icon_tile_empty = asset_editor("TILE_EMPTY_BMP");

   int totalsize = ( viewport->size * TILE_SIZE);
   int basey = totalsize + viewport->y_pos;
   int basex = 0;
   rectfill ( background, basex, basey, basex + TILE_SIZE -1, basey + TILE_SIZE, white );

   //NOTE: Loop stating from 1 to avoid the coordinates
   for ( int j = 0 ; j < viewport->size; j++ )
   {  for ( int i = 1 ; i <= viewport->size; i++ )
      {  int x = (i * TILE_SIZE) + viewport->x_pos + ADJUST_GRID;
         int y = (j * TILE_SIZE) + viewport->y_pos + ADJUST_GRID;
         draw_sprite ( background, icon_tile_empty, x, y );
      }
   }

   rect ( background, viewport->x_pos + TILE_SIZE, viewport->y_pos,
         viewport->x_pos + totalsize + TILE_SIZE + ADJUST_GRID
         , viewport->y_pos + totalsize + ADJUST_GRID, white );

}

void _draw_map_tile_special ( s_mazetile tile, int y, int x )
{  static BITMAP* icon_tile_solid = NULL;
   if (icon_tile_solid == NULL ) icon_tile_solid = asset_editor("TILE_SOLID_BMP");
   static BITMAP* icon_tile_mbounce = NULL;
   if (icon_tile_mbounce == NULL ) icon_tile_mbounce = asset_editor("TILE_MBOUNCE_BMP");
   static BITMAP* icon_tile_light = NULL;
   if (icon_tile_light == NULL ) icon_tile_light = asset_editor("TILE_LIGHT_BMP");


   if ( is_mazetile_special ( tile, MAZETILE_SPECIAL_SOLID) == true )
   {   draw_sprite ( background , icon_tile_solid, x, y );
   }

   if ( is_mazetile_special ( tile, MAZETILE_SPECIAL_MAGIKBOUNCE) == true )
   {   draw_sprite ( background, icon_tile_mbounce, x, y );
   }
   if ( is_mazetile_special ( tile, MAZETILE_SPECIAL_LIGHT) == true )
   {  draw_sprite ( background, icon_tile_light, x, y );
   }

}

void _draw_map_tile_wall ( s_mazetile tile, int y, int x )
{
   static BITMAP* icon_wall_wall = NULL;
   if (icon_wall_wall == NULL ) icon_wall_wall = asset_editor("WALL_WALL_BMP");
   static BITMAP* icon_wall_door = NULL;
   if (icon_wall_door == NULL ) icon_wall_door = asset_editor("WALL_DOOR_BMP");
   static BITMAP* icon_wall_grid = NULL;
   if (icon_wall_grid == NULL ) icon_wall_grid = asset_editor("WALL_GRID_BMP");

   for ( int side = 0 ; side <= MAZETILE_SOLID_WEST ; side++ )
   {  if ( is_mazetile_wall_oftype( tile, side, MAZETILE_SOLIDTYPE_WALL))
      {  rotate_sprite ( background, icon_wall_wall,
            x, y, (side*64)<<16 );
      }
      if ( is_mazetile_wall_oftype( tile, side, MAZETILE_SOLIDTYPE_DOOR))
      {  rotate_sprite ( background, icon_wall_door,
            x, y, (side*64)<<16 );
      }
      if ( is_mazetile_wall_oftype( tile, side, MAZETILE_SOLIDTYPE_GRID))
      {  rotate_sprite ( background, icon_wall_grid,
            x, y, (side*64)<<16 );
      }
   }
}

void _draw_map_tile_filling ( s_mazetile tile, int y, int x )
{  //TODO: TEMP deactivated for compilation
    /*static BITMAP* icon_tile_light = NULL;
   if (icon_tile_light == NULL ) icon_tile_light = asset_editor("TILE_LIGHT_BMP");

   int tmpfilling = get_mazetile_filling( tile );
   if ( tmpfilling > 0 )
   {
      set_trans_blender(0, 0, 0, 100);
      drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
      //TODO: Maybe use a non translucent version, colored BG for example.
      //especially if setting translucency is slow.

      if (Maze_SWALL_INFO[ tmpfilling].drawmode == DRAW_MODE_TRANS)
      {
         rectfill ( background, x, y, x+TILE_SIZE-1, y+TILE_SIZE-1,
         makecol ( Maze_SWALL_INFO [ tmpfilling ].red,
                   Maze_SWALL_INFO [ tmpfilling ].green,
                   Maze_SWALL_INFO [ tmpfilling ].blue ) );
         //TODO: Could prebuild colors

      }
      else
      {
         if ( tmpfilling == MAZETILE_FILLING_DARKNESS )
            draw_trans_sprite ( background, icon_tile_light, x, y );
         //TODO: maybe require a special icon
      }

      drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
      set_trans_blender(0, 0, 0, 255);
   }*/
}


//--------------------------------------------------------------------------//
//-                          Public Methods                                -//
//--------------------------------------------------------------------------//

void init_viewport ( s_viewport *viewport, int size, int xpos, int ypos )
{  viewport->size = size;
   viewport->x_pos = xpos;
   viewport->y_pos = ypos;
   viewport->xscroll = 0;
   viewport->yscroll = 0;
   viewport->special = true;
   viewport->filling = true;
   viewport->events = false;
   viewport->objects = false;
   viewport->masked = false;
}


void draw_map ( s_cursor *cursor, s_viewport *viewport )
{

   _draw_map_background( viewport );
   _draw_map_coordinate( cursor, viewport );

   int viewport_size = (viewport->size -1) * TILE_SIZE;
   //for ( int j = cursor->yscroll ; j < cursor->yscroll + VIEWPORT_SIZE ; j++ )
   //{  for ( int i = cursor->xscroll ; i < cursor->xscroll + VIEWPORT_SIZE ; i++ )
   for ( int y = 0; y < viewport->size ; y++)
   {  for ( int x = 0 ; x < viewport->size ; x++ )
      {
         int xpos = viewport->x_pos + ((x+1)*TILE_SIZE) + ADJUST_GRID;
         int ypos = viewport->y_pos + ( viewport_size - ((y*TILE_SIZE) - ADJUST_GRID));
         int i = x + viewport->xscroll;
         int j = y + viewport->yscroll;
         s_mazetile tile = mazetile [cursor->z][ j ][ i ];

         if ( i >= 0 && i < MAZE_SIZE && j >= 0 && j < MAZE_SIZE )
         {
            if ( viewport->special == true )
            {  _draw_map_tile_special( tile, ypos, xpos );
            }

            _draw_map_tile_wall( tile, ypos, xpos);

            if ( viewport->filling == true )
            {  _draw_map_tile_filling( tile, ypos, xpos);
            }
         }
         // Draw Event Icon
         //TODO: temporarily disabled, see if in another mode or wait for SQL
         /*eventid = get_mazetile_event( mazetile [p_zcur][j][i] );
         if ( eventid > 0 )
         {
            //error = tmpevent.SQLselect ( eventid );

            //TODO: Currently drawing generic maze events since cannot use index
            //if ( error == SQLITE_ROW )
            //{
            //   if ( tmpevent.pass_event() > 0 )
            //      draw_sprite ( editorbuffer,
            //         datafile_index_getobj_asbitmap ( &datidx_editoricon
            //         , EVENT_ICON_PASSFAIL [ tmpevent.pass_event() ]), x, y );
            //   else
            //      draw_sprite ( editorbuffer,
            //         datafile_index_getobj_asbitmap ( &datidx_editoricon,
            //          EVENT_ICON_LOCK [ tmpevent.lock_event() ]), x, y );

            //}
            //else
               draw_sprite ( editorbuffer, icon_event , x, y );

         }*/

         // draw selection if active
         //TODO: Selection drawing, put into another submethod.
         //Maybe use a tool structure and put selection vars in it
         //Maybe selection information could be in a structure or a cursor.
         /*draw_selection = false;
         xok = false;
         yok = false;


         if ( p_selection_active == true)
         {
            switch ( p_tool)
            {

               case Editor_TOOL_RECTANGLE :
                  if ( p_xmark < p_xcur )
                  {
                     if ( i >= p_xmark && i <= p_xcur)
                        xok = true;
                  }
                  else
                     if ( i >= p_xcur && i <= p_xmark)
                        xok = true;

                  if ( p_ymark < p_ycur )
                  {
                     if ( j >= p_ymark && j <= p_ycur)
                        yok = true;
                  }
                  else
                     if ( j >= p_ycur && j <= p_ymark)
                        yok = true;

                  if ( xok == true && yok == true)
                     draw_selection = true;

               break;

            }
         }

         if ( draw_selection == true || p_selection [j][i] == true )
         {
            // display tile  ASSETS_EDICON_MARKER_PAINT

            set_trans_blender(0, 0, 0, 100);
            drawing_mode ( DRAW_MODE_TRANS, NULL, 0, 0 );
            draw_trans_sprite ( editorbuffer, icon_marker_paint, x, y );
            set_trans_blender(0, 0, 0, 255);
            drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
         }
*/

      }
   }

   // draw start position
   //TODO: Start position like events, maybe draw later, or load list
   /*if  ( p_startpos.z == p_zcur )
   {
      if ( p_startpos.x >= p_xscroll && p_startpos.x <= ( p_xscroll + 26 ) )
         if ( p_startpos.y >= p_yscroll && p_startpos.y <= ( p_yscroll + 26 ) )
         {
            x = ( ( p_startpos.x - p_xscroll ) * 16 ) + 16;
            y = 416 - ( ( p_startpos.y - p_yscroll ) * 16 );
            draw_sprite ( editorbuffer, icon_special_start, x, y );
         }
   }*/


}


//TODO: add parameters according to needs in spell vs editor.
void draw_full_tiny_map ( void )
{/*
   int x;
   int y;
   int i; // pixel coordinates
   int j; // pixel coordinates
   int palette;
   //bool solid = true;
   int key;
   int exit = false;
   int tmpcolor;

   while (exit == false)
   {
      clear (editorbuffer);
      drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
      rect ( editorbuffer , 15, 15, 416, 416, makecol (255, 255, 255));
      i=16;
      j=412;
      textprintf_old ( editorbuffer, text_font, 0, 417, makecol ( 255, 255, 255 ),
         "Level=%d | Active Palette = %d", p_zcur, p_active_palette );

      for ( x = 0 ; x < Maze_MAXWIDTH; x++ )
      {
         for ( y = 0 ; y < Maze_MAXWIDTH ; y++ )
         {
            palette = get_mazetile_palette( mazetile [p_zcur][y][x]);

            if ( is_mazetile_special ( mazetile [p_zcur][y][x], MAZETILE_SPECIAL_SOLID ) == false)
            {
               //printf ("Editor: Draw something at %d, %d\n", i, j );
               //TODO: seems to be related to drawing various area on preview map. Disable for now
               //if ( Editor_texpalette_color [palette].texture != -1 )
               //{
               //   drawing_mode ( DRAW_MODE_MASKED_PATTERN,
               //   datafile_index_getobj_asbitmap (
               //      &datidx_editoricon, Editor_texpalette_color [palette].texture)
               //      , 0, 0);
               //}
               //else
               //{
                  drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
               //}
               tmpcolor = Editor_texpalette_color [palette].color;
               if ( palette == p_active_palette )
                  tmpcolor = 255;
               //printf ("Makecol=%d, tablecol=%d", makecol (255, 255, 255), Editor_texpalette_color [31].color );
               rectfill ( editorbuffer, i, j, i+3, j+3, makecol (tmpcolor, tmpcolor, tmpcolor)  );
            }

            if ( get_mazetile_event( mazetile [p_zcur][y][x] ) > MAZETILE_NO_EVENT )
            {  rectfill ( editorbuffer, i+1, j+1, i+2, j+2, makecol (128, 128, 255) );
            }

            j = j - 4;
         }

         i = i + 4;
         j = 412;
      }

      textprintf_old ( editorbuffer, text_font, 0, 460, General_COLOR_TEXT,
         "[ESC]Exit the mode [Bracers]Change active palette [PgUp/Dn]Change Floor"  );

      blit_editorbuffer();



      copy_buffer();

      //tmpcode
//      make_screen_shot( "FullMap.bmp");

      key = mainloop_readkeyboard();
      clear_keybuf();

      switch ( key )
      {
         case KEY_ESC:
            exit = true;
         break;
         case KEY_PGUP:
            if ( p_zcur > 0)
               p_zcur--;
         break;
         case KEY_PGDN:
            if ( p_zcur < Maze_MAXDEPTH -1)
               p_zcur++;
         break;
         case KEY_OPENBRACE:
            if ( p_active_palette > 0 )
               p_active_palette--;
         break;
         case KEY_CLOSEBRACE:
            if ( p_active_palette < Maze_NB_TEXPALETTE -1 )
               p_active_palette++;
         break;
      }
   }
*/
}

