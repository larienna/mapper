/**
   Wizardry Legacy
   Texturepalette.h

   @author: Eric Pietrocupo
   @date: February 13th, 2021
   @license: GNU general public license

   This is the over simplified texture palette that will replace the C++ version of the palette.
   One of the major changes is that there are no more buildup textures using patches. Every
   thing must be a full texture.

   The palette will be stored in this module as a global array of structure and accessor
   functions will be used to get the right texture. The structure will only contains a list of pointers
   to a texture in the datafile already loaded since there is no patch anymore, this is why
   it is massively simplified.

   At the beginning, it is initialised with dummy default textures to make it easy to I dentify in
   case of bugs. Then the BITMAP* pointers can be set manually, or can be searched from the
   database. Therefore building texture will require database access. But still, in case of trouble,
   inserting texture name in the database is not so tedious to do.

   Note that even if it's called a texture palette, maze objects are also located in the palette
   as they are also a colleciton of images, but not really textures.
*/

#ifndef TEXTUREPALETTE_H_INCLUDED
#define TEXTUREPALETTE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif


/** This is a series of constants that limit the size of the palette. Usefull if the user
   is iterating on all elements.
*/

#define TEXTUREPAL_NB_GRID          4
#define TEXTUREPAL_NB_DOOR          4
#define TEXTUREPAL_NB_MASKEDWALL    16
#define TEXTUREPAL_NB_MASKEDFLOOR   16
#define TEXTUREPAL_NB_FLOOROBJECT   16
#define TEXTUREPAL_NB_CEILINGOBJECT 16

//TODO: maybe can set private when editor refractored
#define TEXTUREPAL_NB_PALETTE    64
#define TEXTUREPAL_NB_TEXTURE    76



#define TEXTURE_SIZE       256 //size in pixel of the texture

/** Some methods are required to initialize, save and load texture palette. Saving and loading
   will strictly pass through the database. While initialisation will set all BITMAP points
   to the default texture making it easy to see uninitialized textures in the palette.
*/

void init_texpal ( void );
void load_texpal ( void ); //TODO: depends if database reference hidden, else pass in parameter
void save_texpal ( void );

/** There needs to be an method to change the currently active palette. It basically
   set an index value and if out of bound, set to 0.
*/

int get_texpal_id ( void );
void set_texpal_id ( int palette_id );


/** The palette will require accessor methods to be able to query the right texture.
   Default textures will be used in case of undefined or out of bound texture request.
*/

BITMAP *texpal_wall ();
BITMAP *texpal_floor ();
BITMAP *texpal_ceiling ();
BITMAP *texpal_door ( int index );
BITMAP *texpal_grid ( int index );
BITMAP *texpal_maskwall (int index);
BITMAP *texpal_maskfloor (int index);
BITMAP *texpal_ceilingobject (int index);
BITMAP *texpal_floorobject (int index);

#ifdef __cplusplus
}
#endif

#endif // TEXTUREPALETTE_H_INCLUDED
