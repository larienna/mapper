/**
   editor.c

   @author Eric Pietrocupo
   @since  February 21st, 2004
   @date   Refactored on February 2021
   @license GNU General Public License
*/

#include <allegro.h>
#include <allegrohelp.h>
#include <stdbool.h>
#include <video.h>
#include <wui.h>
#include <wuidialog.h>
//#include <maze.h>
//TODO currently a c++ module, so cannot be called.
#include <mazetile.h>
#include <mazefile.h>
#include <editor.h>
#include <mazemap.h>
#include <assets.h>
#include <texturepalette.h>
#include <stdio.h>

//-------------------------------------------------------------------------//
//-                         Private constant                              -//
//-------------------------------------------------------------------------//

#define VIEWPORT_SIZE   20
#define MAP_XOFFSET     0     //offset on the x axis to slide the drawn map
#define MAP_YOFFSET     16    //offset on the y axis to slide the drawn map



#define TOOL_TILE      0
#define TOOL_CORRIDOR  1
#define TOOL_RECTANGLE 2
#define TOOL_BRUSH     3


//-------------------------------------------------------------------------//
//-                         Private Structures                            -//
//-------------------------------------------------------------------------//

//keep many information about the usage of the currently active tool
typedef struct s_tool
{  int type;
   bool exit_editor;
   bool selection_active;
}s_tool;
//-------------------------------------------------------------------------//
//-                          Private Variables                            -//
//-------------------------------------------------------------------------//

//Wait until necessary, try to keep variable local
//s_Party_position p_startpos;

//TODO: move into the tool most of those variables
/*   private: int p_tool; // identify which tool is currently active
   private: unsigned int p_layer; // list of flags with layers of information to be displayed.
   private: int p_xmark; // marks used for the rectangle tool
   private: int p_ymark;
   private: bool p_selection_active; // indicates currently selecting something
   private: bool p_selection [Maze_MAXWIDTH] [Maze_MAXWIDTH];
   private: int p_active_palette;
   private: bool p_adventure_selected;
   private: char p_adventure_mazefile [ Manager_TMP_STR_LEN + 10];
   private: char p_adventure_databasefile [ Manager_TMP_STR_LEN + 10];*/

//--------------------------------------------------------------------------//
//-                          Private Methods                               -//
//--------------------------------------------------------------------------//

void _init_cursor ( s_cursor *cursor)
{  cursor->x = 0;
   cursor->y = 0;
   cursor->z = 0;
   //cursor->xscroll = 0;
   //cursor->yscroll = 0;
}

void _init_tool ( s_tool *tool)
{  tool->type = TOOL_TILE;
   tool->exit_editor = false;
}

//TODO move to mazedit
void _clear_maze ( void )
{
   int i,j,k;
   s_mazetile tile = new_mazetile_empty();

   set_mazetile_special( &tile, MAZETILE_SPECIAL_SOLID );

   for ( k = 0 ; k < MAZE_DEPTH; k++ )
      for ( j = 0 ; j < MAZE_SIZE; j++ )
         for ( i = 0 ; i < MAZE_SIZE; i++ )
            mazetile [ k ] [ i ] [ j ] = tile;

}

void _draw_background_cursor ( s_cursor *cursor, s_tool *tool, s_viewport *viewport, int xoffset, int yoffset )
{  static BITMAP* icon_tool [] = { NULL, NULL, NULL, NULL};
   if (icon_tool[TOOL_TILE] == NULL) icon_tool[TOOL_TILE] = asset_editor("CURSOR_SQUARE_BMP");
   if (icon_tool[TOOL_CORRIDOR] == NULL) icon_tool[TOOL_CORRIDOR] = asset_editor("CURSOR_ARROW_BMP");
   if (icon_tool[TOOL_RECTANGLE] == NULL) icon_tool[TOOL_RECTANGLE] = asset_editor("CURSOR_ANGLE_BMP");
   if (icon_tool[TOOL_BRUSH] == NULL) icon_tool[TOOL_BRUSH] = asset_editor("CURSOR_BRUSH_BMP");

   int x = xoffset + (( ( cursor->x - viewport->xscroll ) * TILE_SIZE ) + TILE_SIZE ) + ADJUST_GRID;
   int y = yoffset +
      ((VIEWPORT_SIZE-1) * TILE_SIZE) - ( ( cursor->y - viewport->yscroll ) * TILE_SIZE )
      + ADJUST_GRID;
   //int markx = ( ( p_xmark - p_xscroll ) * 16 ) + 16;
   //int marky = 416 - ( ( p_ymark - p_yscroll ) * 16 );

   draw_sprite ( background,  icon_tool[tool->type], x, y );

   //TODO: Finish drawing when tool available
   /*if ( p_selection_active && tool->type = TOOL_RECTANGLE)
   {  draw_sprite ( buffer, icon_marker_flag, markx, marky );
   }*/


}

void _draw_background_tool_intructions ( s_tool *tool )
{  static const char* msg[] =
   { "TOOL: TILE: [ASDW] change wall solidity [Bracket]Change Palette",
     "TOOL: CORRIDOR: [ASDW] Dig in the indicated direction",
     "TOOL: RECTANGLE: [Z] Start/End Rectangle [X] Clear Selection -> [R] Edit Selected",
     "TOOL: BRUSH: [Z] Start/End Selection [ASDW] Expand Selection [X] Clear Selection -> [R] Edit Selected"
   };
   //static const char* FKEY = "F1-FILE     F2-TOOL     F3-TEXTURE  F4-VIEW     F5-LAYER    F6-ERROR    F7-DATA     F8-EVENT";
   static const char* FKEY = "F1-QUIT";

   textout_ex ( background, font, msg[tool->type], 0, 466, lgray, black );

   textout_ex ( background, font, FKEY, 0, 0, lgray, black );

   //TODO: Draw selection instruction, probably using tool structure when available.
   /*
      if ( p_selection_active == false)
         textout_old ( buffer, text_font, "[ Arrows ] move [ PgDnUp ] Change Level [ Del ] Delete"
            , 0, 452, General_COLOR_TEXT ); // draw here to allow window instruction to overlap
      else
         textout_old ( buffer, text_font, "[ Arrows ] move [ Del ] Delete"
            , 0, 452, General_COLOR_TEXT ); // draw here to allow window instruction to overlap
   */

}

void _draw_background_detail_event ( s_cursor *cursor, int xoffset, int yoffset )
{  s_mazetile tile = mazetile [cursor->z][cursor->y][cursor->x];

   if ( get_mazetile_event( tile ) > 0 )
   {  //TODO: Even must be converted as plain C first
      /*Event tmpevent;
      int error;

      error = tmpevent.SQLselect ( get_mazetile_event (tile));

      if ( error == SQLITE_ROW)
      {
         textprintf_old ( editorbuffer, text_font, 440, 396, General_COLOR_TEXT,
            "EVENT %d: WHEN %s OCCURS", tmpevent.primary_key(), STR_EVE_TRIGGER [ tmpevent.trigger()] );
         textprintf_old ( editorbuffer, text_font, 440, 412, General_COLOR_TEXT,
            "IF %s PASS", STR_EVE_LOCK [ tmpevent.lock_event()] );
         textprintf_old ( editorbuffer, text_font, 440, 428, General_COLOR_TEXT,
            "THEN %s", STR_EVE_PASSFAIL [ tmpevent.pass_event()] );
         textprintf_old ( editorbuffer, text_font, 440, 444, General_COLOR_TEXT,
            "ELSE %s", STR_EVE_PASSFAIL [ tmpevent.fail_event()] );
      }
      else*/
         textprintf_ex ( background, font, xoffset, yoffset, lgray, black,
         "Could not read event from the Database" );
   }
   else
      textprintf_ex ( background, font, xoffset, yoffset, lgray, black,
         "There is no event on this tile" );
}

void _draw_background_detail_bytecode ( s_cursor *cursor, int xoffset, int yoffset )
{  s_mazetile tile = mazetile [cursor->z][cursor->y][cursor->x];
   int fh = text_height( font );
   const char* SOLID[] = { "NONE", "WALL", "GRID", "DOOR" };
   const char* BOOL[] = { "No ", "Yes"  };
   static int barcolor = 0;
   if ( barcolor == 0) barcolor = makecol( 50, 50, 100);

   textprintf_ex ( background, font, xoffset, yoffset, white, black,
                  "Tile info: X=%02d Y=%02d Z=%02d", cursor->x, cursor->y, cursor->z);
   textprintf_ex ( background, font, xoffset, yoffset + fh, lgray, barcolor,
                 "1:SOLID     North East  South West   " );
   textprintf_ex ( background, font, xoffset, yoffset + (fh*2), white, black,
                 "            %4s  %4s  %4s  %4s",
                 SOLID [get_mazetile_wall( tile, MAZETILE_SOLID_NORTH)],
                 SOLID [get_mazetile_wall( tile, MAZETILE_SOLID_EAST)],
                 SOLID [get_mazetile_wall( tile, MAZETILE_SOLID_SOUTH)],
                 SOLID [get_mazetile_wall( tile, MAZETILE_SOLID_WEST)] );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*3), lgray, barcolor,
                 "2:EVENT                           ID" );
   textprintf_ex ( background, font, xoffset, yoffset + (fh*4), white, black,
                 "                                 %3d",
                 get_mazetile_event ( tile ) );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*5), lgray, barcolor,
                 "3:SPECIAL ??? Lgt Sol Mgb    Filling" );
   textprintf_ex ( background, font, xoffset, yoffset + (fh*6), white, black,
                 "          %3s %3s %3s %3s    %4s",
                 BOOL[ is_mazetile_special(tile, MAZETILE_SPECIAL_UNKNOWN) ],
                 BOOL[ is_mazetile_special(tile, MAZETILE_SPECIAL_LIGHT) ],
                 BOOL[ is_mazetile_special(tile, MAZETILE_SPECIAL_SOLID) ],
                 BOOL[ is_mazetile_special(tile, MAZETILE_SPECIAL_MAGIKBOUNCE) ],
                 "TODO" );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*7), lgray, barcolor,
                 "4:OBJECT                           ." );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*9), lgray, barcolor,
                 "5:PALETTE                          ." );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*11), lgray, barcolor,
                 "6:OBJPOS                           ." );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*13), lgray, barcolor,
                 "7:MASKTEX                          ." );

   textprintf_ex ( background, font, xoffset, yoffset + (fh*15), lgray, barcolor,
                 "8:MASKPOS                          ." );

   /*textprintf_old ( editorbuffer, text_font, 440, 188, General_COLOR_TEXT,
      "Palette" );
   textprintf_old ( editorbuffer, text_font, 440, 204, General_COLOR_TEXT,
      "ID (%2d)", get_mazetile_palette( tile ) );


   // todo: display textures instead. Not sure since need to prebuild them each time
   // else need palete management change in the editor
   textprintf_old ( editorbuffer, text_font, 440, 276, General_COLOR_TEXT,
      "Active Pal(%2d):Wall-Floor-Ceiling", p_active_palette );

   y = 380;
   x = 440;
   mask = MAZETILE_SPECIAL_MAGIKBOUNCE;
   for ( i = 0 ; i < 4; i++)
   {
      if ( is_mazetile_special ( tile, mask ) > 0 )
      {
         textout_old (editorbuffer, text_font, STR_EDT_SPECIAL_TECH [i], x, y,
         General_COLOR_TEXT );
         x += 42;
      }
      mask = mask << 1;
   }

   //tmpfill = tile.special & MAZETILE_FILLING_MASK;
   if ( get_mazetile_filling( tile ) > 0 )
   {
      textout_old (editorbuffer, text_font,
         STR_EDT_SPECIAL_FILL [get_mazetile_filling (tile)], x, y,
         General_COLOR_TEXT );
   }*/

}

void _draw_background ( s_cursor *cursor, s_tool *tool, s_viewport *viewport )
{
   draw_map(cursor, viewport);

   _draw_background_tool_intructions( tool);
   _draw_background_cursor( cursor, tool, viewport, MAP_XOFFSET, MAP_YOFFSET );

   int tiledetail_xoffset = ( VIEWPORT_SIZE + 2 ) * TILE_SIZE;
   _draw_background_detail_event( cursor, tiledetail_xoffset, 396);
   _draw_background_detail_bytecode( cursor, tiledetail_xoffset, TILE_SIZE);

}

//TODO: Decompose in smaller parts
//TODO: add rotation keys to change the view direction of the tile preview. Put an arrow
// that points into the displayed direction.
void draw_detailed_tile ( void )
{
 /*  int i;
   unsigned char mask = 0;
   short x = 0;
   short y = 0;
   //s_mazetile tile = mazetile [p_zcur][p_ycur][p_xcur];
   int texID = 0;
   int tmpsolid;
   int polytype;
   int tmpolytype;*/
   //int tmpfill;

   //TODO: add texture masking on walls and floors
   //

   // 6 polygons to be drawn and the information stored in 2 different structures
   // Editor_POLYGON_INFO and Editor_room
   // TODO: Disabled to compilation reason. Will have to redo the logic.

   /*
   set_trans_blender ( 0, 0, 0, EDITOR_TRANSWALL_BLEND );

   for ( i = 0; i < 6; i++)
   {
      texID = -1; // skip drawing if remains at -1, there is no wall there.
      //printf ("step 1\n");
      if ( Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_WALL
         || Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL)
      {
         //printf ("step 2\n");
         tmpsolid = ( tile.solid & Editor_POLYGON_INFO[i].mask );


         if ( tmpsolid > 0)
         {
          //  printf ("step 4\n");
            tmpsolid = tmpsolid >> Editor_POLYGON_INFO[i].shift;

            // set polytype transparent for the front wall that will hide the rest of the room
            polytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
               ? POLYTYPE_ATEX_TRANS : POLYTYPE_ATEX;

        //    printf ("step 5\n");
            switch (tmpsolid )
            {

               case MAZETILE_SOLID_WEST_WALL:
                  texID = TexPal_IDX_WALL;
               break;
               case MAZETILE_SOLID_WEST_GRID:
                  texID = TexPal_IDX_GRID;
                  texID +=
                     get_mazetile_gridtexid( mazetile [p_zcur][p_ycur][p_xcur] );
                  polytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
                     ? POLYTYPE_ATEX_MASK_TRANS : POLYTYPE_ATEX_MASK;
               break;
               case MAZETILE_SOLID_WEST_DOOR:
                  texID = TexPal_IDX_DOOR;
                  polytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
                     ? POLYTYPE_ATEX_MASK_TRANS : POLYTYPE_ATEX_MASK;
                  tmpolytype = Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL
                     ? POLYTYPE_ATEX_TRANS : POLYTYPE_ATEX;
                  // Draw wall first, then the door
                  if ( Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL )
                  {
   //                  set_trans_blender ( 0, 0, 0, EDITOR_TRANSWALL_BLEND );
                  }
                  quad3d ( editorbuffer, tmpolytype,
                     mazepal.bmp [TexPal_IDX_WALL],
                     &Editor_room [i][ 0 ],
                     &Editor_room [i][ 1 ],
                     &Editor_room [i][ 2 ],
                     &Editor_room [i][ 3 ] );
               break;

            }


         }
      }
      else // it's a floor or ceiling
      {
         polytype = POLYTYPE_ATEX;
         texID =  Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_FLOOR
            ? TexPal_IDX_FLOOR : TexPal_IDX_CEILING;

      }
      //printf ("step 6\n");
      if ( texID != -1)
      {
         //printf ("Drawing poly=%d, type=%d, texID=%d\n", i, polytype, texID );

         if ( Editor_POLYGON_INFO[i].type == EDITOR_POLYGON_TRANSWALL )
         {
            //set_trans_blender ( 0, 0, 0, EDITOR_TRANSWALL_BLEND );
         }

         quad3d ( editorbuffer, polytype,
                 mazepal.bmp [texID],
                 &Editor_room [i][ 0 ],
                 &Editor_room [i][ 1 ],
                 &Editor_room [i][ 2 ],
                 &Editor_room [i][ 3 ] );

         //restore Opacity.
         //set_trans_blender ( 0, 0, 0, 255 );

         //printf ("step 7\n");
         //printf ("Drawn polygon %d\n", i);
      }

   }
   */

   //set_trans_blender ( 0, 0, 0, 255 );

   /*quad3d ( editorbuffer, POLYTYPE_ATEX,
              mazepal.bmp [TexPal_IDX_CEILING],
              &Editor_room [4][ 0 ],
              &Editor_room [4][ 1 ],
              &Editor_room [4][ 2 ],
              &Editor_room [4][ 3 ] );

   quad3d ( editorbuffer, POLYTYPE_ATEX,
              mazepal.bmp [TexPal_IDX_FLOOR],
              &Editor_room [5][ 0 ],
              &Editor_room [5][ 1 ],
              &Editor_room [5][ 2 ],
              &Editor_room [5][ 3 ] );*/


/*
// note: to change with active palette textures
   stretch_sprite ( editorbuffer, texpal_wall (), 440, 292, 66, 66 );
   //stretch_sprite ( editorbuffer, texpal.tex[TexPal_IDX_WALL], 490, 292, 50, 50 );
   //stretch_sprite ( editorbuffer, texpal.tex[TexPal_IDX_DOOR], 490, 292, 50, 50 );
   stretch_sprite ( editorbuffer, texpal_floor(), 504, 292, 66, 66 );
   stretch_sprite ( editorbuffer, texpal_ceiling(), 570, 292, 66, 66 );
*/



}



int read_key ( s_cursor *cursor, s_tool *tool, s_viewport *viewport )
{
   int tmpkey = mainloop_readkeyboard();
   clear_keybuf();

   switch ( tmpkey )
   {
      //_____ Navigation Keys _____
      case KEY_RIGHT :
         if ( cursor->x < MAZE_SIZE - 1 )
         {  cursor->x++;
            if ( viewport->xscroll < ( cursor->x - VIEWPORT_SIZE +1 ) &&
               viewport->xscroll < MAZE_SIZE - VIEWPORT_SIZE +1 ) viewport->xscroll++;
         }
      break;

      case KEY_LEFT :
         if ( cursor->x > 0 )
         {  cursor->x--;
            if ( viewport->xscroll > cursor->x && viewport->xscroll > 0  ) viewport->xscroll--;
         }
      break;

      case KEY_UP :
         if ( cursor->y < MAZE_SIZE - 1 )
         {  cursor->y++;
            if ( viewport->yscroll < ( cursor->y - VIEWPORT_SIZE +1 ) &&
               viewport->yscroll < MAZE_SIZE - VIEWPORT_SIZE +1 )
               viewport->yscroll++;
         }
      break;

      case KEY_DOWN :
         if ( cursor->y > 0 )
         {  cursor->y--;
            if ( viewport->yscroll > cursor->y && viewport->yscroll > 0  ) viewport->yscroll--;
         }
      break;

      case KEY_PGUP :
         tool->selection_active = false;
         if ( cursor->z > 0 ) cursor->z--;
      break;

      case KEY_PGDN :
         tool->selection_active = false;
         if ( cursor->z < MAZE_DEPTH -1 ) cursor->z++;
      break;

      //_____ Mazemap dipswitch display keys )))

      case KEY_1: viewport->special = viewport->special ? false : true;
         break;

      /*bool special; //draw special flags
   bool filling; //draw filling
   bool events;  //draw event icons
   bool objects; //draw presence of objects
   bool masked;  //draw presence of masked texture*/

      //__________________________________ CODE REFACTORING ___________________________
      //TODO: call delete_tile(). Maybe put operations on the maze on a separate module
      //since it could potentially be called elsewhere. Also, maze is already exposed as global

/*      case KEY_DEL :  // solid trigger change key also erase content
         cmd_delete ();
      break;
*/

      //TODO: EDITING keys in separate functions
      /*case KEY_W :
         if ( p_tool == Editor_TOOL_TILE)
            cmd_wall ( Maze_2BIT_NORTH );
         else
         {
            if ( p_ycur < Maze_MAXWIDTH - 1 )
            {
               p_ycur++;
               if ( p_yscroll < ( p_ycur - 25 ) &&
                  p_yscroll < 100 - 25 )
                  p_yscroll++;

               if (p_tool == Editor_TOOL_CORRIDOR)
                  cmd_corridor ( Maze_2BIT_NORTH );
               if (p_tool == Editor_TOOL_BRUSH)
                  p_selection [ p_ycur ] [p_xcur] = true;
            }
         }


      break;

      case KEY_D :
         if ( p_tool == Editor_TOOL_TILE)
            cmd_wall ( Maze_2BIT_EAST );
         else
         {
            if ( p_xcur < Maze_MAXWIDTH - 1 )
            {
               p_xcur++;
               if ( p_xscroll < ( p_xcur - 25 ) &&
                  p_xscroll < 100 - 25 )
                  p_xscroll++;

               if (p_tool == Editor_TOOL_CORRIDOR)
                  cmd_corridor ( Maze_2BIT_EAST );
               if (p_tool == Editor_TOOL_BRUSH)
                  p_selection [ p_ycur ] [p_xcur] = true;
            }
         }
      break;


      case KEY_S :
         if ( p_tool == Editor_TOOL_TILE)
            cmd_wall ( Maze_2BIT_SOUTH );
         else
         {
            if ( p_ycur > 0 )
            {
               p_ycur--;
               if ( p_yscroll > ( p_ycur )  &&
                  p_yscroll > 0  )
                  p_yscroll--;

               if (p_tool == Editor_TOOL_CORRIDOR)
                  cmd_corridor ( Maze_2BIT_SOUTH );
               if (p_tool == Editor_TOOL_BRUSH)
                  p_selection [ p_ycur ] [p_xcur] = true;
            }
         }
      break;


      case KEY_A :
         if ( p_tool == Editor_TOOL_TILE)
            cmd_wall ( Maze_2BIT_WEST );
         else
         {
            if ( p_xcur > 0 )
            {
               p_xcur--;
               if ( p_xscroll > ( p_xcur )  &&
                  p_xscroll > 0  )
                  p_xscroll--;

               if (p_tool == Editor_TOOL_CORRIDOR)
                  cmd_corridor ( Maze_2BIT_WEST );
               if (p_tool == Editor_TOOL_BRUSH)
                  p_selection [ p_ycur ] [p_xcur] = true;
            }
         }
      break;
*/
      case KEY_F1 :
         /*menu_retval = menu_file();
         if ( menu_retval == Editor_MENU_EXIT)
         {

            //printf ("editor.start: should exit editor\n");
            exit_editor = true;
         }*/
         //TODO: TMPCODE
         tool->exit_editor = true;
      break;

      /*case KEY_F2 :
         menu_retval = menu_tool();
      break;

      case KEY_F3 :
         menu_retval = menu_texture();
      break;

      case KEY_F4 :
         menu_retval = menu_view();
      break;

      case KEY_F5 :
         menu_retval = menu_layer();
      break;

      case KEY_F6 :
         menu_retval = menu_error();
      break;

      case KEY_F7 :
         menu_retval = menu_data();
      break;

      case KEY_F8:
         menu_retval = menu_event();
      break;
*/
      // short cut keys
//TODO: Maybe turn off selection and activate content
/*
      //TODO: Number keys assigned to something else.
      case KEY_1:
         if ( p_selection_active == false )
            p_tool = Editor_TOOL_TILE;
      break;

      case KEY_2:
         if ( p_selection_active == false )
            p_tool = Editor_TOOL_CORRIDOR;
      break;
      case KEY_3:
         if ( p_selection_active == false )
            p_tool = Editor_TOOL_RECTANGLE;
      break;
      case KEY_4:
         if ( p_selection_active == false )
            p_tool = Editor_TOOL_BRUSH;
      break;

      case KEY_0:
         if ( p_selection_active == false )
            start_test_maze();
      break;

      case KEY_9:
         if ( p_selection_active == false )
            show_full_map();
      break;
*/
/*      case KEY_OPENBRACE:
         if ( tool->type == Editor_TOOL_TILE )
         {
            tmpal = get_mazetile_palette( mazetile [p_zcur][p_ycur][p_xcur]);
            if ( tmpal > 0)
            {
               tmpal--;
               set_mazetile_palette ( &mazetile [p_zcur][p_ycur][p_xcur], tmpal );
            }
         }
      break;

      case KEY_CLOSEBRACE:
         if ( tool->type == Editor_TOOL_TILE )
         {
            tmpal = get_mazetile_palette( mazetile [p_zcur][p_ycur][p_xcur]);
            if ( tmpal < Maze_NB_TEXPALETTE - 1)
            {
               tmpal++;
               set_mazetile_palette ( &mazetile [p_zcur][p_ycur][p_xcur], tmpal );
            }

         }
      break;
*/
      // Selection keys

      /*case KEY_Z:
         if ( p_selection_active == false)
         {

            if ( p_tool == Editor_TOOL_RECTANGLE )
            {
               p_xmark = p_xcur;
               p_ymark = p_ycur;
               p_selection_active = true;
               //printf ("Editor.start: Selection should be active\n");
            }
            else if ( p_tool == Editor_TOOL_BRUSH)
            {
               p_selection [p_ycur][p_xcur] = true;
               p_selection_active = true;
            }
         }
      break;*/

      /*case KEY_X:
         if ( p_selection_active == true )
         {

            if ( p_tool == Editor_TOOL_RECTANGLE )
            {
               p_xmark = 0;
               p_ymark = 0;
            }
            else if ( p_tool == Editor_TOOL_AREA)
            {
               // unselect all area

            }
            clear_selection();
            p_selection_active = false;
         }
      break;*/

      /*case KEY_R:
         if ( p_selection_active == true )
         {
            if ( p_tool == Editor_TOOL_RECTANGLE)
               rect_to_sel();

            menu_selection();

            if ( p_tool == Editor_TOOL_RECTANGLE )
            {
               // used to patch up a selection bug
               p_xmark = 0;
               p_ymark = 0;
               clear_selection();
               p_selection_active = false;
            }



            //p_xmark = 0;
            //p_ymark = 0;
            //p_selection = false;
         }
      break;*/

      /*case KEY_MINUS:
         set_mazetile_event ( &mazetile [ p_zcur][p_ycur][p_xcur], MAZETILE_NO_EVENT);
      break;

      case KEY_EQUALS:
         select_event();
      break;*/

   }

   return 0; //TODO: Not sure what to return, maybe selected menus, could centralise
   //the loop with dialogs in the main method instead of having multiple loops.
}


//TODO: file management, and clear maze
/*void _create_new_adventure ( String name )
{
}*/

//--------------------------------------------------------------------------//
//-                          Public Methods                                -//
//--------------------------------------------------------------------------//

void editor_main ( void )
{  s_cursor cursor;
   s_tool tool;
   s_viewport viewport;



   stop_midi();

   //TODO: Launch LOAD/NEW game dialogs module. Reuse manager open/close adventure, or some of the
   //subroutines since they are the same as manager (open savegame vs open adventure)

   _init_cursor ( &cursor );
   _init_tool (&tool);
   init_viewport( &viewport, VIEWPORT_SIZE, MAP_XOFFSET, MAP_YOFFSET );
   //editor_mode = MODE_LAYOUT;

   //TODO: move before loading or on new maze.
   //TODO: move to mazedit module
   _clear_maze();

   //TODO: TMPCODE
   load_mazefile( "adventure/DebuggingDemo/maze.bin");

   //TODO TESTCODE: Event dump
   /*for ( int j = 0 ; j < 20; j++)
      for ( int i = 0 ; i < 20; i++)
         printf ( "Event ID: X=%d, Y=%d, ID=%d, ID2=%d\n", i, j,
                 mazetile [0][j][i].event,
                 get_mazetile_event( mazetile [0][j][i]) );*/


   //TODO BUG: Cannot load while file not open, maybe force a file open to avoid bug
   //TODO LOAD texture palette whole palette on game open (related to save/load)

   while ( tool.exit_editor == false )
   {
      set_texpal_id( get_mazetile_palette( mazetile [cursor.z][cursor.y][cursor.x] ));

      clear ( background );

      _draw_background ( &cursor, &tool, &viewport );
     // draw_detailed_tile(); //TODO could change according to the mode, put int draw BG

      blit_background();

      copy_buffer();
      read_key( &cursor, &tool, &viewport );
   }

   //TODO: Emmergency save at user confirmation

}




/* ------------------------------------OLD CODE TO REFACTORISE ------------------------------


//asset references
//FONT* text_font = NULL;

//asset reference on icons, not sure if necessary
BITMAP *icon_event = NULL;
BITMAP *icon_marker_paint = NULL;
BITMAP *icon_marker_flag = NULL;
BITMAP *icon_special_start = NULL;
BITMAP *icon_cursor_square = NULL;
BITMAP *icon_cursor_arrow = NULL;
BITMAP *icon_cursor_angle = NULL;
BITMAP *icon_cursor_brush = NULL;
BITMAP *icon_wall_wall = NULL;
BITMAP *icon_wall_door = NULL;
BITMAP *icon_wall_grid = NULL;
BITMAP *icon_tile_light = NULL;
BITMAP *icon_tile_empty = NULL;
BITMAP *icon_tile_solid = NULL;
BITMAP *icon_tile_mbounce = NULL;




//TODO: used for menu, but will be changed.
const char STR_TEX_PALETTE [][25] =
{
   {"Wall"},
   {"Door"},
   {"Floor"},
   {"Ceiling"},
   {"Grid 01"},
   {"Grid 02"},
   {"Grid 03"},
   {"Grid 04"},

   {"Masked Wall 01"},
   {"Masked Wall 02"},
   {"Masked Wall 03"},
   {"Masked Wall 04"},
   {"Masked Wall 05"},
   {"Masked Wall 06"},
   {"Masked Wall 07"},
   {"Masked Wall 08"},

   {"Masked Wall 09"},
   {"Masked Wall 10"},
   {"Masked Wall 11"},
   {"Masked Wall 12"},
   {"Masked Wall 13"},
   {"Masked Wall 14"},
   {"Masked Wall 15"},
   {"Masked Wall 16"},

   {"Masked Floor/Ceiling 01"},
   {"Masked Floor/Ceiling 02"},
   {"Masked Floor/Ceiling 03"},
   {"Masked Floor/Ceiling 04"},
   {"Masked Floor/Ceiling 05"},
   {"Masked Floor/Ceiling 06"},
   {"Masked Floor/Ceiling 07"},
   {"Masked Floor/Ceiling 08"},

   {"Masked Floor/Ceiling 09"},
   {"Masked Floor/Ceiling 10"},
   {"Masked Floor/Ceiling 11"},
   {"Masked Floor/Ceiling 12"},
   {"Masked Floor/Ceiling 13"},
   {"Masked Floor/Ceiling 14"},
   {"Masked Floor/Ceiling 15"},
   {"Masked Floor/Ceiling 16"}


};



void initialise_editor_assets ( void )
{
   if ( icon_event == NULL ) icon_event = asset_editor("EVENT_BMP");
   if ( icon_marker_paint == NULL ) icon_marker_paint = asset_editor("MARKER_PAINT_BMP");
   if ( icon_marker_flag == NULL ) icon_marker_flag = asset_editor("MARKER_FLAG_BMP");
   if ( icon_special_start == NULL ) icon_special_start = asset_editor("SPECIAL_START_BMP");
   if ( icon_cursor_square == NULL ) icon_cursor_square = asset_editor("CURSOR_SQUARE_BMP");
   if ( icon_cursor_arrow == NULL ) icon_cursor_arrow = asset_editor("CURSOR_ARROW_BMP");
   if ( icon_cursor_angle == NULL ) icon_cursor_angle = asset_editor("CURSOR_ANGLE_BMP");
   if ( icon_cursor_brush == NULL ) icon_cursor_brush = asset_editor("CURSOR_BRUSH_BMP");
   if ( icon_wall_wall == NULL ) icon_wall_wall = asset_editor("WALL_WALL_BMP");
   if ( icon_wall_door == NULL ) icon_wall_door = asset_editor("WALL_DOOR_BMP");
   if ( icon_wall_grid == NULL ) icon_wall_grid = asset_editor("WALL_GRID_BMP");
   if ( icon_tile_light == NULL ) icon_tile_light = asset_editor("TILE_LIGHT_BMP");
   if ( icon_tile_empty == NULL ) icon_tile_empty = asset_editor("TILE_EMPTY_BMP");
   if ( icon_tile_solid == NULL ) icon_tile_solid = asset_editor("TILE_SOLID_BMP");
   if ( icon_tile_mbounce == NULL ) icon_tile_mbounce = asset_editor("TILE_MBOUNCE_BMP");
}


//-------------------------------------------------------------------------/
//-                      Constructor & Destructor                         -/
//-------------------------------------------------------------------------/

Editor::Editor ( void )
{
   //short i;
   //short j;
   //short k;
   //s_mazetile tile;

   //p_width = 40;
   //p_depth = 10;
   //p_sky = -1;
   p_rclevel = 0;
   p_startpos.x = 0;
   p_startpos.y = 0;
   p_startpos.z = 0;
   p_startpos.facing = Party_FACE_NORTH;

   p_xcur = 0;
   p_ycur = 0;
   p_zcur = 0;

   p_xscroll = 0;
   p_yscroll = 0;


   //tile.walltex = 0;
   //tile.floortex = 0;
   //tile.objectimg = 0;
   //tile.event = 0;
   //tile.special = 0;
   //tile.texture = 0;
   //tile.solid = 0;

   //tile.objectpic = 0; // 4 bit Floor Object, 4 bit ceiling object from palette
   //tile.wobjpalette = 0; // 3 bit w all object + 5 bit palette ID
   //tile.objposition = 0; // 2 bit wall + 2 bit ceiling + 4 bit floor
   //tile.masked = 0; // 4 bit Floor Ceiling texture + 4 bit wall texture
   //tile.maskposition = 0; //

   p_tool = 0;
   p_layer = 0;
   p_xmark = 0;
   p_ymark = 0;
   p_selection_active = false;
   p_active_palette = 0;
   p_adventure_selected = false;
   strcpy ( p_adventure_databasefile, "");
   strcpy ( p_adventure_mazefile, "");

   clear_selection();

*/
/*

   k = 0;
   j = 0;
   for ( k = 0 ; k < Maze_MAXDEPTH; k++ )
       for ( j = 0 ; j < Maze_MAXWIDTH; j++ )
       {
         for ( i = 0 ; i < Maze_MAXWIDTH; i++ )
         {
            p_map [ i ] [ j ] [ k ] = tile;
         }
       }*/
/*
//   p_map = create_bitmap ( 640, 480 );


}

Editor::~Editor ( void )
{

}


//-------------------------------------------------------------------------/
//-                            Methods                                    -/
//-------------------------------------------------------------------------/



void Editor::start_test_maze ( void )
{
   party.position ( p_xcur, p_ycur, p_zcur, Party_FACE_NORTH );
   //maze.light ( Maze_LIGHT_LEVEL_3 );
   //maze.load_hardcoded_texture();

   set_texpal_id( get_mazetile_palette ( mazetile [p_zcur][p_ycur][p_xcur] ));
//   mazepal.load ( get_mazetile_palette ( mazetile [p_zcur][p_ycur][p_xcur] ));
//   mazepal.build();

   maze.start( true );
}

int Editor::menu_file ( void )
{
   int answer;
   int answer2;
   Menu mnu_file;
   int retval = Editor_MENU_NONE;
   bool hidden=false;
   int dberror;
   int mazesuccess;

   if ( p_adventure_selected == false )
      hidden = true;

   mnu_file.add_item (0, "Select Adventure");
   mnu_file.add_item (1, "Load Adventure", hidden);
   mnu_file.add_item (2, "Save Adventure", hidden);
   mnu_file.add_item (3, "Exit");

   WinMenu wmnu_file ( mnu_file, 0, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   if (answer == 0)
      select_adventure();
   else if ( answer == 1 )
   {
      //WinQuestion wqst_exit ("Are you sure you want to exit the editor?\nUnsaved data will be lost");
      WinQuestion wqst_load ("Are you sure you want to reload your adventure data?\nAll changes will be lost.");
      blit_editorbuffer();
      answer2 = Window::show_all();
      if ( answer2 == WinQuestion_ANSWER_YES)
      {
         SQLclose();
         mazesuccess = maze.load_from_mazefile (p_adventure_mazefile);
         dberror = SQLopen ( p_adventure_databasefile );

         if ( mazesuccess == false || dberror != SQLITE_OK )
         {
            WinMessage wmsg_errorload ( "Error loading adventure\nResetting Content" );
            blit_editorbuffer();
            Window::show_all();
            clear_maze();

            p_adventure_selected = false;
         }
         else
         {
            p_zcur = 0;
            p_xcur = 0;
            p_ycur = 0;
            p_xscroll = 0;
            p_yscroll = 0;
            //set_texpal_id( get_mazetile_palette ( mazetile [p_zcur][p_ycur][p_xcur] ));
            printf("\nEditor:Menu_file: debug");
            //mazepal.load ( mazepalid );
            //mazepal.build();
            //TODO: revise code here, since now only use 1 palette shared by the editor and maze
            //editorpal.load ( 0 );
            set_texpal_id( 0);
            //editorpal.build();
            //palsample.load_build();
         }
      }
   }
   else if ( answer == 2)
   {
         mazesuccess = maze.save_to_mazefile (p_adventure_mazefile);
         //dberror = SQLcommit();

         if ( mazesuccess == false )
         {
             WinMessage wmsg_errorsave ( "Error Saving Maze\nContent still in memory" );
               blit_editorbuffer();
               Window::show_all();
         }
//         else if ( dberror != SQLITE_OK )
//            {
//printf ("Editor: Sql error = %d", dberror);
//               WinMessage wmsg_errorsave ( "Error Saving Database\nContent still in memory" );
//               blit_editorbuffer();
//               Window::show_all();
//            }
   }
   else if ( answer == 3 )
   {
         WinQuestion wqst_exit ("Are you sure you want to exit the editor?\nUnsaved data will be lost");
         blit_editorbuffer();
         answer2 = Window::show_all();
         if ( answer2 == WinQuestion_ANSWER_YES)
         {
            SQLclose();
            clear_maze();
            p_adventure_selected = false;
            retval = Editor_MENU_EXIT;
         }
   }

   return ( retval );
}

int Editor::menu_tool ( void )
{
   int answer;
   //int answer2;
   Menu mnu_file;
   int retval = Editor_MENU_NONE;

   if ( p_tool == Editor_TOOL_TILE )
      mnu_file.add_item (Editor_TOOL_TILE, "* Tile (1)", true);
   else
      mnu_file.add_item (Editor_TOOL_TILE, "  Tile (1)", p_selection_active );

   if ( p_tool == Editor_TOOL_CORRIDOR )
      mnu_file.add_item (Editor_TOOL_CORRIDOR, "* Corridor (2)", true);
   else
      mnu_file.add_item (Editor_TOOL_CORRIDOR, "  Corridor (2)", p_selection_active );

if ( p_tool == Editor_TOOL_RECTANGLE )
      mnu_file.add_item (Editor_TOOL_RECTANGLE, "* Rectangle (3)", true);
   else
      mnu_file.add_item (Editor_TOOL_RECTANGLE, "  Rectangle (3)", p_selection_active);

if ( p_tool == Editor_TOOL_BRUSH )
      mnu_file.add_item (Editor_TOOL_BRUSH, "* Area (4)", true);
   else
      mnu_file.add_item (Editor_TOOL_BRUSH, "  Area (4)", p_selection_active );

   WinMenu wmnu_file ( mnu_file, 72, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   if ( answer != -1 )
      p_tool = answer;

   return ( retval );
}

int Editor::menu_texture ( void )
{
   int answer;
   int answer2 = 0;
   Menu mnu_texture;
   int retval = Editor_MENU_NONE;
   WinTitle wttl_loadpalette ("Loading Palette", 320, 200);
   wttl_loadpalette.hide();
   bool hidden = false;

   if ( p_adventure_selected == false )
      hidden = true;
   else
      hidden = p_selection_active;

   mnu_texture.add_item (0, "Active Palette", hidden);
   mnu_texture.add_item (1, "Edit Palette", hidden);

   WinMenu wmnu_texture ( mnu_texture, 150, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   switch ( answer )
   {
      case 0:
         wmnu_texture.hide();
         answer2 = select_palette();
         if ( answer2 != -1)
         {
            p_active_palette = answer2;
            wttl_loadpalette.unhide();
            blit_editorbuffer();
            Window::draw_all();
            printf("\nEditor:Menu_texture: debug");
            //TODO: Not sure if must change active palette for the editor, auto change according
            //to position. Direct access could be more convenient than switching.
            set_texpal_id(answer2);
            //editorpal.load (answer2);
            //editorpal.build();

         }

      break;
      case 1:
         while ( answer2 != -1)
         {
            wmnu_texture.hide();
            answer2 = select_active_palette_texture();
            if ( answer2 != -1)
            {
//printf ("Debug:Editor:Start: Before call edit pal=%d, tex=%d\n", p_active_palette, answer2 );
               //TODO: code disabled but think related to texture editing.
               //editorpal.tex [ answer2].edit ( p_active_palette, answer2 );
               //editorpal.build();
            }
         }

         //palsample.load_build();
      break;
   }

   return ( retval );



}

int Editor::menu_view ( void )
{
   int answer;
   //int answer2;
   Menu mnu_file;
   int retval = Editor_MENU_NONE;

   mnu_file.add_item (0, "Full Map (9)", p_selection_active);
   mnu_file.add_item (1, "Maze View (0)", p_selection_active);

   WinMenu wmnu_file ( mnu_file, 216, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   switch ( answer )
   {
      case 0:
         wmnu_file.hide();
         show_full_map ();
      break;
      case 1:
         wmnu_file.hide();
         start_test_maze();
      break;
   }

   return ( retval );


}

int Editor::menu_layer ( void )
{
   return ( Editor_MENU_NONE );
}

int Editor::menu_error ( void )
{
   return ( Editor_MENU_NONE );
}

int Editor::menu_data ( void )
{
   return ( Editor_MENU_NONE );
}

int Editor::menu_event ( void )
{
   int answer;
   Menu mnu_event;
   //int retval = Editor_MENU_NONE;
   bool hidden = false;

   if ( p_adventure_selected == false )
      hidden = true;

   mnu_event.add_item (0, "Mark Event (+=)", hidden);
   mnu_event.add_item (1, "Unmark Event (-_)", hidden);

   WinMenu wmnu_event ( mnu_event, 400, 16 );

   blit_editorbuffer();
   answer = Window::show_all();

   wmnu_event.hide();

   switch ( answer )
   {
      case 0:
         select_event ();
      break;
      case 1:
         set_mazetile_event( &mazetile [p_zcur][p_ycur][p_xcur], MAZETILE_NO_EVENT );
      break;
   }

   return ( Editor_MENU_NONE);
}

int Editor::menu_selection ( void )
{
   int answer;
   int answer2;
   Menu mnu_file ("Selection Menu");
   int retval = Editor_MENU_NONE;
   int i;
   int tmpal;

   mnu_file.add_item (0, "Make a room");
   mnu_file.add_item (1, "Set a Filling" );
   mnu_file.add_item (2, "Set as Magic Bounce ");
   mnu_file.add_item (3, "Set as Light");
   mnu_file.add_item (4, "Change Palette" );
   mnu_file.add_item (5, "Set masked floor texture", false );
   mnu_file.add_item (6, "Set masked ceiling texture", false );
   mnu_file.add_item (7, "Delete");
   mnu_file.add_item (8, "Remove Special and Fillings");

   mnu_file.add_item (-1, "Do Nothing" );

   WinMenu wmnu_file ( mnu_file, 48, 48 );

   blit_editorbuffer();
   answer = Window::show_all();

   //if ( answer == 0)

   wmnu_file.hide();

   if ( answer == 0)
      sel_build_room ();

   if ( answer == 1)
   {
      Menu mnu_filling ("Select Filling");

      for ( i = 0 ; i < Maze_NB_FILLING ; i++ )
      {
         mnu_filling.add_item ( i, STR_EDT_SPECIAL_FILL [i] );
      }
      WinMenu wmnu_filling ( mnu_filling, 48, 48, true );

      blit_editorbuffer();
      answer2 = Window::show_all();

      sel_filling ( answer2 );
   }

   if ( answer == 2)
      sel_special ( MAZETILE_SPECIAL_MAGIKBOUNCE);

   if ( answer == 3)
      sel_special ( MAZETILE_SPECIAL_LIGHT);

   if ( answer == 4)
   {
      tmpal = select_palette ();

      if ( tmpal != -1 )
         sel_palette ( tmpal );
   }

   if ( answer == 5 || answer == 6)
   {

      List lst_texture ("Select the texture?", 16, true );
      //TODO: must be fixed.
      //WinData<BITMAP> wdat_texture ( WDatProc_texture_bitmap,
//
//                                     *(editorpal.bmp[TexPal_IDX_MFLOOR]),
//                                 WDatProc_POSITION_TEXTURE  );

      answer2 = 0;
      for ( i = 0; i < TEXTUREPAL_NB_MASKEDFLOOR; i++)
      {
         // need to offset the selected texture because flor ceiling textures goes from 24 to 39
         //TODO: fix with the new user interface.
         //lst_texture.add_item ( i,STR_TEX_PALETTE [i + TexPal_IDX_MFLOOR]);
      }

      WinList wlst_texture ( lst_texture, 20, 40 );

      while ( lst_texture.selected() == false && answer2 != -1 )
      {
         blit_editorbuffer();
         answer2 = Window::show_all();

         if ( answer2 != -1 )
         {
            //Commented, linked to the old window system.
            //wdat_texture.parameter ( *(editorpal.bmp [ answer2 ]) );
            //Window::refresh_all();
         }

      }

      if ( lst_texture.selected() == true )
      {
         if ( answer == 5)
            sel_masked ( answer2, MAZETILE_MASKTEXPOS_FLOOR );
         else
            sel_masked ( answer2, MAZETILE_MASKTEXPOS_CEILING );
      }

   }

   if ( answer == 7)
      sel_delete ();

   if ( answer == 8)
      sel_remove_special ();



   return ( retval );
}


void Editor::cmd_wall ( int side ) // use maze 2bit
{
   unsigned char tmpwall = get_mazetile_wall( mazetile [p_zcur][p_ycur][p_xcur], side);

   // Cycle trought the valid wall types
   tmpwall++;
   if ( tmpwall > MAZETILE_SOLIDTYPE_DOOR )
   {   tmpwall = 0;
   }

   set_mazetile_wall ( &mazetile [p_zcur][p_ycur][p_xcur], side, tmpwall );

   unset_mazetile_special ( &mazetile [p_zcur][p_ycur][p_xcur], MAZETILE_SPECIAL_SOLID );

}

void Editor::cmd_delete ( void )
{
   delete_tile( p_zcur, p_ycur, p_xcur);
}

void Editor::cmd_corridor ( int direction ) // use maze 2bit constants
{
   //int zsrc = p_zcur;
   //int ysrc = p_ycur;
   //int xsrc = p_xcur;
   //unsigned int tmpsolid;
   //int i;
   //int walltype;

   //switch ( direction )
   //{
   //   case Maze_2BIT_NORTH: ysrc--; break;
   //   case Maze_2BIT_EAST: xsrc--; break;
   //   case Maze_2BIT_SOUTH: ysrc++; break;
   //   case Maze_2BIT_WEST: xsrc++; break;
   //}

   // remove solid

   unset_mazetile_special( &mazetile [p_zcur][p_ycur][p_xcur], MAZETILE_SPECIAL_SOLID );

   // set_active_palette

   set_mazetile_palette( &mazetile [p_zcur][p_ycur][p_xcur], p_active_palette );


   // remove the wall from where you came from

   switch ( direction )
   {
      case Maze_2BIT_NORTH:
         set_mazetile_wall( &mazetile [p_zcur][ p_ycur-1][ p_xcur], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
      case Maze_2BIT_SOUTH:
         set_mazetile_wall ( &mazetile [p_zcur][ p_ycur+1][ p_xcur], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
      case Maze_2BIT_EAST:
         set_mazetile_wall ( &mazetile [p_zcur][ p_ycur][ p_xcur-1], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
      case Maze_2BIT_WEST:
         set_mazetile_wall ( &mazetile [p_zcur][ p_ycur][ p_xcur+1], direction
            , MAZETILE_SOLIDTYPE_NONE);
      break;
   }



   // Set copies of wall from each non-solid area

   patch_surrounding_walls( p_zcur, p_ycur, p_xcur );



   //mazetile [p_zsrc][p_ysrc][p_xsrc].special =
   //   ( mazetile [p_zcur][p_ycur][p_xcur].special & ~MAZETILE_SPECIAL_SOLID) ;


   // side walls in paralell to the direction
*/
   /*switch (direction)
   {
      case Maze_2BIT_NORTH :
      case Maze_2BIT_SOUTH :
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_WEST_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_WEST_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_EAST_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_EAST_WALL);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_WEST);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_EAST);

         if ( p_ycur < Maze_MAXWIDTH -1 )
            if ( ( mazetile [ p_zcur][ p_ycur+1][ p_xcur].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur+1, p_xcur, Maze_2BIT_SOUTH);
         if ( p_ycur > 0 )
            if ( ( mazetile [ p_zcur][ p_ycur-1][ p_xcur].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur-1, p_xcur, Maze_2BIT_NORTH);
      break;

      case Maze_2BIT_EAST :
      case Maze_2BIT_WEST :
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_NORTH_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_NORTH_WALL);
         //tmpsolid = ( mazetile [p_zcur][p_ycur][p_xcur].solid & ~MAZETILE_SOLID_SOUTH_MASK );
         //   mazetile [p_zcur][p_ycur][p_xcur].solid = (tmpsolid | MAZETILE_SOLID_SOUTH_WALL);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_NORTH);
         set_wall ( p_zcur, p_ycur, p_xcur, Maze_2BIT_SOUTH);

         if ( p_xcur < Maze_MAXWIDTH -1 )
            if ( ( mazetile [ p_zcur][ p_ycur][ p_xcur+1].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur, p_xcur+1, Maze_2BIT_WEST);
         if ( p_xcur > 0 )
            if ( ( mazetile [ p_zcur][ p_ycur][ p_xcur-1].special & MAZETILE_SPECIAL_SOLID ) == 0 )
               set_wall ( p_zcur, p_ycur, p_xcur-1, Maze_2BIT_EAST);
      break;
   }*/

/*
}

void Editor::patch_surrounding_walls ( int z, int y, int x )
{
   //unsigned int tmpsolid;
   //int i;
   int walltype;

   //TODO: See if can make a loop from 0 to 3
   // test north to set or not walls
   if ( is_mazetile_special( mazetile [z][y+1][x], MAZETILE_SPECIAL_SOLID) == true
       || y == Maze_MAXWIDTH -1)
      set_mazetile_wall( &mazetile [z][y][x], Maze_2BIT_NORTH, MAZETILE_SOLIDTYPE_WALL);
   else
   {
      walltype = get_mazetile_wall( mazetile [z][y+1][x], Maze_2BIT_SOUTH );

      set_mazetile_wall( &mazetile [z][y][x], Maze_2BIT_NORTH, walltype );
   }

   //south
   if ( is_mazetile_special( mazetile [z][y-1][x], MAZETILE_SPECIAL_SOLID) == true
       || y == 0)
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_SOUTH, MAZETILE_SOLIDTYPE_WALL);
   else
   {
      walltype = get_mazetile_wall( mazetile [z][y-1][x], Maze_2BIT_NORTH );

      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_SOUTH, walltype );
   }

   //east
   if ( is_mazetile_special( mazetile [z][y][x+1], MAZETILE_SPECIAL_SOLID) == true
       || x == Maze_MAXWIDTH - 1)
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_EAST, MAZETILE_SOLIDTYPE_WALL);
   else
   {
      walltype = get_mazetile_wall ( mazetile [z][y][x+1], Maze_2BIT_WEST);

      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_EAST, walltype );

   }

   //west
   if ( is_mazetile_special( mazetile [z][y][x-1], MAZETILE_SPECIAL_SOLID) == true
       || x == 0 )
      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_WEST, MAZETILE_SOLIDTYPE_WALL );
   else
   {
      walltype = get_mazetile_wall ( mazetile [z][y][x-1], Maze_2BIT_EAST);

      set_mazetile_wall ( &mazetile [z][y][x], Maze_2BIT_WEST, walltype );

   }


}

void Editor::rect_to_sel ( void )
{
   bool xok;
   bool yok;
   int i;
   int j;

   //printf ("xmark=%d, ymark=%d, xcur=%d, ycur=%d\n", p_xmark, p_ymark, p_xcur, p_ycur);

   clear_selection();

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {
         xok = false;
         yok = false;

         if ( p_xmark < p_xcur )
         {
            if ( i >= p_xmark && i <= p_xcur)
               xok = true;
         }
         else
            if ( i >= p_xcur && i <= p_xmark)
               xok = true;

         if ( p_ymark < p_ycur )
         {
            if ( j >= p_ymark && j <= p_ycur)
               yok = true;
         }
         else
            if ( j >= p_ycur && j <= p_ymark)
               yok = true;

         if ( xok == true && yok == true)
         {
            p_selection [j][i] = true;
         }
      }
   }

}

void Editor::clear_selection ( void )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {
         //if ( is_tile_special ( p_zcur, j, i, MAZETILE_SPECIAL_SELECTION ))
         //{
            p_selection [j][i] = false;
         //}

      }
   }
}


void Editor::sel_build_room ( void )
{

   int i;
   int j;
   s_mazetile tmptile = new_mazetile_empty();
   //int k;


   set_mazetile_palette( &tmptile, p_active_palette );


   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
            mazetile [ p_zcur][j][i] = tmptile;
         }
      }
   }

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
            patch_surrounding_walls( p_zcur, j, i);
         }
      }
   }

}
void Editor::sel_filling ( int fill )
{
   int i;
   int j;
   //unsigned char tmptile;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true && is_mazetile_special ( mazetile [p_zcur][j][i],
            MAZETILE_SPECIAL_SOLID) == false)
         {
            set_mazetile_filling( &mazetile[p_zcur][j][i], fill );

         }

      }
   }

}
void Editor::sel_special ( unsigned int special )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true && is_mazetile_special( mazetile[p_zcur][j][i],
            MAZETILE_SPECIAL_SOLID) == false)
         {
            set_mazetile_special ( &mazetile [p_zcur][j][i], special);
         }

      }
   }

}

void Editor::sel_remove_special ( void )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true && is_mazetile_special ( mazetile [p_zcur][j][i],
            MAZETILE_SPECIAL_SOLID) == false)
         {
            unset_mazetile_special( &mazetile [p_zcur][j][i], MAZETILE_SPECIAL_ALL );
            set_mazetile_filling( &mazetile [p_zcur][j][i], MAZETILE_FILLING_NONE );
         }

      }
   }

}


void Editor::sel_palette ( int palette )
{
   int i;
   int j;
   //int tmpwobj;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
            set_mazetile_palette( &mazetile [ p_zcur][j][i], palette );

         }

      }
   }
}
void Editor::sel_masked ( int texid, unsigned int location  )
{
   int i;
   int j;
   //int backup_masked;
   //int backup_position;
 //  int mask;

   //printf ("\nEditor.sel_masked: Texture id = %d, location=%d\n", texid, location );

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true )
         {
    //        if ( location == Maze_TILE_MPOS_FLOOR )
      //         mask =

            //backup_masked = (mazetile [p_zcur][j][i].masked & ~MAZETILE_MASKTEX_FLOOR_MASK );
            //printf ("Editor.sel_masked: before %d ", mazetile [p_zcur][j][i].masked);
            //mazetile [p_zcur][j][i].maskposition =
            //   ( mazetile [p_zcur][j][i].maskposition | location );
            set_mazetile_maskedposition( &mazetile[p_zcur][j][i], location);
            //extract wobj first
            set_mazetile_maskedfloortex( &mazetile [ p_zcur][j][i], texid );
            //mazetile [ p_zcur][j][i].masked =
            //   backup_masked + ( texid << 4 );
            //printf (": after %d \n", mazetile [p_zcur][j][i].masked);
         }

      }
   }




 //#define Maze_TILE_MPOS_FLOOR    0b10000000 // 128 // 01000000
 //#define Maze_TILE_MPOS_CEILING  0b01000000 // 64  // 10000000


//#define Maze_TILE_MASKED_FLOOR_MASK 0b11110000 //240 // 11110000
//#define Maze_TILE_MASKED_WALL_MASK  0b00001111 //15  // 00001111
//unsigned char masked; // 4 bit Floor Ceiling texture + 4 bit wall texture
//unsigned char maskposition; // 2 bit floor ceiling + 4 bit wall position + 2 bit grid tex

}
void Editor::sel_delete ( void )
{
   int i;
   int j;

   for ( j = 0 ; j < Maze_MAXWIDTH ; j++)
   {
      for ( i = 0 ; i < Maze_MAXWIDTH ; i++ )
      {

         if ( p_selection[j][i] == true)
         {
            delete_tile (p_zcur, j, i );
         }

      }
   }

}

void Editor::delete_tile ( int z, int y, int x )
{
   //unsigned int tmpsolid;


   //printf ("passed through delete tile: z=%d, y=%d, x=%d\n", z, y, x );

   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {



      set_mazetile_empty( &mazetile [z][y][x] );
      set_mazetile_special( &mazetile [z][y][x], MAZETILE_SPECIAL_SOLID);

   // check for suroundings


   // north
   //if ( y < Maze_MAXWIDTH - 1)
      if ( is_mazetile_special ( mazetile [z][y+1][x], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y+1][x], Maze_2BIT_SOUTH, MAZETILE_SOLIDTYPE_WALL);

      }
   // south
   //if ( y > 0)
      if ( is_mazetile_special ( mazetile [z][y-1][x], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y-1][x], Maze_2BIT_NORTH, MAZETILE_SOLIDTYPE_WALL);

      }
   // east
   //if ( x < Maze_MAXWIDTH - 1)
      if ( is_mazetile_special ( mazetile [z][y][x+1], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y][x+1], Maze_2BIT_WEST, MAZETILE_SOLIDTYPE_WALL);

      }
   // west
   //if ( x > 0 )
      if ( is_mazetile_special ( mazetile [z][y][x-1], MAZETILE_SPECIAL_SOLID) == false )
      {
         set_mazetile_wall ( &mazetile [z][y][x-1], Maze_2BIT_EAST, MAZETILE_SOLIDTYPE_WALL);

      }
   }
}
*/
/*void Editor::set_wall ( int z, int y, int x, int direction)
{
   unsigned char tmpsolid;


   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {


   tmpsolid = ( mazetile [z][y][x].solid & ~Editor_POLYGON_INFO[direction].mask );
         mazetile [z][y][x].solid = (tmpsolid | Editor_POLYGON_INFO[direction].wall);
   }
}*/

/*void Editor::set_wall_type ( int z, int y, int x, int side, int type )
{
   unsigned char tmpsolid;

   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {
      tmpsolid = ( mazetile [z][y][x].solid & ~Editor_POLYGON_INFO[side].mask );

      mazetile [z][y][x].solid = (tmpsolid | ( type << Editor_POLYGON_INFO[side].shift) );
   }
}*/

/*void Editor::remove_wall ( int z, int y, int x, int direction)
{
   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {
      mazetile [z][y][x].solid = ( mazetile [z][y][x].solid & ~Editor_POLYGON_INFO[direction].mask );
   }
}*/

/*bool Editor::is_tile_special ( int z, int y, int x, unsigned int special )
{
    if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
    {
      if ( ( mazetile [ z][ y][ x].special & special ) == special )
         return ( true );
      else
         return ( false );
    }
    else
       return ( false );
}*/

/*bool Editor::is_tile_selected ( int z, int y, int x )
{

}*/

/*void Editor::remove_tile_special ( int z, int y, int x, unsigned int special )
{
   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {
      mazetile [z][y][x].special = ( mazetile [z][y][x].special & ~special );
   }
}

void Editor::set_tile_special ( int z, int y, int x, unsigned int special )
{
   if ( x >= 0 && x < Maze_MAXWIDTH && y >= 0 && y < Maze_MAXWIDTH && z >= 0 && z < Maze_MAXDEPTH )
   {


      mazetile [z][y][x].special = ( mazetile [z][y][x].special | special );
   }
}*/


/*int Editor::get_palette ( int z, int y, int x )
{
   return ( mazetile [z][y][x].wobjpalette & MAZETILE_PALETTE_MASK );
}

void Editor::set_palette ( int z, int y, int x, int palette)
{
   int tmpobj = mazetile [z][y][x].wobjpalette & ~MAZETILE_PALETTE_MASK;

   mazetile [z][y][x].wobjpalette = tmpobj + palette;

}*/

/*
void Editor::load_maz_file ( char *filename )
{
   int x;
   int y;
   int z;
   int dummy;
   s_mazetile tmptile;
   fs_maz_header mazheader;
   FILE* maz_file;

   //tmptile.walltex = 0;
   //tmptile.floortex = 0;
   //tmptile.objectimg = 0;
   tmptile.event = 0;
   tmptile.special = 0;
   //tmptile.texture = 0;
   tmptile.solid = 0;
   tmptile.objectpic = 0; // * 4 bit Floor Object, 4 bit ceiling object from palette
   tmptile.wobjpalette = 0; // * 3 bit wall object + 5 bit palette ID
   tmptile.objposition = 0; // * 2 bit wall + 2 bit ceiling + 4 bit floor
   tmptile.masked = 0; // * 4 bit Floor Ceiling texture + 4 bit wall texture
   tmptile.maskposition = 0; //

   // clean current maze
   for ( z = 0 ; z < Maze_MAXDEPTH; z++ )
       for ( y = 0 ; y < Maze_MAXWIDTH; y++ )
       {
         for ( x = 0 ; x < Maze_MAXWIDTH; x++ )
         {
            mazetile [ z ] [ y ] [ x ] = tmptile;
         }
       }

   if ( exists ( filename ) != 0 )
   {
      maz_file = fopen ( filename, "rb" );

      if ( maz_file != NULL )
      {
         dummy = fread ( &mazheader, sizeof ( fs_maz_header ), 1, maz_file );

         p_width = mazheader.f_width;
         p_depth = mazheader.f_depth;
         p_sky = mazheader.f_sky;
         p_rclevel = mazheader.f_rclevel;
         p_startpos.x = mazheader.f_startx;
         p_startpos.y = mazheader.f_starty;
         p_startpos.z = mazheader.f_startz;
         p_startpos.facing = mazheader.f_startface;
   //      mazheader.f_reserve;

         for ( z = 0 ; z < p_depth ; z++ )
            for ( y = 0 ; y < p_width ; y++ )
               for ( x = 0 ; x < p_width ; x++ )
               {
                  dummy = fread ( &tmptile, sizeof ( s_mazetile ), 1, maz_file );
                  mazetile [ z ] [ y ] [ x ] = tmptile;
               }
         fclose ( maz_file );
         textout_old ( buffer, font, "Passed corectly", 10, 10, General_COLOR_TEXT );
      }
      else
         textout_old ( buffer, font, "fopen error", 10, 10, General_COLOR_TEXT );
   }
   else
      textout_old ( buffer, font, "exist error", 10, 10, General_COLOR_TEXT );

   copy_buffer();
   while ( mainloop_readkeyboard() != KEY_ENTER );
}

void Editor::save_maz_file ( char *filename )
{
   fs_maz_header mazheader;
   s_mazetile tmptile;
   FILE* maz_file;
   int x;
   int y;
   int z;

   mazheader.f_width = p_width;
   mazheader.f_depth = p_depth;
   mazheader.f_sky = p_sky;
   mazheader.f_rclevel = p_rclevel;
   mazheader.f_startx = p_startpos.x;
   mazheader.f_starty = p_startpos.y;
   mazheader.f_startz = p_startpos.z;
   mazheader.f_startface = p_startpos.facing;
   mazheader.f_reserved = 0;

   maz_file = fopen ( filename, "wb" );

   if ( maz_file != NULL )
   {

      fwrite ( &mazheader, sizeof ( fs_maz_header ), 1, maz_file );

      for ( z = 0 ; z < p_depth ; z++ )
         for ( y = 0 ; y < p_width ; y++ )
            for ( x = 0 ; x < p_width ; x++ )
            {
               tmptile = mazetile [ z ] [ y ] [ x ];
               fwrite ( &tmptile, sizeof ( s_mazetile ), 1, maz_file );
            }
      fclose ( maz_file );
   }

}

*/
/*-------------------------------------------------------------------------*/
/*-                           Private Methods                             -*/
/*-------------------------------------------------------------------------*/

/*void Editor::copy_background ( void )
{
   blit ( backup, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   clear_bitmap (backup);
}*/

/*

//TODO refactor this method and the way the preview is drawn on screen considering rotations
//will eventually be handled.



void Editor::select_adventure ( void )
{

   List lst_file ("Select adventure directory to edit ( adventure// )", 20 );
   //char tmpstr [ Manager_TMP_STR_LEN ];
   int i;
   int answer;
   //short answer2;
   int retval;
   char filelist [ Manager_MAX_NB_FILE ] [ Manager_FILENAME_LEN ];
   int flistindex;
//   string tmpfilename;
   //char strfname [ 25 ];
   al_ffblk info;
   //char tmpsavename [Manager_FILENAME_LEN];
   //int savenumber = 0;
   int fileID;
   //struct tm *timeptr;
   //time_t loctime;
   //char tmpstr_database [ Manager_TMP_STR_LEN + 20];
   //char tmpstr_maze [ Manager_TMP_STR_LEN + 20];
   int error;
   int success;

   retval = al_findfirst("adventure//*", &info, FA_ALL  );

   flistindex = 0;
   if ( retval == 0 )
   {
      if ((info.attrib & FA_DIREC) == FA_DIREC && info.name[0] != '.')
      {
         strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
         flistindex++;
      }
      while ( al_findnext ( &info ) == 0 && flistindex < Manager_MAX_NB_FILE)
      {
         if ((info.attrib & FA_DIREC) == FA_DIREC && info.name[0] != '.')
         {
            strncpy ( filelist [ flistindex ], info.name, Manager_FILENAME_LEN );
            flistindex++;
         }
      }
      al_findclose( &info);
   }

   if ( flistindex > 0)
   {
      for ( i = 0 ; i < flistindex ; i++ )
      {
         lst_file.add_item ( i, filelist [ i ] );
      }

      WinList wlst_file ( lst_file, 20, 40 );
      answer = Window::show_all();
      wlst_file.hide();

      if ( answer != -1 )
      {

         fileID = answer;
         sprintf (p_adventure_databasefile, "adventure//%s//database.sqlite", filelist [ fileID ] );
         sprintf (p_adventure_mazefile, "adventure//%s//maze.bin", filelist [ fileID ] );

         clear_maze();
         success = maze.load_from_mazefile ( p_adventure_mazefile);

         if ( success == true)
         {
            SQLclose();
            error = SQLopen (p_adventure_databasefile);

            if ( error == SQLITE_OK)
            {

               p_zcur = 0;
               p_xcur = 0;
               p_ycur = 0;
               p_xscroll = 0;
               p_yscroll = 0;
               //mazepalid = get_mazetile_palette( mazetile [p_zcur][p_ycur][p_xcur] );
               printf("\nEditor:select_adventure: debug");
               //mazepal.load ( mazepalid );
               //mazepal.build();
               //TODO: FIX use only 1 palette
               set_texpal_id( 0 );
               //editorpal.load ( 0 );
               //editorpal.build();
               //palsample.load_build();


               p_adventure_selected = true;
            }
            else
            {
               WinMessage wmsg_error ( "Error loading database.sqlite\n" );
               Window::show_all();
               clear_maze();
               p_adventure_selected = false;
            }

         }
         else
            {
               WinMessage wmsg_error ( "Error loading maze.bin" );
               Window::show_all();
            }

         //Window::draw_all();
         //textprintf_centre_old ( buffer, text_font, 320, 200, General_COLOR_TEXT,
         //   "Reading %s", filelist [ fileID ] );
         //textout_centre_old ( buffer, text_font, "Please Wait", 320, 220, General_COLOR_TEXT );
         //copy_buffer();

         //tmpobj = load_datafile ( filelist [ answer ] );
         // start loading DATAFILE
         //sprintf ( strfname,"adventur//%s", filelist [ answer ] );
         //tmpobj = load_datafile_object( strfname , "DBA_ADATABASE" );

         //fake dbloading
         //retval = SQLopen ("wizardry.sqlite");



      }

   }

}

void Editor::select_event ( void )
{
   //??to do: select event from the list and set mazetile[p_zcur][y][x] to event ID
   // do not select event > 255. 0 = no event.
   // events are absolute, there is not a different list for each palete
   // but even can also be used in diffent place. Ex a stair up can be used for all start up events.
   int error;
   int answer;
   List lst_event ("Select the event you want ot trigger?", 20 );
   Event tmpevent;
   int showlist = true;

   error = tmpevent.SQLprepare ("WHERE pk >= 1 AND pk <= 255");

   if ( error == SQLITE_OK)
   {
      error = tmpevent.SQLstep();

      while ( error == SQLITE_ROW )
      {
         lst_event.add_itemf (tmpevent.primary_key(), "%3d-WHEN %s IF %s PASS, THEN %s ELSE %s",
            tmpevent.primary_key(),
            STR_EVE_TRIGGER [ tmpevent.trigger()],
            STR_EVE_LOCK [ tmpevent.lock_event() ],
            STR_EVE_PASSFAIL [ tmpevent.pass_event() ],
            STR_EVE_PASSFAIL [ tmpevent.fail_event() ] );

         //printf ("debug: select event: passed in loop\n");

         error = tmpevent.SQLstep();
      }

      tmpevent.SQLfinalize();
   }
   else
   {
      WinMessage wmsg_error ("There was an error querying the event list.\nMaybe there are no events in the database.\nMake sure their PK is between 1 and 255");
      Window::show_all();
      showlist = false;
   }

   if ( showlist == true)
   {

      WinList wlst_event ( lst_event, 0, 16 );
      blit_editorbuffer();
      answer = Window::show_all();

      if ( answer != -1)
      {
         set_mazetile_event ( &mazetile [p_zcur][p_ycur][p_xcur], answer );
      }
   }
*/
   /* retval = tmpitem.SQLpreparef( "WHERE loctype=%d A2ND lockey=%d AND type=%d", Item_LOCATION_CHARACTER, primary_key(), Item_TYPE_WEAPON );
         if ( retval == SQLITE_OK)
         {
            retval = tmpitem.SQLstep();

            while ( retval == SQLITE_ROW)
            {
               if ( tmpitem.range() > max_range)
                  max_range = tmpitem.range();

               retval = tmpitem.SQLstep();
            }*/
/*
}

int  Editor::select_palette ( void )
{
   int i;
   int selpalette = 0;
   //bool selected = false;
   //int answer;
   List lst_texture ("Select the palette?", 20, true );
   //TODO : Fix with the new user interface
   //WinData<BITMAP> wdat_texture ( WDatProc_texture_bitmap, *(palsample.bmp[0]),
   //                              WDatProc_POSITION_TEXTURE  );

   for ( i = 0; i < TEXTUREPAL_NB_PALETTE; i++)
   {
      lst_texture.add_itemf ( i,"Palette %d", i);
   }

   WinList wlst_texture ( lst_texture, 20, 40 );

   while ( lst_texture.selected() == false && selpalette != -1 )
   {
      blit_editorbuffer();
      selpalette = Window::show_all();

      if ( selpalette != -1 )
      {
         //wdat_texture.parameter ( *(palsample.bmp [ selpalette ]) );
         Window::refresh_all();
      }

   }

   return (selpalette);
}

 int Editor::select_active_palette_texture ( void )
 {
       int i;
   int seltexture = -1;
   //bool selected = false;
   //int answer;
   List lst_texture ("Select the texture?", 20, true );
   // TODO: Fix with the new user interface.
   // WinData<BITMAP> wdat_texture ( WDatProc_texture_bitmap, *(editorpal.bmp[0]),
   //                              WDatProc_POSITION_TEXTURE  );



   for ( i = 0; i < TEXTUREPAL_NB_TEXTURE; i++)
   {
      lst_texture.add_item ( i,STR_TEX_PALETTE [i]);
   }

   WinList wlst_texture ( lst_texture, 20, 40 );

   do // to avoid a warning, I changed a while as a do while to allow seltexture of value -1
      // to enter the block.
   {
      blit_editorbuffer();
      seltexture = Window::show_all();

      if ( seltexture != -1 )
      {
         //wdat_texture.parameter ( *(editorpal.bmp [ seltexture ]) );
         Window::refresh_all();
      }

   }
   while ( lst_texture.selected() == false && seltexture != -1 );

   return (seltexture);
 }

*/
/*void Editor::read_curtile ( void )
{
   s_mazetile mtile = mazetile [p_zcur][p_ycur][p_xcur];

   p_curtile.solid [0] = (mtile.solid & MAZETILE_SOLID_NORTH_MASK) >> 6;
   p_curtile.solid [1] = (mtile.solid & MAZETILE_SOLID_EAST_MASK) >> 4;
   p_curtile.solid [2] = (mtile.solid & MAZETILE_SOLID_SOUTH_MASK) >> 2;
   p_curtile.solid [3] = (mtile.solid & MAZETILE_SOLID_WEST_MASK);

   p_curtile.special_tech = (mtile.special & MAZETILE_SPECIAL_TECH_MASK);
   p_curtile.special_fill = (mtile.special & MAZETILE_FILLING_MASK);
   p_curtile.masktex = (mtile.special & ( Maze_WALL_MTEX_MASK + Maze_FLOOR_MTEX_MASK));
//   p_curtile.object_position = (mtile.texture & Maze_OBJECT_MASK) >> 6;
   //p_curtile.tilesetid = (mtile.texture & Maze_TEXSET_MASK);
   //p_curtile.walltex = mtile.walltex;
   //p_curtile.floortex = mtile.floortex;
   //p_curtile.objectimg = mtile.objectimg;
   p_curtile.event = mtile.event;


}*/

/*void Editor::write_curtile ( void )
{
   s_mazetile mtile;

   mtile.solid = (p_curtile.solid [0] << 6) + (p_curtile.solid [1] << 4)
      + (p_curtile.solid [2] << 2) + p_curtile.solid [3];
   mtile.special = p_curtile.special_tech + p_curtile.special_fill;
   //mtile.texture = p_curtile.masktex + ( p_curtile.object_position << 6 )
   //   + p_curtile.tilesetid;
   //mtile.walltex = p_curtile.walltex;
   //mtile.floortex = p_curtile.floortex;
   //mtile.objectimg = p_curtile.objectimg;
   mtile.event = p_curtile.event;

   mazetile [p_zcur][p_ycur][p_xcur] = mtile;
}*/


/*
   notes on work to do that should normaly be written on paper.

the grid icon must be thiner
Command : Add various view type : Special, fill, texture, masktex, item
   Or detailed view ( larger square with texture, item in it.
Command : Add one way wall door finder, or simple viewer
Launch demo view ( check with maze, drop some operationality and no combat)

*/

/*





const char STR_EDT_SPECIAL_TECH [][16] =
{
   {"Bounce"},
   {"Solid"},
   {"Light"},
   {""}
};

const char STR_EDT_SPECIAL_FILL [][16] =
{
   {"None"},
   {"Water"},
   {"Fizzle"},
   {"Fog"},
   {"Poison Gas"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"Darkness"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"},
   {"!Undefined!"}
};

const s_Editor_polygon_info Editor_POLYGON_INFO [6] =
{
   { EDITOR_POLYGON_WALL, 6, MAZETILE_SOLID_NORTH_MASK, MAZETILE_SOLID_NORTH_WALL },
   { EDITOR_POLYGON_WALL, 4, MAZETILE_SOLID_EAST_MASK, MAZETILE_SOLID_EAST_WALL },
   { EDITOR_POLYGON_WALL, 0, MAZETILE_SOLID_WEST_MASK, MAZETILE_SOLID_WEST_WALL },
   { EDITOR_POLYGON_CEILING, 0, 0, 0 },
   { EDITOR_POLYGON_FLOOR, 0, 0, 0 },
   { EDITOR_POLYGON_TRANSWALL, 2, MAZETILE_SOLID_SOUTH_MASK, MAZETILE_SOLID_SOUTH_WALL }

};

const s_Editor_texpalette_color Editor_texpalette_color [32] =
{
  { 80, -1 }, // texture palette 0
  { 80, 25 },
  { 88, -1 },
  { 88, 25 },
  { 96, -1 },
  { 96, 25 },
  { 104, -1 },
  { 104, 25 },
  { 112, -1 },
  { 112, 25 },
  { 120, -1 },
  { 120, 25 },
  { 128, -1 },
  { 128, 25 },
  { 136, -1 },
  { 136, 25 },

  { 144, -1 }, // texture palette 16
  { 144, 25 },
  { 152, -1 },
  { 152, 25 },
  { 160, -1 },
  { 160, 25 },
  { 168, -1 },
  { 168, 25 },
  { 176, -1 },
  { 176, 25 },
  { 184, -1 },
  { 184, 25 },
  { 192, -1 },
  { 192, 25 },
  { 200, -1 },
  { 200, 25 }

};



V3D Editor_room [ 6 ] [ 4 ] =
{

   { // center wall
      {490<<16, 145<<16, 1, 0,           TEXTURE_SIZE<<16, 255 },
      {490<<16, 59<<16,  1, 0,           0,           255 },
      {590<<16, 59<<16,  1, TEXTURE_SIZE<<16, 0,           255 },
      {590<<16, 145<<16, 1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 }
   },
   { // right wall
      {590<<16, 145<<16, 1, 0,           TEXTURE_SIZE<<16, 255 },
      {590<<16, 59<<16,  1, 0,           0,           255 },
      {640<<16, 16<<16,  0, TEXTURE_SIZE<<16, 0,           255 },
      {640<<16, 188<<16, 0, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 }
   },
   { // left wall
      {440<<16, 188<<16, 0, 0,           TEXTURE_SIZE<<16, 255},
      {440<<16, 16<<16,  0, 0,           0,           255},
      {490<<16, 59<<16,  1, TEXTURE_SIZE<<16, 0,           255},
      {490<<16, 145<<16, 1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255}
   },
   { // ceiling
      {490<<16, 59<<16, 1, 0,           TEXTURE_SIZE<<16, 255 },
      {440<<16, 16<<16,  0, 0,           0,          255 },
      {640<<16, 16<<16,  0, TEXTURE_SIZE<<16, 0,          255 },
      {590<<16, 59<<16, 1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 }
   },
   { //floor
      {440<<16, 188<<16, 0, 0,           TEXTURE_SIZE<<16, 255 },
      {490<<16, 145<<16,  1, 0,           0,          255 },
      {590<<16, 145<<16,  1, TEXTURE_SIZE<<16, 0,          255 },
      {640<<16, 188<<16, 0, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 }
   },
   { // translucent wall (last in the list ot make sure it's drawn last)
      {440<<16, 188<<16, 0, 0,           TEXTURE_SIZE<<16, 255 },
      {440<<16, 16<<16,  0, 0,           0,           255 },
      {640<<16, 16<<16,  0, TEXTURE_SIZE<<16, 0,           255 },
      {640<<16, 188<<16, 0, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 }
   },

};




*/
