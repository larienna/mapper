/**
   editor.h

   @author Eric Pietrocupo
   @since  February 21st, 2004
   @date   Refactored on February 2021
   @license GNU General Public License

   This modules contains the routines related to the adventure editor. It contains all
   the public definitions of the modules which should be required to start the editor.
   everything else has been hidden to the caller.

   The module has been refactored from the original editor programmed in C++ and missing features
   has been implemented. It will use the revised new adventure and maze file format.
*/

#ifndef EDITOR_H_INCLUDED
#define EDITOR_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/**
   There is a single method visible to the rest of the program and it's the main routine to
   launch the editor. Everything else is hidden or stored in another module
*/

void editor_main ( void );

/** This is a simple cursor used internally to keep the cursor position of the editor. But
    the information can be required by other modules so I exposed the structure here.
*/

typedef struct s_cursor
{  int x;
   int y;
   int z;
}s_cursor;



#ifdef __cplusplus
}
#endif


#endif // EDITOR_H_INCLUDED
