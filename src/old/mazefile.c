/**
   mazefile.c

   @author Eric Pietrocupo
   @since  March 2021
   @license GNU General Public License

   This module is used to save load and store the content of the maze. It will be used by
   the editor and the the maze drawing routines. The maze content is stored in a global
   variable with a fixed size.

*/

#include <allegro.h>
#include <stdio.h>
#include <stdbool.h>
#include <mazetile.h>
#include <mazefile.h>


bool load_mazefile ( const char* filename )
{  FILE *loadfile;
   s_mazefile_header header;

   //TODO: SMALLER MAZE: Use header parameter to load smaller mazes instead of maximum.

   loadfile = fopen (filename, "rb");

   if (loadfile != NULL)
   {
      fread ( &header, sizeof (s_mazefile_header), 1, loadfile);

      for ( int z = 0; z < MAZE_DEPTH; z++ )
         for ( int y = 0; y < MAZE_SIZE; y++)
            for ( int x = 0; x < MAZE_SIZE; x++)
            {   fread ( &mazetile [z][y][x], sizeof (s_mazetile), 1, loadfile );
            }

      fclose ( loadfile );
   }
   else return false;

   return (true);
}

bool save_mazefile ( const char* filename )
{
   FILE *savefile;
   s_mazefile_header header;

   // setup header, this is only there for compatibility. Not used in the game.
   // TODO: Make it meaningful
   header.version = 1;
   header.width = MAZE_SIZE;
   header.depth = MAZE_DEPTH;
   header.reserved = 0;

   savefile = fopen (filename, "wb");

   if (savefile != NULL)
   {
      fwrite ( &header, sizeof (s_mazefile_header), 1, savefile);

      for ( int z = 0; z < MAZE_DEPTH; z++ )
         for ( int y = 0; y < MAZE_SIZE; y++)
            for ( int x = 0; x < MAZE_SIZE; x++)
            {   fwrite ( &mazetile [z][y][x], sizeof (s_mazetile), 1, savefile );
            }

      fclose ( savefile );
   }
   else return false;


   return (true);
}
