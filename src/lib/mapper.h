/**
   MAPPER

   This is the main header API that the programmer will use to play their own maze.

   @author Eric Pietrocupo
   @date June 5th, 2021

   @license Apache license 2.0
*/

#ifndef MAPPER_H_INCLUDED
#define MAPPER_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

//TODO: use this header file to include other necessary header files. And rename this one
//something related to the projection room.
//It is also possible to fusion all .h into a lar.h file to make sure all the doc is at the same
//place and keep completely private routines into an alternate.h files.

/**
   The concept behind mapper is that it creates a projection room to build where all the walls
   has been position and projected. Then a scene can be built by drawing specific walls to match
   the map layout.


    This structure contains all the information concening the projection room necessary
    to project polygons. You must create and pass such structure to mpr_init() to initialize
    it, then pass it to almost all mpr methods. The structure is opaque so pointers must
    be used.
*/

typedef struct mpr_s_projection_room mpr_s_projection_room;

/** In order to initialise the engine, you need to give a series of parameters that will be used
    to render the maze corretly. Those parameters are store in the structure below that you will
    pass to the initialization function.
*/

typedef struct mpr_s_param
{  float wall_height;
   float wall_width;
   float camera_height; // make sure it is > 0 && < wall_height;
   float field_of_view; //in degrees, Converted by the engine
   float aspect_ratio; //normally screen width / screen height
   float pull_back; // must be < ( wall_width / 2)
   unsigned char room_size; // width and height of the room in tiles, must be an odd number
}mpr_s_param;

/** Error codes that can be returned by the initialisation function are listed in the following
    enumeration.
*/

typedef enum mpr_e_init_error
{  INIT_SUCCESS=0,
   INIT_ERROR_WRONG_PARAM,  //the parameters are invalid
   INIT_ERROR_INTERNAL,    //There is an internal error
}mpr_e_init_error;

/** The first thing to call is the initialisation function below with the parameters, the BITMAP
    you intend to draw your maze on and the address of a pointer for projection room. room_ptr
    is actually a return parameter. The BITMAP will have to be the same as the one used in the
    rendering routines, else the ZBuffering of the polygons will not work.
*/
mpr_e_init_error mpr_init ( mpr_s_projection_room **room_ptr, mpr_s_param *params, BITMAP *bmp );


/** An enumeration is used to indicate the facing position of the camera. It use a clockwise
    index of 4 values. It's used in all sort of facing including wall position in the maze.
*/

typedef enum e_facing { NORTH=0, EAST=1, SOUTH=2, WEST=3 } e_facing;

/** The position of the camera into maze coordinates is stored in the structure below. It's
    basically the x, y , z position in the maze and the facing of the camera. Facing up and down
    is of course impossible.
*/

typedef struct mpr_s_position
{  int x;
   int y;
   int z;
   e_facing f;
}mpr_s_position;

/** Before rendering of the screen, you will need to verify that the action of the player
    is valid for the maze, and move the position accordinly. The input processing can be
    done with mpr_process_input() that converts keyboard or joystick input into actions.

    The structure below contains the results code of what happened after the update action.
*/

//TODO: Maybe remove
/*typedef enum mpr_e_update_error
{  UPDATE_SUCCESS = 0,
   UPDATE_HIT_WALL,
   UPDATE_DOOR_LOCKED,
   UPDATE_NOTHING_TO_USE
}mpr_e_update_error;
*/

typedef struct mpr_s_update_result
{  //mpr_e_update_error error;
   bool door_opened; // a door was opened
   bool event_triggered;
   //TODO: There could be face wall vs use wall triggers
   bool hit_wall; // party movement has collided with an impassible surface
   bool wall_locked; // front wall is disabled.
}mpr_s_update_result;

/** The update routine will require a maze than you can get with mpr_load_mazefile() or
    mpr_create_maze(). It will be used to validate movement in the maze. A position is required
    to know the camera position and to modify the position after the action if necessary.
    Finally, the action parameter is the possible movement that the player can make.
*/

mpr_s_update_result mpr_update ( mpr_s_maze *maze, mpr_s_position *pos, mpr_e_action action );

//TODO: add texture palette

/** The rendering routing is the big function that build up the projection room and draw
    all the polygons on the bitmap. It require the room to project, the maze to use, the bitmap
    to draw into, and the position of the camera. This will be the function with the largest
    impact on the frame rate.
*/
void mpr_render ( mpr_s_projection_room *room, mpr_s_maze *maze, BITMAP *bmp, mpr_s_position *pos );

//TODO: might require allegro timers installed
void mpr_render_sequence ( const mpr_s_param *params, BITMAP *scr, mpr_s_position *pos, mpr_e_action action,
                          int nb_frame, float frame_length );


/** It's important to destroy the projection room once the program closes with the routine below.
*/
void mpr_destroy ( mpr_s_projection_room *room );



//Utility method. made public for the main, not sure if will be necessary
int modulo(int a, int mod);

#ifdef __cplusplus
}
#endif

#endif // MAPPER_H_INCLUDED
