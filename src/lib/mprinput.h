/**
   MAPPER

   This file contains input management routine

   @author Eric Pietrocupo
   @date June 22nd, 2021

   @license Apache license 2.0
*/


#ifndef MPRINPUT_H_INCLUDED
#define MPRINPUT_H_INCLUDED

#define NB_INPUT_KEY  8
#define NB_INPUT_BUTTON  4 //directions are on axis and cannot be reconfigured.

/** This is the series of actions that can be performed in the maze. Those actions can be
    triggered by different input devices. This is why all inputs are converted as action
    for uniformity.
*/

typedef enum mpr_e_action
{ USE_WALL=0,
  EXIT_NAVIGATION=1,     //return to another interface like a menu, or quit the demo
  SHIFT_LEFT=2,
  SHIFT_RIGHT=3,
  TURN_LEFT=4,
  TURN_RIGHT=5,
  MOVE_FORWARD=6,
  MOVE_BACKWARD=7,
  NO_ACTION=99
} mpr_e_action;


/** The following method will process the keyboard and joystick input and return the associated
    action. List of keys identified by mpr_e_action will be used. For the joystick, only the non
    direction buttons can be redefined. For directions, all axis will be read. This method
    will loop and return until one input has been pressed. Use a joystick_id of -1 if you do not
    want the joystick to be read.

    The input index meaning matches the first 4 or 8 values of enum mpr_e_action. The buttons
    are handled in that specific priority order. Keyboard buffer is flushed after each call.
    Joystick will keep the press tu avoid multipress of the same button recorded. The priority
    order of the button will prevent diagonals to be considered.
*/

mpr_e_action mpr_process_input ( char input_keys[NB_INPUT_KEY],
                                unsigned char input_button[NB_INPUT_BUTTON],
                                int joystick_id, int stick_id );



#endif // MPRINPUT_H_INCLUDED
