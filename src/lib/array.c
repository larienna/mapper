/**
   MAPPER

   Some utility methods for handling arrays.

   @author Eric Pietrocupo
   @date June 14th, 2021

   @license Apache license 2.0
*/

#include <stdlib.h>

void*** allocate_3D_dynamic_array ( size_t size, int z, int y, int x )
{  void ***array = (void ***)malloc(size*z);
   for ( int k = 0 ; k < z ; k++ )
   {  array[k] = (void **)malloc(size*y);
      for ( int j = 0 ; j < y ; j++ )
      {  array[k][j] = (void *)malloc(size*x);
      }
   }
   return array;
}

void destroy_3D_dynamic_array ( void ***array, int z, int y, int x )
{  for ( int k = 0 ; k < z ; k++ )
   {  for ( int j = 0 ; j < y ; j++ )
      {  free ( array[k][j] );
      }
      free(array[k]);
   }
   free (array);
}
