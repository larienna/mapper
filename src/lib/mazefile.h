/**
   MAPPER

   @author Eric Pietrocupo
   @date June 22nd, 2021

   @license Apache license 2.0

   This module is used to save load and store the content of the maze. It will be used by
   the editor and the the maze drawing routines. The maze is stored into a dynamic 3D
   array to allow mazes of variable size.

   The file format is a custom binary format which is composed of a small header and a
   dumping of the 3D array of structure. The main challenge is basically to save and load the
   tiles in the right order.

   The save and load routines will add the option to compress the mazedata using the zlib library
   in order to save space for every large maze and allow larger mazetile structures.

   This file also contains the routines and structure to create and store mazes. So it will
   be mandatory that is file is included by the user.
*/

#ifndef MAZEFILE_H_INCLUDED
#define MAZEFILE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/** There is the header in the binary maze file, it is mandatory to ensure compatibility
    between various file format. The header is not compressed, it must be read naturally.
    many information is copied into the mpr_s_maze structure;
   */

typedef struct s_mazefile_header
{  int version; // maze version. changes how the tiles are read (used simple int for retro compatibility
   unsigned char compression; //hold information about compression if used
   unsigned char sizeof_mazetile; //size of the mazetile structure in case various size are available
   unsigned char width; // Width and height of the maze (square).
   unsigned char depth; // Number of levels in the maze. Placed there in case of variable size.
   unsigned char reserved1; // standby for future use.
   unsigned char reserved2; // standby for future use.
   unsigned char reserved3; // standby for future use.
   unsigned char reserved4; // standby for future use.
}s_mazefile_header;

typedef enum mpr_e_mazefile_version
{  MAZEFILE_V1_WL_INITIAL=1,
   MAZEFILE_V2_WL_REVISED=2,
   MAZEFILE_V3_WL_REFACTORED=3,
   MAZEFILE_V4_MAPPER_16b01=4
}mpr_e_mazefile_version;

#define COMPRESSION_NONE   0
#define COMPRESSION_ZLIB   1 // it could contain other values eventually

/** This is the maze structure that will hold the loaded maze. It is allocated dynamically
    so make sure you free the structure using mpr_free_maze(). With a current mazetile of 16 bytes,
    a 256x256x256 maze would require 256 meg of RAM. That should be the biggest expected maze.
*/

//TODO: if I put this structure private, I might need accessors, not sure if it's worth it since
//it must be combined with various mazetile routines.
typedef struct mpr_s_maze
{  unsigned char width;
   unsigned char depth;
   s_mazetile ***tile;
}mpr_s_maze;

/** The following routines can be used to create and free mazes. Especially useful when creating
    a maze not loaded from a file. Create will dynamically allocate the maze, so it must be freed.
*/

mpr_s_maze* mpr_create_maze ( unsigned char width, unsigned char depth );
void mpr_destroy_maze ( mpr_s_maze *maze );

/** There is 2 simple routines that load and save the maze information from and to a
binary mazefile. Note that load_mazefile will CREATE a new maze, so it must be freed.
If there is an error loading the file, NULL will be returned
*/

mpr_s_maze* mpr_load_mazefile ( const char* filename );
bool mpr_save_mazefile ( mpr_s_maze* maze, const char* filename, unsigned char compression,
                        mpr_e_mazefile_version version );

/** The following methods will create a simple dummy maze for testing purpose. The method will
    take care of allocating a maze of the right size. So dont forget to free the maze with
    mpr_destroy_maze()
*/

mpr_s_maze *mpr_create_dummy_maze ( void );

#ifdef __cplusplus
}
#endif


#endif // MAZEFILE_H_INCLUDED
