/**
   MAPPER

   Contains the list of private methods that are tested by unitest

   @author: Eric Pietrocupo
   @date: June 6th, 2021

   @license: Apache license 2.0
*/



#ifndef MAPPER_PRIVATE_H_INCLUDED
#define MAPPER_PRIVATE_H_INCLUDED

#define NODE_HEIGHT 2
#define TILE_HEIGHT 1

#define NB_VERTEX 6     //Only 4 nodes are used but more vertex are created for the clipping routine.
#define NB_POLYGON 12  //Number of polygons PER TILE.

//TODO: Will have to set as maze parameters
#define TEXTURE_SIZE 256

//__________________________________ structures ___________________________________

/** Polygon information that will change while moving in the maze. The polygon vertex
   is the only thing that well remain constant after projection is complete.
*/
typedef struct s_polygon
{  V3D_f vertex[NB_VERTEX]; //contains virtual coordinates on init and screen coordinates
                            //once projection is complete
   int nb_vertex;         //nb of veretex in the polygon. Clipping can increase the number of vertex
   int type;          //should hold a contant with the polygon type information since not always PTEX
   unsigned char texture; //index of the texture to use in the palette. (byte save space compared to a pointer)
}s_polygon;

/** Contains pointers on nodes and polygon to allow access from a grid point of view
*/

typedef struct s_tile
{  V3D_f node[2][2][2]; // All 8 nodes surrounding the tile in x, y, z position
      //this is a duplication of the nodes above since holding pointers was too complicated.
      //It does not changes anything since values are constant.
      //z=0=floor, z=1=ceiling, y=0=back, y=1=front, x=0=left, x=1=right
   s_polygon polygon[NB_POLYGON];
   int polygon_count; //count the number of polygons created in the array
}s_tile;


struct mpr_s_projection_room
{  s_tile ***tile; //contains all tiles of the room (not the maze)
   ZBUFFER *zbuffer; //zbuffer used for drawing polygons on the screen in the right order
   float wall_height;
   float wall_width;
   float camera_height;
   float field_of_view;
   float aspect_ratio;
   float pull_back;
   unsigned char size;
};

typedef enum e_vector_index
{  X_INDEX=0,
   Y_INDEX=1,
   Z_INDEX=2
}e_vector_index;

typedef struct s_camera
{  float pos [3]; //position in world coordinates
   float dirv [3]; //direction vector in world coordinates
   float upv [3]; //Direction of the up vector
   float fov; //field of view
   float aspect; //aspect ratio
}s_camera;

//_________________________ Comparison methods _______________________________

bool _compare_polygon ( s_polygon *a, s_polygon *b);
bool _compare_vertex ( V3D_f *a, V3D_f *b );

//______________________________ Private methods to be tested ___________________________

int modulo(int a, int mod);
bool _validate_parameters ( mpr_s_param *params );
float _calculate_distance ( mpr_s_projection_room *room, int y, int x );
int _convert_distance_to_light ( float distance, float max_distance );
void _build_nodes ( mpr_s_projection_room *room, V3D_f ***node );
void _build_polygon_wall (s_tile *tile, unsigned char side );
e_facing _left_facing_of ( e_facing face );
e_facing _right_facing_of ( e_facing face );
e_facing _back_facing_of ( e_facing face );
void _move_in_direction ( mpr_s_position *pos, e_facing direction );


#endif // MAPPER_PRIVATE_H_INCLUDED
