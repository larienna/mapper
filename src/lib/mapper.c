/**
   MAPPER

   @author: Eric Pietrocupo
   @date: June 5th, 2021

   @license: Apache license 2.0
*/

#include <allegro.h>
#include <stdbool.h>
#include <mazetile.h>
#include <mazefile.h>
#include <mprinput.h>
#include <mapper.h>
#include <mapper_private.h>
#include <stdio.h>
#include <math.h>
#include <array.h>

//_____________________________ Private Methods _____________________________________

//TODO: Maybe move else where and put public since multipurpose.
int modulo(int a, int mod)
{ //source: https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers
  int r = a % mod;
  if (r < 0)
  {   r = (mod < 0) ? r - mod : r + mod;
  }
  return r;
}

bool _compare_vertex ( V3D_f *a, V3D_f *b )
{  if ( a->c != b->c ) return false;
   if ( a->u != b->u ) return false;
   if ( a->v != b->v ) return false;
   if ( a->x != b->x ) return false;
   if ( a->y != b->y ) return false;
   if ( a->z != b->z ) return false;
   return true;
}

bool _compare_polygon ( s_polygon *a, s_polygon *b)
{  if ( a->nb_vertex != b->nb_vertex) return false;
   //printf ("DEBUG: nb of vertex equal\n");
   if ( a->texture != b->texture ) return false;
   //printf ("DEBUG: texture index equal\n");
   if ( a->type != b->type ) return false;
   //printf ("DEBUG: polygon type equal\n");
   for ( int i = 0 ; i < a->nb_vertex; i++ )
   {  if ( !_compare_vertex(&a->vertex [i], &b->vertex[i] )) return false;
      //printf ("DEBUG: vertex %d equal\n", i);
   }
   return true;
}

/** Use the Pythagorean theorem to calculate distance between player position and NODES
*/

float _calculate_distance ( mpr_s_projection_room *room, int y, int x )
{  float room_center = (room->wall_width * (((room->size + 1) / 2) -1) )
                     + (room->wall_width / 2);
   float camera_x = room_center;
   float camera_y = room_center - room->pull_back;
   float point_x = room->wall_width * x;
   float point_y = room->wall_width * y;
   float lenght_a = fabs ( point_y - camera_y );
   float length_b = fabs ( point_x - camera_x );
   return (float) sqrt ( lenght_a*lenght_a + length_b*length_b );
}

/** Convert distance in a value from 35 to 255
*/

int _convert_distance_to_light ( float distance, float max_distance )
{  return (int) (((distance / max_distance ) * 220)+35);
}


void _build_nodes ( mpr_s_projection_room *room, V3D_f ***node )
{  float max_distance = _calculate_distance( room, room->size, room->size );

   int node_size = room->size + 1;
   for ( int k = 0; k < NODE_HEIGHT; k++ )
   {  for ( int j = 0 ; j < node_size; j++ )
      {  for ( int i = 0 ; i < node_size; i++ )
         {  node [k][j][i].x = i * room->wall_width;
            node [k][j][i].y = k * room->wall_height;
            node [k][j][i].z = -(j * room->wall_width);
            node [k][j][i].c =
               _convert_distance_to_light( _calculate_distance( room, j, i ), max_distance );
         }
      }
   }
}


void _copy_nodes_in_tiles ( mpr_s_projection_room *room, V3D_f ***node )
{  for ( int k = 0; k < TILE_HEIGHT; k++ )
   {  for ( int j = 0 ; j < room->size; j++ )
      {  for ( int i = 0 ; i < room->size; i++ )
         {  room->tile[k][j][i].node [0][0][0] = node [k][j][i];
            room->tile[k][j][i].node [0][0][1] = node [k][j][i+1];
            room->tile[k][j][i].node [0][1][0] = node [k][j+1][i];
            room->tile[k][j][i].node [0][1][1] = node [k][j+1][i+1];
            room->tile[k][j][i].node [1][0][0] = node [k+1][j][i];
            room->tile[k][j][i].node [1][0][1] = node [k+1][j][i+1];
            room->tile[k][j][i].node [1][1][0] = node [k+1][j+1][i];
            room->tile[k][j][i].node [1][1][1] = node [k+1][j+1][i+1];
         }
      }
   }
}

void _empty_polygon ( s_polygon *poly )
{  V3D_f vertex = { .x=0, .y=0, .z=0, .u=0, .v=0, .c=0 };
   for ( int i = 0; i < NB_VERTEX; i++)
   {  poly->vertex[i] = vertex;
   }
   poly->texture = 0;
   poly->nb_vertex = 0;
   poly->type = POLYTYPE_FLAT;
}


void _clear_polygons ( mpr_s_projection_room *room )
{  for ( int k = 0; k < TILE_HEIGHT; k++ )
   {  for ( int j = 0 ; j < room->size; j++ )
      {  for ( int i = 0 ; i < room->size; i++ )
         {  for ( int h = 0; h < NB_POLYGON; h++ )
            {  _empty_polygon ( &room->tile [k][j][i].polygon[h] );
               room->tile [k][j][i].polygon_count = 0;
            }
         }
      }
   }
}



/** Creates a polygon on the specified side inside the tile passed in parameter
    the side is not always the character facing. It's the facing for the front walls
    but not for the side walls.
*/
void _build_polygon_wall (s_tile *tile, unsigned char side )
{  //nx and ny Determines which node to use according to the facing.
   //nx and ny keeps track of which x y nodes to use since z is constant
   static const int NY[4][4] = {{ 1,1,1,1},{1,1,0,0},{0,0,0,0},{0,0,1,1}};
   static const int NX[4][4] = {{ 0,0,1,1},{1,1,1,1},{1,1,0,0},{0,0,0,0}};
   const int idx = tile->polygon_count; //just to make it easier to write
   int light = makecol (255, 255, 255);

   if ( tile->polygon_count < NB_POLYGON )
   {  tile->polygon[idx].vertex[0] = tile->node [0][NY[side][0]][NX[side][0]];
      tile->polygon[idx].vertex[0].u = 0;
      tile->polygon[idx].vertex[0].v = TEXTURE_SIZE;
      tile->polygon[idx].vertex[1] = tile->node [1][NY[side][1]][NX[side][1]];
      tile->polygon[idx].vertex[1].u = 0;
      tile->polygon[idx].vertex[1].v = 0;
      tile->polygon[idx].vertex[2] = tile->node [1][NY[side][2]][NX[side][2]];
      tile->polygon[idx].vertex[2].u = TEXTURE_SIZE;
      tile->polygon[idx].vertex[2].v = 0;
      tile->polygon[idx].vertex[3] = tile->node [0][NY[side][3]][NX[side][3]];
      tile->polygon[idx].vertex[3].u = TEXTURE_SIZE;
      tile->polygon[idx].vertex[3].v = TEXTURE_SIZE;
      tile->polygon[idx].nb_vertex = 4;
      tile->polygon[idx].type = POLYTYPE_PTEX;
      tile->polygon[idx].texture = 0; //TODO: Should change once palette is available

      for ( int v = 0; v < 4 ; v++ ) tile->polygon[idx].vertex[v].c = light;
      tile->polygon_count++;
   }
}

/** This method make a wall check, if it's true
*/
void _check_wall ( mpr_s_projection_room *room, mpr_s_maze *maze, int max_offset,
                  mpr_s_position *pos, unsigned char side, int k, int j, int i )
{

   int x = pos->x + i - max_offset;
   int y = pos->y + j - max_offset;
   x = modulo ( x, maze->width );
   y = modulo ( y, maze->width );

   if ( is_mazetile_surface( &maze->tile[k][y][x], side, SURFACE_SOLID ))
   {  _build_polygon_wall( &room->tile[0][j][i], side);
   }
}

void _build_polygons ( mpr_s_projection_room *room, mpr_s_maze *maze, mpr_s_position *pos )
{  const int max_offset = room->size / 2;

   //top section north wall check
   for ( int j = max_offset; j < room->size ; j++ )
   {  for ( int i = 0; i < room->size; i++ )
      {  _check_wall( room, maze, max_offset, pos, NORTHWALL, pos->z, j, i);
      }
   }

   //bottom section south wall check
   for ( int j = 0; j <= max_offset ; j++ )
   {  for ( int i = 0; i < room->size; i++ )
      {  _check_wall( room, maze, max_offset, pos, SOUTHWALL, pos->z, j, i);
      }
   }

   //right section east wall check
   for ( int j = 0; j < room->size ; j++ )
   {  for ( int i = max_offset; i < room->size; i++ )
      {  _check_wall( room,  maze, max_offset, pos, EASTWALL, pos->z, j, i);
      }
   }

   //left section west wall check
   for ( int j = 0; j < room->size ; j++ )
   {  for ( int i = 0; i <= max_offset; i++ )
      {  _check_wall( room, maze, max_offset, pos, WESTWALL, pos->z, j, i);
      }
   }

}

void _project_polygon ( s_polygon *poly, const MATRIX_f *camera_mx )
{  // temporary container for projection and clipping operation. Pointers are required for clipping
   int tmpout[8];
   V3D_f out_v[NB_VERTEX];
   V3D_f tmp_v[NB_VERTEX];
   V3D_f *wvptr[NB_VERTEX];
   V3D_f *ovptr[NB_VERTEX];
   V3D_f *tvptr[NB_VERTEX];


   //reference pointer
   for ( int i = 0 ; i < NB_VERTEX; i++ )
   {  ovptr[i] = &out_v[i];
      tvptr[i] = &tmp_v[i];
      wvptr[i] = &poly->vertex[i];
   }
   //printf ("DEBUG: pass 6.1\n");
   for ( int j = 0 ; j < poly->nb_vertex ; j++ )
   {  //changes the position of the wall according to the camera
      /*printf( "DEBUG: before apply matrix : x:%f, y:%f, z:%f\n",
                 poly->vertex[j].x,
                 poly->vertex[j].y,
                 poly->vertex[j].z);*/
      apply_matrix_f (camera_mx, poly->vertex[j].x,
                      poly->vertex[j].y,
                      poly->vertex[j].z,
                      &poly->vertex[j].x,
                      &poly->vertex[j].y,
                      &poly->vertex[j].z );
      /*printf( "DEBUG: after apply matrix : x:%f, y:%f, z:%f\n",
                 poly->vertex[j].x,
                 poly->vertex[j].y,
                 poly->vertex[j].z);*/
   }
   //printf ("DEBUG: pass 6.2\n");
   //clip polygons that ends up behind the camera
   poly->nb_vertex = clip3d_f( poly->type, 0.01, 0.01, poly->nb_vertex,
                  (AL_CONST V3D_f **)wvptr, ovptr, tvptr, tmpout);
   //printf ("DEBUG: NB vertex after clipping %d\n", poly->nb_vertex );

   for ( int j = 0 ; j < poly->nb_vertex; j++ )
   {  //convert 3D coordinates into 2D coordinates
      persp_project_f (out_v[j].x,
                       out_v[j].y,
                       out_v[j].z,
                       &poly->vertex[j].x,
                       &poly->vertex[j].y );
      //copy remaing information into the polygon
      poly->vertex[j].z = out_v[j].z;
      poly->vertex[j].u = out_v[j].u;
      poly->vertex[j].v = out_v[j].v;
      poly->vertex[j].c = out_v[j].c;
      /*printf( "DEBUG: after projection : x:%f, y:%f, z:%f\n",
                 poly->vertex[j].x,
                 poly->vertex[j].y,
                 poly->vertex[j].z);*/
   }
   //printf ("DEBUG: pass 6.4\n");
}

void _project_all_polygons ( mpr_s_projection_room *room, const MATRIX_f *camera_mx )
{  for ( int j = 0; j < room->size; j++ )
   {  for ( int i = 0; i < room->size; i++ )
      {  for ( int h = 0 ; h < room->tile[0][j][i].polygon_count; h++ )
         {  //printf ( "j=%d, i=%d, h=%d\n", j, i, h );
            _project_polygon ( &room->tile[0][j][i].polygon[h], camera_mx );
         }
      }
   }
}

void _draw_polygons ( mpr_s_projection_room *room, BITMAP *bmp )
{  static BITMAP *texture = NULL; //temporary texture
   //int debug_polygon_count = 0;
   if ( texture == NULL) texture = load_bitmap("stonewall.bmp", NULL);
   // if ( texture == NULL) texture = load_bitmap("square.bmp", NULL);
   V3D_f *vptr[NB_VERTEX];

   for ( int j = 0; j < room->size; j++ )
   {  for ( int i = 0; i < room->size; i++ )
      {  for ( int h = 0 ; h < room->tile[0][j][i].polygon_count; h++ )
         {  if ( room->tile[0][j][i].polygon[h].nb_vertex > 0 ) // only process polygons with vertexes
            {  for (int v = 0; v < NB_VERTEX; v++) //need to convvert to pointer, could be slow
               {  vptr[v] = &room->tile[0][j][i].polygon[h].vertex[v];
               }

               polygon3d_f ( bmp,
                            room->tile[0][j][i].polygon[h].type | POLYTYPE_ZBUF,
                            texture,
                            room->tile[0][j][i].polygon[h].nb_vertex,
                            vptr );
               //debug_polygon_count++;
            }
         }
      }
   }

   /*printf ("Room size = %d\n", room->size );
   printf ("Nb Drawn Polygon = %d\n", debug_polygon_count );
   */
}

bool _validate_parameters ( mpr_s_param *params )
{  if ( params->wall_height <= 0 ) return false;
   if ( params->wall_width <= 0 ) return false;
   if ( params->camera_height < 0
       || params->camera_height > params->wall_height ) return false;
   if ( params->field_of_view <= 0 || params->field_of_view > 360) return false;
   if ( params->pull_back > params->wall_width / 2 ) return false;
   if ( params->room_size % 2 == 0 ) return false;
   if ( params->room_size < 3 ) return false;

   return true;
}



void _build_camera_matrix ( MATRIX_f *matrix, mpr_s_projection_room *room, e_facing face )
{  s_camera camera;
   float center = ((room->size/2) * room->wall_width) + (room->wall_width/2);
   camera.pos[X_INDEX] = center;
   camera.pos[Z_INDEX] = -center;
   camera.pos[Y_INDEX] = room->camera_height;
   camera.dirv[Y_INDEX] = 0;
   camera.upv[X_INDEX] = 0;
   camera.upv[Y_INDEX] = 1;
   camera.upv[Z_INDEX] = 0;
   camera.fov = room->field_of_view;
   camera.aspect = room->aspect_ratio;

   switch ( face )
   {  case NORTH:
         camera.dirv[Z_INDEX] = -1;
         camera.dirv[X_INDEX] = 0;
         camera.pos[Z_INDEX] += room->pull_back;
      break;
      case EAST:
         camera.dirv[Z_INDEX] = 0;
         camera.dirv[X_INDEX] = 1;
         camera.pos[X_INDEX] -= room->pull_back;
         break;
      case SOUTH:
         camera.dirv[Z_INDEX] = 1;
         camera.dirv[X_INDEX] = 0;
         camera.pos[Z_INDEX] -= room->pull_back;
         break;
      case WEST:
         camera.dirv[Z_INDEX] = 0;
         camera.dirv[X_INDEX] = -1;
         camera.pos[X_INDEX] += room->pull_back;
         break;
   }

   get_camera_matrix_f( matrix,
                        camera.pos[X_INDEX],
                        camera.pos[Y_INDEX],
                        camera.pos[Z_INDEX],
                        camera.dirv[X_INDEX],
                        camera.dirv[Y_INDEX],
                        camera.dirv[Z_INDEX],
                        camera.upv[X_INDEX],
                        camera.upv[Y_INDEX],
                        camera.upv[Z_INDEX],
                        camera.fov,
                        camera.aspect );

}


void _copy_params_in_room ( mpr_s_projection_room *room, mpr_s_param *params )
{
   room->wall_height =  params->wall_height;
   room->wall_width =   params->wall_width;
   room->camera_height = params->camera_height;
   room->field_of_view = (params->field_of_view / 360.0 ) * 256;
   room->aspect_ratio = params->aspect_ratio;
   room->pull_back = params->pull_back;
   room->size = params->room_size;
}

e_facing _left_facing_of ( e_facing face )
{  return modulo(face -1, 4);
}

e_facing _right_facing_of ( e_facing face )
{  return modulo(face +1, 4);
}

e_facing _back_facing_of ( e_facing face )
{  return modulo(face +2, 4);
}

/** move the position in the direction indicated. It does not always match the facing
    of the camera.
*/
void _move_in_direction ( mpr_s_position *pos, e_facing direction )
{  switch ( direction )
   {  case NORTH: pos->y++;
         break;
      case EAST: pos->x++;
         break;
      case SOUTH: pos->y--;
         break;
      case WEST: pos->x--;
         break;
   }
}



// ____________________________________ Public Methods ________________________________

//TODO: could return NULL pointer in case of error, it would simplify the parameters,
//but return less information in case of errors
//TODO: put the bitmap inside the structure, will have to be a copy pointer, 2 pointer on the same bmp.
//to allow the user to reuse his bmp for overlay drawing.
mpr_e_init_error mpr_init ( mpr_s_projection_room **room_ptr, mpr_s_param *params, BITMAP *bmp )
{  V3D_f ***node; //temporary node array to build the tile nodes information
   if ( _validate_parameters(params) == false ) return INIT_ERROR_WRONG_PARAM;

   *room_ptr = (mpr_s_projection_room*) malloc (sizeof (mpr_s_projection_room) );
   _copy_params_in_room ( *room_ptr, params );
   int node_room_size;
   node_room_size = (*room_ptr)->size + 1;

   node = (V3D_f***) allocate_3D_dynamic_array( sizeof (V3D_f),
                                               NODE_HEIGHT, node_room_size, node_room_size);
   (*room_ptr)->tile = (s_tile***) allocate_3D_dynamic_array( sizeof (s_tile),
                                             TILE_HEIGHT, (*room_ptr)->size, (*room_ptr)->size);

   _build_nodes(*room_ptr, node);
   _copy_nodes_in_tiles (*room_ptr, node);

   (*room_ptr)->zbuffer = create_zbuffer(bmp);

   set_zbuffer( (*room_ptr)->zbuffer );

   destroy_3D_dynamic_array( (void***) node, NODE_HEIGHT, node_room_size, node_room_size);

   return INIT_SUCCESS;

}

mpr_s_update_result mpr_update ( mpr_s_maze *maze, mpr_s_position *pos, mpr_e_action action )
{  mpr_s_update_result result;

   //TODO: Maybe in the future, could prevent movement on impassible floors and ceilings.

   switch ( action )
   {  case NO_ACTION:
   case EXIT_NAVIGATION:
      default: //do nothing
         break;

      case MOVE_FORWARD:
         if ( !is_mazetile_surface( &maze->tile[pos->z][pos->y][pos->x],
                                   pos->f, SURFACE_IMPASSIBLE ) )
         {  _move_in_direction( pos, pos->f);
         }
         else result.hit_wall = true;
         break;

      case MOVE_BACKWARD:
         if ( !is_mazetile_surface( &maze->tile[pos->z][pos->y][pos->x],
                                   _back_facing_of(pos->f), SURFACE_IMPASSIBLE ))
         {  _move_in_direction( pos, _back_facing_of(pos->f));
         }
         else result.hit_wall = true;
         break;

      case TURN_LEFT: pos->f = _left_facing_of(pos->f);
         break;

      case TURN_RIGHT: pos->f = _right_facing_of(pos->f);
         break;

      case SHIFT_LEFT:
         if ( !is_mazetile_surface( &maze->tile[pos->z][pos->y][pos->x],
                                   _left_facing_of(pos->f), SURFACE_IMPASSIBLE ))
         {  _move_in_direction( pos, _left_facing_of(pos->f));
         }
         else result.hit_wall = true;
         break;

      case SHIFT_RIGHT:
         if ( !is_mazetile_surface( &maze->tile[pos->z][pos->y][pos->x],
                                   _right_facing_of(pos->f), SURFACE_IMPASSIBLE ))
         {  _move_in_direction( pos, _right_facing_of(pos->f));
         }
         else result.hit_wall = true;
         break;
   }

   pos->x = modulo ( pos->x, maze->width );
   pos->y = modulo ( pos->y, maze->width );

   return result;
}


//TODO: See if can avoid building walls again on a simple rotation, no clear and build polygons
//add the action as a parameter.
void mpr_render ( mpr_s_projection_room *room, mpr_s_maze *maze, BITMAP *bmp, mpr_s_position *pos )
{  MATRIX_f camera_mx;

   clear_bitmap( bmp );
   _clear_polygons( room );
   _build_camera_matrix ( &camera_mx, room, pos->f );

   clear_zbuffer(room->zbuffer, 0.);

   _build_polygons( room, maze, pos );
   _project_all_polygons ( room, &camera_mx );
   _draw_polygons ( room, bmp );

}

//TODO: Maybe will be discarded if no real time animation. Could still give it a try for fun.
//Animation problematic with doors. Need open frame with thickness in front. Not impossible to do
//but complex
void mpr_render_sequence ( const mpr_s_param *params, BITMAP *scr, mpr_s_position *pos, mpr_e_action action,
                          int nb_frame, float frame_length )
{
}


void mpr_destroy ( mpr_s_projection_room *room )
{
   destroy_3D_dynamic_array( (void***) room->tile, TILE_HEIGHT, room->size, room->size);
   destroy_zbuffer( room->zbuffer);
   free (room);
}
