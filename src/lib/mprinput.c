/**
   MAPPER

   @author: Eric Pietrocupo
   @date: June 22nd, 2021

   @license: Apache license 2.0
*/

#include <allegro.h>
#include <stdio.h>
#include <stdbool.h>
//#include <mapper.h>
#include <mprinput.h>

//_______________________________ Private mehods _______________________________

/** Return associated key action or -1 if no key is pressed.
*/
mpr_e_action _process_keys ( char input_keys[NB_INPUT_KEY] )
{
   if ( !keypressed()) return NO_ACTION;

   int code = readkey() >> 8;
   clear_keybuf();
   for ( int i = 0; i < NB_INPUT_KEY; i++)
   {  if ( input_keys[i] == code ) return i;
   }

   return NO_ACTION;


}

/** Return associated action, or -1 if no button or axis is pressed. All axis will be used
    and compared.
*/
#define NB_DIRECTION    4
#define DIR_OFFSET      4
mpr_e_action _process_joystick ( unsigned char input_button[NB_INPUT_BUTTON],
                                int joystick_id, int stick_id )
{  //Simulate key release. Hold the last valid state to avoid multiple button press without release
   static int pressed[NB_INPUT_KEY] = {0,0,0,0,0,0,0,0};
   //store the current status of the joystick input
   int status[NB_INPUT_KEY];

   poll_joystick();

   //Copy the status of the 4 input buttons
   for ( int i = 0 ; i < NB_INPUT_BUTTON; i++ )
   {
      if ( input_button[i] < joy[joystick_id].num_buttons )
      {   status[i] = joy[joystick_id].button[input_button[i]].b;
      }
   }

   //copy the status of the 4 joystick position
   if ( stick_id < joy[joystick_id].num_sticks )
   {  status[NB_INPUT_BUTTON] = joy[joystick_id].stick[stick_id].axis[0].d1;
      status[NB_INPUT_BUTTON+1] = joy[joystick_id].stick[stick_id].axis[0].d2;
      status[NB_INPUT_BUTTON+2] = joy[joystick_id].stick[stick_id].axis[1].d1;
      status[NB_INPUT_BUTTON+3] = joy[joystick_id].stick[stick_id].axis[1].d2;
   }

   //update pressed status
   for ( int i = 0; i < NB_INPUT_KEY; i++)
   {  if ( status[i] == 0 && pressed[i] == 1)
      {  pressed[i] = 0;
      }
      else if ( status[i] == 1 && pressed[i] == 0 )
      {  pressed[i] = 1;
         //printf ("DEBUG: input %d pressed\n", i );
         //for ( int i=0; i < NB_INPUT_KEY; i++) printf ("[%d]", pressed[i] );
         //printf ("\n");
         return i;
      }
   }

   return NO_ACTION;
}

//_____________________________________ Public Methods _______________________________

mpr_e_action mpr_process_input ( char input_keys[NB_INPUT_KEY],
                                unsigned char input_button[NB_INPUT_BUTTON],
                                int joystick_id, int stick_id )
{  mpr_e_action action = NO_ACTION;

   do // this will loop rapidly until an input is pressed
   {  action = _process_keys ( input_keys);
      if ( action == NO_ACTION && joystick_id != -1 && joystick_id < num_joysticks)
      {  action = _process_joystick (input_button, joystick_id, stick_id);
      }
   }
   while ( action == NO_ACTION );
   return action;
}

