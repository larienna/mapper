/**
   MAPPER

   Some utility methods for handling arrays.

   @author Eric Pietrocupo
   @date June 14th, 2021

   @license Apache license 2.0
*/

#ifndef ARRAY_H_INCLUDED
#define ARRAY_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/** These routines are used to create 3D dynamic arrays, by creating arrays, or arrays, of value.
    There for, recursive allocation and deallocation is necessary. The advantage is that the
    index [] operator can be used on the returning pointers to access them like a 3D array.

    You simply pass the sizeof() and the dimensions to the function. Dimensions are also required
    for deallocation to make sure each sub array is deallocation correctly.
*/

void*** allocate_3D_dynamic_array ( size_t size, int z, int y, int x );
void destroy_3D_dynamic_array ( void ***array, int z, int y, int x );

#ifdef __cplusplus
}
#endif

#endif // ARRAY_H_INCLUDED
