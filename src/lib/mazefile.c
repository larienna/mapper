/**
   MAPPER

   @author: Eric Pietrocupo
   @date: June 22nd, 2021

   @license: Apache license 2.0
*/

#include <allegro.h>
#include <stdio.h>
#include <stdbool.h>
#include <mazetile.h>
#include <mazefile.h>
#include <array.h>

//________________________ Structures __________________________________



//________________________________ Public methods __________________________

mpr_s_maze* mpr_create_maze ( unsigned char width, unsigned char depth )
{  mpr_s_maze *maze = (mpr_s_maze*) malloc (sizeof (mpr_s_maze));
   maze->width = width;
   maze->depth = depth;
   maze->tile = (s_mazetile***) allocate_3D_dynamic_array( sizeof(s_mazetile), depth, width, width );
   return (maze);
}

void mpr_destroy_maze ( mpr_s_maze *maze )
{  destroy_3D_dynamic_array( (void***)maze->tile, maze->depth, maze->width, maze->width);
   free (maze);
}

//TODO: add compression
mpr_s_maze* mpr_load_mazefile ( const char* filename )
{  FILE *loadfile;
   mpr_s_maze *maze;
   s_mazefile_header header;

   //Currently only loads the latest version of the file.
   //TODO: Maybe there is an interest to create a loading routine for each version.
   //or atleast the previous version to transfer some files. But not really mandatory for now.


   loadfile = fopen (filename, "rb");

   if (loadfile != NULL)
   {
      fread ( &header, sizeof (s_mazefile_header), 1, loadfile);
      if ( header.version < MAZEFILE_V4_MAPPER_16b01 ) return NULL;

      maze = mpr_create_maze( header.width, header.depth);

      for ( int z = 0; z < maze->depth; z++ )
         for ( int y = 0; y < maze->width; y++)
            for ( int x = 0; x < maze->width; x++)
            {   fread ( &maze->tile [z][y][x], sizeof (s_mazetile), 1, loadfile );
            }

      fclose ( loadfile );
   }
   else return NULL;

   return (maze);
}

bool mpr_save_mazefile ( mpr_s_maze* maze, const char* filename, unsigned char compression,
                        mpr_e_mazefile_version version  )
{
   FILE *savefile;
   s_mazefile_header header;


   header.version = version;
   header.compression = compression;
   header.sizeof_mazetile = sizeof(s_mazetile);
   header.width = maze->width;
   header.depth = maze->depth;
   header.reserved1 = 0;
   header.reserved2 = 0;
   header.reserved3 = 0;
   header.reserved4 = 0;

   savefile = fopen (filename, "wb");

   if (savefile != NULL)
   {
      fwrite ( &header, sizeof (s_mazefile_header), 1, savefile);

      for ( int z = 0; z < maze->depth; z++ )
         for ( int y = 0; y < maze->width; y++)
            for ( int x = 0; x < maze->width; x++)
            {   fwrite ( &maze->tile [z][y][x], sizeof (s_mazetile), 1, savefile );
            }

      fclose ( savefile );
   }
   else return false;


   return (true);
}

mpr_s_maze *mpr_create_dummy_maze ( void )
{  mpr_s_maze *maze = mpr_create_maze ( 5, 1 );

   //empty the room
   for ( int j = 0 ; j < maze->width ; j++ )
   {  for ( int i = 0 ; i < maze->width ; i++ )
      {  set_mazetile_empty( &maze->tile[0][j][i]);
      }
   }

   //horizontal walls
   for ( int i = 0 ; i < maze->width ; i++ )
   {  set_mazetile_surface( &maze->tile[0][maze->width-1][i], NORTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
      set_mazetile_surface( &maze->tile[0][0][i], SOUTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   }

   //vertical walls
   for ( int j = 0 ; j < maze->width ; j++ )
   {  set_mazetile_surface( &maze->tile[0][j][0], WESTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
      set_mazetile_surface( &maze->tile[0][j][maze->width-1], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   }

   //interior walls L walls
   set_mazetile_surface( &maze->tile[0][1][1], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze->tile[0][1][1], NORTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   set_mazetile_surface( &maze->tile[0][3][1], SOUTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze->tile[0][3][1], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   set_mazetile_surface( &maze->tile[0][2][1], SOUTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze->tile[0][2][1], NORTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   set_mazetile_surface( &maze->tile[0][3][2], WESTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze->tile[0][1][2], WESTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   //interior long wall
   set_mazetile_surface( &maze->tile[0][1][3], WESTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze->tile[0][2][3], WESTWALL, SURFACE_SOLID );
   set_mazetile_surface( &maze->tile[0][3][3], WESTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   set_mazetile_surface( &maze->tile[0][1][2], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze->tile[0][2][2], EASTWALL, SURFACE_SOLID );
   set_mazetile_surface( &maze->tile[0][3][2], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   //exit entry
/*   set_mazetile_surface( &maze[2][4], NORTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze[2][4], SOUTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   unset_mazetile_surface( &maze[2][4], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze[3][4], SOUTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   set_mazetile_surface( &maze[1][4], NORTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
*/

   unset_mazetile_surface( &maze->tile[0][2][maze->width-1], EASTWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);
   unset_mazetile_surface( &maze->tile[0][maze->width-1][2], NORTHWALL, SURFACE_SOLID | SURFACE_IMPASSIBLE);

   return maze;
}
