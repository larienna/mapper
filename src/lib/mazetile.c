/**

   @author Eric Pietrocupo
   @date   June 9th 2021
   @license Apache 2 license

*/

#include <stdbool.h>
#include <mazetile.h>

#define FILLING_COLOR_MASK   0b00001111 //mask the color from the type
#define FILLING_DENSITY_MASK 0b00110000 //how dense should it appear


void set_mazetile_empty ( s_mazetile *tile )
{  for ( int i = 0; i < NB_SURFACE; i++ )
   {  tile->surface[i] = 0;
      tile->interact[i] = 0;
   }

   tile->filling = 0;
   tile->palette = 0;
   tile->area = 0;
   tile->reserved = 0;
}

void set_mazetile_surface ( s_mazetile *tile, e_surface id, unsigned char flag )
{  tile->surface[id] = tile->surface[id] | flag ;
}

void unset_mazetile_surface ( s_mazetile *tile, e_surface id, unsigned char flag )
{  tile->surface[id] = tile->surface[id] & ~flag;
}

bool is_mazetile_surface ( s_mazetile *tile, e_surface id, unsigned char flag )
{  return ( (tile->surface[id] & flag) == flag );
}

void set_mazetile_interact ( s_mazetile *tile, e_surface id, unsigned char flag )
{  tile->interact[id] = tile->interact[id] | flag ;
}

void unset_mazetile_interact ( s_mazetile *tile, e_surface id, unsigned char flag )
{  tile->interact[id] = tile->interact[id] & ~flag;
}

bool is_mazetile_interact ( s_mazetile *tile, e_surface id, unsigned char flag )
{  return ( (tile->interact[id] & flag) == flag );
}

void set_mazetile_filling_color ( s_mazetile *tile, unsigned char color )
{  unsigned char other_bits = tile->filling & ~FILLING_COLOR_MASK;
   tile->filling = other_bits + color;
}

unsigned char get_mazetile_filling_color ( s_mazetile *tile )
{  return (tile->filling & FILLING_COLOR_MASK );
}

void set_mazetile_filling_density ( s_mazetile *tile, unsigned char density )
{  unsigned char other_bits = tile->filling & ~FILLING_DENSITY_MASK;
   tile->filling = other_bits + density;
}

unsigned char get_mazetile_filling_density ( s_mazetile *tile )
{  return (tile->filling & FILLING_DENSITY_MASK );
}

