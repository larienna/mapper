/**

   @author Eric Pietrocupo
   @date   June 9th 2021
   @license Apache 2 license

   In order to avoid bugs and unwanted issues with the maze tile system, I am using
   a C style module to centralise all read and write operations at the same place to
   make sure they are beign done properly. In order to optimise space and speed,
   classes cannot be used, so the tile structure is passed to each call.

   This file is used to managed the new maze tile encoding that is more functional
   than esthetic. The division of the byte is also changed to make sure most features
   are encoded on a single bit, and that each wall, floor, ceiling has it's own byte
   of data. Bigger structure has been used for further expansion. Compression of the
   file will make the extra space irrelevant.

   I also tried to reduce exception and the number of methods.
*/

#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/** Each tile in the maze will have the following structure. 1 variable has been made for each
    wall, floor and ceiling. The structure has been increased in size to support additional
    features in the future. palette has been split up to separate functionality from aesthetics.
    Event positions are marked in the maze, but it's content is handled by the software. The
    engine only warns that an even has been triggered. Certain flag configuration might have
    no effect.

    Wall and ceilings are identified as surfaces. They all hold the same flags but can be used
    differently according to if the flag is set on a wall or on a floor. Therefore there are
    6 surfaces for each tile. Here is the structure of a tile.
*/

#define NB_SURFACE   6

typedef struct s_mazetile
{  unsigned char surface[NB_SURFACE]; //Contains flags about how to display the surface.
   unsigned char interact[NB_SURFACE]; //Possible interaction with the 6 surface including doors
   unsigned char filling; //filling encoding mostly used for aesthetics
   unsigned char palette; //texture palette id, aesthetic portion of an area
   unsigned char area;    //determine random encounter, events, etc. Non aesthetic elements
   unsigned char reserved; //not used yet.
}s_mazetile;
//TODO: Maybe subdivide area with sub area to avoid duplicating data for each area.
//see what are the needs when I am there.

/** A tile can be initialised to it's default values with the following method
*/

void set_mazetile_empty ( s_mazetile *tile );

/** Each surface is identified with an ID stored into an enumeration
*/

typedef enum e_surface
{  NORTHWALL= 0,
   EASTWALL=  1,
   SOUTHWALL= 2,
   WESTWALL=  3,
   FLOOR=     4,
   CEILING=   5
}e_surface;

/** Then it is possible to set values for each of the flag available for each surface.
    You must supply a tile, a surface id, and the flag to modify. Surface has been decomposed
    into tiny flags for more flexibility, and to make it easier to read and modify.
    On the other hand, certain combinations could be invalid, like having a door without a
    wall. Right now, it's up to the editor to keep the consistencies valid.
*/

void set_mazetile_surface ( s_mazetile *tile, e_surface id, unsigned char flag );
void unset_mazetile_surface ( s_mazetile *tile, e_surface id, unsigned char flag );
bool is_mazetile_surface ( s_mazetile *tile, e_surface id, unsigned char flag );

/** The flags available are
*/

#define SURFACE_SOLID       0b00000001 //indicated the surface cannot be passed through
#define SURFACE_TRANSPARENT 0b00000010 //We should be able to see through the surface but it
                                               //does not imply that it is solid
#define SURFACE_ALTERNATE   0b00000100 //display the surface with alternate texture
#define SURFACE_MARKING     0b00001000 //There is a marking on the surface to identify an event
#define SURFACE_IMPASSIBLE  0b00010000 //Indicates that it cannot be crossed
//TODO: potential problem in editor, a wall required 2 flags: impassible and solid to be a wall

//TODO: How to handle liquid floor: maybe translucent bit?

/** There is also a series of routines to setup the interaction option for each surface.
    The parameter structure is the same as for surface.
*/

void set_mazetile_interact ( s_mazetile *tile, e_surface id, unsigned char flag );
void unset_mazetile_interact ( s_mazetile *tile, e_surface id, unsigned char flag );
bool is_mazetile_interact ( s_mazetile *tile, e_surface id, unsigned char flag );

/** But the constant that can be used as flags are different
*/

#define INTERACT_DOOR        0b00000001 //
#define INTERACT_SECRET_DOOR 0b00000010 //
#define INTERACT_DISABLED    0b00000100 //Indicates that any interaction is disabled (ex: locked door)
#define INTERACT_EVENT       0b00001000
#define INTERACT_EVENT_USE   0b00010000 //if the bit is set, must use interact button to activate

/** Finally, special methods are required to handle filling density. Filling is now only
    aesthetics, it's about setting a color in an area, at different density. Here are
    the getters and setters for the color and the density.
*/

void set_mazetile_filling_color ( s_mazetile *tile, unsigned char color );
unsigned char get_mazetile_filling_color ( s_mazetile *tile );
void set_mazetile_filling_density ( s_mazetile *tile, unsigned char density );
unsigned char get_mazetile_filling_density ( s_mazetile *tile );

/* The type of densities and colors are defined as follow
*/

#define FILLING_DENSITY_NONE    0b00000000
#define FILLING_DENSITY_LIGHT   0b00010000
#define FILLING_DENSITY_GAS     0b00100000
#define FILLING_DENSITY_OPAQUE  0b00110000
//color: like old text mode, 8 color dark 8 color light so: two 2x2x2 rgb matrix

#define FILLING_COLOR_BLACK     0b00000000
#define FILLING_COLOR_NAVY      0b00000001
#define FILLING_COLOR_GREEN     0b00000010
#define FILLING_COLOR_TURQUOISE 0b00000011
#define FILLING_COLOR_RED       0b00000100
#define FILLING_COLOR_MAGENTA   0b00000101
#define FILLING_COLOR_BROWN     0b00000110
#define FILLING_COLOR_LGRAY     0b00000111
#define FILLING_COLOR_DGRAY     0b00001000
#define FILLING_COLOR_BLUE      0b00001001
#define FILLING_COLOR_LIME      0b00001010
#define FILLING_COLOR_CYAN      0b00001011
#define FILLING_COLOR_PINK      0b00001100
#define FILLING_COLOR_FUSCHIA   0b00001101
#define FILLING_COLOR_YELLOW    0b00001110
#define FILLING_COLOR_WHITE     0b00001111


#ifdef __cplusplus
}
#endif


#endif // TILE_H_INCLUDED
