/**
   MAPPER

   demo program of the new "real" 3D engine
   without using code from the original wizardry legacy

   @author: Eric Pietrocupo
   @date: May 27th, 2021

   @license: Apache license 2.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <allegro.h>
#include <stdbool.h>
#include <math.h>
//#include <unitest.h>
#include <mazetile.h>
#include <mazefile.h>
#include <mprinput.h>
#include <mapper.h>
#include <sys/time.h>

#define INIT_FAILURE 1
#define INIT_SUCCESS 0

// structure containing the vertex for world and screen position of the polygon
//
// vertex 1 +----+ vertex 2
//          |    |
//          |    |
// vertex 0 +----+ vertex 3

#define MAX_VERTEX 8 //use 8 from the example since clipping can add vertex
//TODO: might not be unnessessary if do not want animation.

#define TEXTURE_SIZE 256
#define NB_POINT  2
#define MAX_POINT 4

#define X_INDEX 0
#define Y_INDEX 1
#define Z_INDEX 2

#define NB_POLYGON 6
#define NB_LINE   14

#define HEIGHT 10
#define LENGTH 10


typedef struct s_polygon_demo
{  V3D_f world_v[MAX_VERTEX];
   V3D_f screen_v[MAX_VERTEX];
   V3D_f tmp_v[MAX_VERTEX];
   //polygon signature functions requires an array of pointers
   //so pointers to the previous arrays are used
   V3D_f *wvptr[MAX_VERTEX];
   V3D_f *svptr[MAX_VERTEX];
   V3D_f *tvptr[MAX_VERTEX];

   int nb_vertex; // count the number of vertex, can change after clipping
}s_polygon_demo;



typedef struct s_gridline
{  V3D_f v[MAX_POINT];
   V3D_f tmpv[MAX_POINT];
   V3D_f scrv[MAX_POINT];
   //pointer on vertex above
   V3D_f *wvptr[MAX_POINT];
   V3D_f *svptr[MAX_POINT];
   V3D_f *tvptr[MAX_POINT];
   int nb_vertex; // count the number of vertex, can change after clipping
                  // but I will still only use the first 2 vertex, since it's a line.
}s_gridline;

typedef struct s_camera
{  float pos [3]; //position in world coordinates
   float dirv [3]; //direction vector in world coordinates
   float upv [3]; //Direction of the up vector
   float fov; //field of view
   float aspect; //aspect ratio
}s_camera;


s_polygon_demo wspoly [NB_POLYGON];
s_gridline gridline [NB_LINE];

/** Initialisation routine for allegro of stuff that should be required*/
int init ( void )
{  set_uformat ( U_ASCII );
   if ( allegro_init () != 0 )
   {  printf ( "ERROR: %s\n", allegro_error);
      return INIT_FAILURE;
   }

   if ( install_keyboard () != 0 )
   {  printf ( "ERROR: %s\n", allegro_error);
      return ( INIT_FAILURE );
   }

   if ( install_joystick ( JOY_TYPE_AUTODETECT ) != 0 )
   {  printf ( "ERROR: %s\n", allegro_error);
      return ( INIT_FAILURE );
   }

   for ( int j = 0 ; j < num_joysticks; j++ )
   {  //TODO: does not give instruction to user, could be bad if calibration required.
      //still not the responsability of mapper.
      while (joy[j].flags & JOYFLAG_CALIBRATE) {
         calibrate_joystick_name(j);
         printf ("DEBUG: calibrating joystick %d\n", j );
      }

   }



   if ( install_timer () != 0 )
   {  printf ( "ERROR: %s\n", allegro_error);
      return ( INIT_FAILURE );
   }

   set_color_depth(16);
   if ( set_gfx_mode ( GFX_AUTODETECT, 640, 480, 0, 0 ) != 0 )
   {  printf ( "ERROR: %s\n", allegro_error);
      return ( INIT_FAILURE );
   }

   //source: allegro doc
   /*for (int i=0; i<num_joysticks; i++)
   {
      while (joy[i].flags & JOYFLAG_CALIBRATE) {
         printf ("DEBUG: calibrating joystick %d\n", i );
         const char *msg = calibrate_joystick_name(i);
         textprintf_ex(screen,font,0,0,makecol(255, 255, 255),0, "%s, and press a key\n", msg);
         readkey();
         if (calibrate_joystick(i) != 0) {
            textprintf_ex(screen,font,0,0,makecol(255, 255, 255),0, "oops!\n");
            readkey();
            exit(1);
         }
      }
   }*/

   return INIT_SUCCESS;
}

void create_lines (void)
{
   //X axis lines
   int max = (NB_LINE / 2);
   for ( int i = 0 ; i < max; i++)
   {  gridline[i].v[0].x = -LENGTH;
      gridline[i].v[0].y = 0;
      gridline[i].v[0].z = -(3*LENGTH) + (i*(LENGTH/2));
      gridline[i].v[1].x = 500;
      gridline[i].v[1].y = 0;
      gridline[i].v[1].z = -(3*LENGTH) + (i*(LENGTH/2));
   }

   for ( int i = max ; i < NB_LINE; i++)
   {  gridline[i].v[0].x = -LENGTH + ((i-max)*(LENGTH/2));
      gridline[i].v[0].y = 0;
      gridline[i].v[0].z = -(3*LENGTH);
      gridline[i].v[1].x = -LENGTH + ((i-max)*(LENGTH/2));
      gridline[i].v[1].y = 0;
      gridline[i].v[1].z = 0;
   }

   //referencing pointer for cliping
   printf ("start of referencing\n");
   for ( int i = 0 ; i < NB_LINE ; i++ )
   {  for ( int j = 0 ; j < MAX_POINT; j++ )
      {  gridline[i].wvptr[j] = &gridline[i].v[j];
         gridline[i].tvptr[j] = &gridline[i].tmpv[j];
         gridline[i].svptr[j] = &gridline[i].scrv[j];
      }
   }
   printf ("end of referencing\n");

}

void create_polygons ( void )
{
   //front wall
   wspoly[0].world_v[0].x = LENGTH;
   wspoly[0].world_v[0].y = 0;
   wspoly[0].world_v[0].z = -(2*LENGTH);
   wspoly[0].world_v[1].x = LENGTH;
   wspoly[0].world_v[1].y = HEIGHT;
   wspoly[0].world_v[1].z = -(2*LENGTH);
   wspoly[0].world_v[2].x = (2*LENGTH);
   wspoly[0].world_v[2].y = HEIGHT;
   wspoly[0].world_v[2].z = -(2*LENGTH);
   wspoly[0].world_v[3].x = (2*LENGTH);
   wspoly[0].world_v[3].y = 0;
   wspoly[0].world_v[3].z = -(2*LENGTH);

   wspoly[1].world_v[0].x = -LENGTH;
   wspoly[1].world_v[0].y = 0;
   wspoly[1].world_v[0].z = -(2*LENGTH);
   wspoly[1].world_v[1].x = -LENGTH;
   wspoly[1].world_v[1].y = HEIGHT;
   wspoly[1].world_v[1].z = -(2*LENGTH);
   wspoly[1].world_v[2].x = 0;
   wspoly[1].world_v[2].y = HEIGHT;
   wspoly[1].world_v[2].z = -(2*LENGTH);
   wspoly[1].world_v[3].x = 0;
   wspoly[1].world_v[3].y = 0;
   wspoly[1].world_v[3].z = -(2*LENGTH);

   //right wall
   wspoly[2].world_v[0].x = LENGTH;
   wspoly[2].world_v[0].y = 0;
   wspoly[2].world_v[0].z = 0;
   wspoly[2].world_v[1].x = LENGTH;
   wspoly[2].world_v[1].y = HEIGHT;
   wspoly[2].world_v[1].z = 0;
   wspoly[2].world_v[2].x = LENGTH;
   wspoly[2].world_v[2].y = HEIGHT;
   wspoly[2].world_v[2].z = -LENGTH;
   wspoly[2].world_v[3].x = LENGTH;
   wspoly[2].world_v[3].y = 0;
   wspoly[2].world_v[3].z = -LENGTH;

   //left wall
   wspoly[3].world_v[0].x = 0;
   wspoly[3].world_v[0].y = 0;
   wspoly[3].world_v[0].z = 0;
   wspoly[3].world_v[1].x = 0;
   wspoly[3].world_v[1].y = HEIGHT;
   wspoly[3].world_v[1].z = 0;
   wspoly[3].world_v[2].x = 0;
   wspoly[3].world_v[2].y = HEIGHT;
   wspoly[3].world_v[2].z = -LENGTH;
   wspoly[3].world_v[3].x = 0;
   wspoly[3].world_v[3].y = 0;
   wspoly[3].world_v[3].z = -LENGTH;

   //L wall west

   wspoly[5].world_v[0].x = 0;
   wspoly[5].world_v[0].y = 0;
   wspoly[5].world_v[0].z = -(2*LENGTH);
   wspoly[5].world_v[1].x = 0;
   wspoly[5].world_v[1].y = HEIGHT;
   wspoly[5].world_v[1].z = -(2*LENGTH);
   wspoly[5].world_v[2].x = 0;
   wspoly[5].world_v[2].y = HEIGHT;
   wspoly[5].world_v[2].z = -(3*LENGTH);
   wspoly[5].world_v[3].x = 0;
   wspoly[5].world_v[3].y = 0;
   wspoly[5].world_v[3].z = -(3*LENGTH);


   //L wall east

   wspoly[4].world_v[0].x = LENGTH;
   wspoly[4].world_v[0].y = 0;
   wspoly[4].world_v[0].z = -(2*LENGTH);
   wspoly[4].world_v[1].x = LENGTH;
   wspoly[4].world_v[1].y = HEIGHT;
   wspoly[4].world_v[1].z = -(2*LENGTH);
   wspoly[4].world_v[2].x = (LENGTH);
   wspoly[4].world_v[2].y = HEIGHT;
   wspoly[4].world_v[2].z = -(3*LENGTH);
   wspoly[4].world_v[3].x = (LENGTH);
   wspoly[4].world_v[3].y = 0;
   wspoly[4].world_v[3].z = -(3*LENGTH);

   //reference all pointers

   //printf ("BEFORE pointer reference\n");
   for ( int i = 0 ; i < NB_POLYGON ; i++ )
   {  for ( int j = 0 ; j < MAX_VERTEX; j++ )
      {  wspoly[i].wvptr[j] = &wspoly[i].world_v[j];
         wspoly[i].svptr[j] = &wspoly[i].screen_v[j];
         wspoly[i].tvptr[j] = &wspoly[i].tmp_v[j];
      }
      //map texture
      wspoly[i].world_v[0].u = 0;
      wspoly[i].world_v[0].v = TEXTURE_SIZE;
      wspoly[i].world_v[1].u = 0;
      wspoly[i].world_v[1].v = 0;
      wspoly[i].world_v[2].u = TEXTURE_SIZE;
      wspoly[i].world_v[2].v = 0;
      wspoly[i].world_v[3].u = TEXTURE_SIZE;
      wspoly[i].world_v[3].v = TEXTURE_SIZE;
   }



   //printf ("AFTER pointer reference\n");
}

//Convert world coordinates in screen coordinates
void project_polygons ( MATRIX_f *camera )
{  int c[NB_POLYGON] = { makecol( 250, 150, 150),
                         makecol( 250, 250, 150),
                         makecol( 150, 150, 250),
                         makecol( 150, 250, 150),
                       };


   int tmpout[8]; // required by clipping
   for ( int i = 0 ; i < NB_POLYGON; i++)
   {  printf ("Quad vertex %d world positions\n", i);
      for ( int j = 0 ; j < 4 ; j++ )
      {  apply_matrix_f (camera, wspoly[i].world_v[j].x,
                         wspoly[i].world_v[j].y,
                         wspoly[i].world_v[j].z,
                         &wspoly[i].world_v[j].x,
                         &wspoly[i].world_v[j].y,
                         &wspoly[i].world_v[j].z );

         //tmpvertexcopy
         /*wspoly[i].screen_v[j].x = wspoly[i].world_v[j].x;
         wspoly[i].screen_v[j].y = wspoly[i].world_v[j].y;
         wspoly[i].screen_v[j].z = wspoly[i].world_v[j].z;*/

         /*printf( "after camera : x:%f, y:%f, z:%f\n",
                 wspoly[i].world_v[j].x,
                 wspoly[i].world_v[j].y,
                 wspoly[i].world_v[j].z);*/
      }

      wspoly[i].nb_vertex = clip3d_f( POLYTYPE_PTEX, 0.01f, 0.01f, 4,
                  (AL_CONST V3D_f **)wspoly[i].wvptr,
                  wspoly[i].svptr,
                  wspoly[i].tvptr,
                  tmpout);

      printf ("\nDEBUG: NB vertex after clipping %d", wspoly[i].nb_vertex );

      for ( int j = 0 ; j < wspoly[i].nb_vertex; j++ )
      {  /*printf( "after clip   : x:%f, y:%f, z:%f\n",
                 wspoly[i].screen_v[j].x,
                 wspoly[i].screen_v[j].y,
                 wspoly[i].screen_v[j].z);*/

         persp_project_f (wspoly[i].screen_v[j].x,
                          wspoly[i].screen_v[j].y,
                          wspoly[i].screen_v[j].z,
                          &wspoly[i].screen_v[j].x,
                          &wspoly[i].screen_v[j].y );
         //wspoly[i].screen_v[j].c = makeacol(255, 255, 255); //in case it impact texture
         wspoly[i].screen_v[j].c = c[i];
         /*printf( "after project: x:%f, y:%f, z:%f, u:%f, v:%f\n",
                 wspoly[i].screen_v[j].x,
                 wspoly[i].screen_v[j].y,
                 wspoly[i].screen_v[j].z,
                 wspoly[i].screen_v[j].u,
                 wspoly[i].screen_v[j].v
                 );*/
      }
   }
}

void project_lines ( MATRIX_f *camera)
{  //int tmpout[8]; // required by clipping

   for ( int i = 0 ; i < NB_LINE ; i++ )
   {  for ( int j = 0 ; j < NB_POINT ; j++)
      {  apply_matrix_f (camera,
                         gridline[i].v[j].x,
                         gridline[i].v[j].y,
                         gridline[i].v[j].z,
                         &gridline[i].tmpv[j].x,
                         &gridline[i].tmpv[j].y,
                         &gridline[i].tmpv[j].z );
      }

      /*
      //this will crash the program
      wspoly[i].nb_vertex = clip3d_f( POLYTYPE_PTEX, 0.01, 0.01, 2,
                  (AL_CONST V3D_f **)gridline[i].wvptr,
                  gridline[i].svptr,
                  gridline[i].tvptr,
                  tmpout);
*/
      for ( int j = 0 ; j < NB_POINT ; j++)
      {
         persp_project_f ( gridline[i].tmpv[j].x,
                           gridline[i].tmpv[j].y,
                           gridline[i].tmpv[j].z,
                           &gridline[i].scrv[j].x,
                           &gridline[i].scrv[j].y );
      }
   }

}



void draw_polygons ( BITMAP *texture )
{  //test polygon
   /*V3D tmpoly [4] =
   { { 0<<16, 256<<16, 1, 0,       TEXTURE_SIZE<<16, 255 },
      {   0<<16, 0<<16,   1, 0,       0,       255 },
      {  256<<16, 0<<16,   1, TEXTURE_SIZE<<16, 0,       255 },
      {   256<<16, 256<<16, 1, TEXTURE_SIZE<<16, TEXTURE_SIZE<<16, 255 } };

   quad3d ( screen, POLYTYPE_FLAT, NULL
         , &tmpoly[ 0 ], &tmpoly [ 1 ], &tmpoly [ 2 ], &tmpoly [ 3 ] );*/




   for ( int i = 0 ; i < NB_POLYGON; i++)
   {  // creates an array of pointers on the vertexes of the polygon to satify signature.

      printf ("Polygon vertex %d screen positions\n", i);
      for ( int j = 0 ; j < wspoly[i].nb_vertex; j++)
      {
         /*printf( "Drawing:  x:%f, y:%f z:%f\n",
                 wspoly[i].svptr[j]->x,
                 wspoly[i].svptr[j]->y,
                 wspoly[i].svptr[j]->z);*/
      }

      polygon3d_f ( screen, POLYTYPE_PTEX | POLYTYPE_ZBUF,
                    texture, wspoly[i].nb_vertex, wspoly[i].svptr );
      /*quad3d_f ( screen, POLYTYPE_ptex, NULL,
              &wspoly[i].screen_v[0],
              &wspoly[i].screen_v[1],
              &wspoly[i].screen_v[2],
              &wspoly[i].screen_v[3] );
      printf ("Quad vertex %d screen positions\n", i);
      for ( int j = 0 ; j < 4; j++)
      {   printf( "x:%f, y:%f z:%f\n",
                 wspoly[i].screen_v[j].x,
                 wspoly[i].screen_v[j].y,
                 wspoly[i].screen_v[j].z);
      }*/
   }
}

void draw_lines( void )
{  int c = makecol( 100, 100, 100 );
   for ( int i = 0; i < NB_LINE; i++ )
   {  line ( screen,
            (int)gridline[i].scrv[0].x,
            (int)gridline[i].scrv[0].y,
            (int)gridline[i].scrv[1].x,
            (int)gridline[i].scrv[1].y,
            c);
   }
}

void make_screen_shot ( char *filename )
{

   BITMAP *scrnshot;
   PALETTE pal;

   scrnshot = create_bitmap ( SCREEN_W, SCREEN_H );

   blit ( screen, scrnshot, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   get_palette ( pal );


   save_bitmap (filename, scrnshot, pal );

   destroy_bitmap ( scrnshot );
}



#define NORTH  0
#define EAST   1
#define SOUTH  2
#define WEST   3

bool process_input ( s_camera *camera )
{  static int face = 0; //up 0, west 3

   int code = readkey() >> 8;

   switch ( code )
   {
      case KEY_UP:
      {  switch ( face )
         {  case NORTH:
               camera->pos[Z_INDEX] -= (LENGTH/2);
            break;
         case EAST:
            camera->pos[X_INDEX] += (LENGTH/2);
            break;
         case SOUTH:
            camera->pos[Z_INDEX] += (LENGTH/2);
            break;
         case WEST:
             camera->pos[X_INDEX] -= (LENGTH/2);
            break;
         }
      }
      break;

      case KEY_LEFT: face = modulo(face -1, 4);
      break;

      case KEY_RIGHT: face = modulo(face +1, 4);
      break;

      case KEY_ESC: return false;
   }

   switch ( face )
   {  case NORTH:
         camera->dirv[Z_INDEX] = -1;
         camera->dirv[X_INDEX] = 0;
      break;
      case EAST:
         camera->dirv[Z_INDEX] = 0;
         camera->dirv[X_INDEX] = 1;
         break;
      case SOUTH:
         camera->dirv[Z_INDEX] = 1;
         camera->dirv[X_INDEX] = 0;
         break;
      case WEST:
         camera->dirv[Z_INDEX] = 0;
         camera->dirv[X_INDEX] = -1;
         break;
   }


   return true;
}


void camera_to_matrix ( const s_camera *camera, MATRIX_f *matrix )
{  get_camera_matrix_f( matrix,
                        camera->pos[X_INDEX],
                        camera->pos[Y_INDEX],
                        camera->pos[Z_INDEX],
                        camera->dirv[X_INDEX],
                        camera->dirv[Y_INDEX],
                        camera->dirv[Z_INDEX],
                        camera->upv[X_INDEX],
                        camera->upv[Y_INDEX],
                        camera->upv[Z_INDEX],
                        camera->fov,
                        camera->aspect );
}

void init_camera ( s_camera *camera )
{
   camera->pos[X_INDEX] = (LENGTH/2);
   camera->pos[Y_INDEX] = 5; //value must be adjuted manually
   camera->pos[Z_INDEX] = -(LENGTH/2);
   camera->dirv[X_INDEX] = 0;
   camera->dirv[Y_INDEX] = 0;
   camera->dirv[Z_INDEX] = -1;
   camera->upv[X_INDEX] = 0;
   camera->upv[Y_INDEX] = 1;
   camera->upv[Z_INDEX] = 0;
   camera->fov = 64;
   camera->aspect = (float)SCREEN_W / (float)SCREEN_H;
   //camera->aspect = 1;
}

int demo_maze ( void )
{  if ( init() == INIT_SUCCESS )
   {
      set_projection_viewport (0, 0, SCREEN_W, SCREEN_H);

      MATRIX_f matrix;
      s_camera camera;
      init_camera( &camera );
      BITMAP *texture = load_bitmap("stonewall.bmp", NULL);

      ZBUFFER *zbuffer = create_zbuffer(screen);
      set_zbuffer( zbuffer );

      do {
         //slow, would need an extra temp vertex
         printf ("before create polygons\n");
         create_polygons ();
         printf ("before create lines\n");
         //create_lines();
         printf ("before clear bitmap\n");
         clear_bitmap( screen );
         clear_zbuffer(zbuffer, 0.);

         printf ("before camera to matrix\n");
         camera_to_matrix( &camera, &matrix);

         printf ("before project lines\n");
         //project_lines( &matrix);

         printf ("before project polygons\n");
         project_polygons( &matrix );

         //draw_sprite ( screen, texture, 0, 0 );
//         textout_centre_ex( screen, font, "Hello", 320, 240,
//                           makecol (255, 255, 255), makecol (0, 0, 0) );
         printf ("before draw lines\n");
         //draw_lines();
         printf ("before draw polygons\n");
         draw_polygons( texture );


      }
      while ( process_input( &camera) );

      make_screen_shot("output.bmp");

      destroy_zbuffer(zbuffer);

      return 0;
   }

   return INIT_FAILURE;
}


//main to test the new engine
int new_engine ( mpr_s_maze *dummy_maze)
{  mpr_s_projection_room *room;

   if ( init() != INIT_SUCCESS ) return INIT_FAILURE;

   struct timeval stop, start; // get time delay
   char key_list[NB_INPUT_KEY] = { KEY_Z, KEY_ESC, KEY_Q, KEY_W, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, };
   unsigned char button_list [NB_INPUT_BUTTON] = {0, 1, 4, 5 };

   set_projection_viewport (0, 0, SCREEN_W, SCREEN_H);

   mpr_s_param params;
   params.aspect_ratio = (float)SCREEN_W / (float)SCREEN_H;
   params.field_of_view = 90;
   params.camera_height = 5;
   params.pull_back = 5;
   params.room_size = 9;
   params.wall_height = 10;
   params.wall_width = 10;

   BITMAP *buffer = create_bitmap( SCREEN_W, SCREEN_H);

   int error = mpr_init ( &room, &params, buffer );
   if ( error != INIT_SUCCESS ) return error;
   //printf ("DEBUG: Launched the init routine\n");

   mpr_s_position pos = {.x=0, .y=0, .z=0, .f=NORTH };

   mpr_e_action action = NO_ACTION;

   while ( action != EXIT_NAVIGATION )
   {
      mpr_s_update_result result = mpr_update (dummy_maze, &pos, action);
      gettimeofday(&start, NULL);
      mpr_render( room, dummy_maze, buffer, &pos);

      if ( result.hit_wall ) textprintf_ex( buffer, font, 0, 0, makecol(255, 255, 255), 0,
                                           "You hit a wall");
      blit ( buffer, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
      gettimeofday(&stop, NULL);


      action = mpr_process_input( key_list, button_list, 1, 3 );
   }

   make_screen_shot("output.bmp");

   //printf ("DEBUG: pass end 1\n");
   destroy_bitmap( buffer );
   //printf ("DEBUG: pass end 2\n");
   mpr_destroy( room );
   //printf ("DEBUG: pass end 3\n");
   mpr_destroy_maze( dummy_maze);

  //printf ("Final xy position: (%d,%d)\n", pos.x, pos.y );
printf("Run time %lu \n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);
   return 0;
}

int test_input ( void )
{

   if ( init() != INIT_SUCCESS ) return INIT_FAILURE;
   char key_list[NB_INPUT_KEY] = { KEY_Z, KEY_ESC, KEY_Q, KEY_W, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, };
   unsigned char button_list [NB_INPUT_BUTTON] = {0, 1, 4, 5 }; //according to my pink controller

   mpr_e_action action = -1;
   while ( action != EXIT_NAVIGATION )
   {  //printf ("DEBUG 0\n");
      action = mpr_process_input(key_list, button_list, 1, 3);
      //printf ("DEBUG 1\n");
      switch ( action )
      {  case TURN_LEFT:
            printf ("INPUT: turn left\n");
            break;
         case TURN_RIGHT:
            printf ("INPUT: turn right\n");
            break;
         case MOVE_FORWARD:
            printf ("INPUT: move forward\n");
            break;
         case MOVE_BACKWARD:
            printf ("INPUT: move backward\n");
            break;
         case USE_WALL:
            printf ("INPUT: use wall\n");
            break;
         case SHIFT_LEFT:
            printf ("INPUT: shift left\n");
            break;
         case SHIFT_RIGHT:
            printf ("INPUT: shift right\n");
            break;
         case EXIT_NAVIGATION:
            printf ("INPUT: exit\n");
            break;
         default:
            printf ("INPUT: Invalid actions were returned\n");
      }
   }

   return 0;
}


int main()
{  //return run_all_test();
   //return demo_maze();

   mpr_s_maze *dummy_maze = mpr_create_dummy_maze();
   if ( dummy_maze != NULL) return new_engine( dummy_maze);
   //return test_input();
}
END_OF_MAIN()

