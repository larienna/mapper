/**
   MAPPER
   Unit tests

   @author: Eric Pietrocupo
   @date: June 6th, 2021

   @license: Apache license 2.0

   This is a series of test suite registerng routines that will register each test individually.

   Make sure that the registry is initiallised correctly first. The run_all_test() method should
   take care of that and lauch all test suite registering functions.

*/

#ifndef UNITEST_H_INCLUDED
#define UNITEST_H_INCLUDED


void TS00_register_suite ( void );
void TS01_register_suite ( void );
int run_all_test ( void );





#endif // UNITEST_H_INCLUDED
