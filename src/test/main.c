/**
   MAPPER

	Main that launch all the unit tests

   @author: Eric Pietrocupo
   @date: June 27th, 2021

   @license: Apache license 2.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <allegro.h>
#include <stdbool.h>
#include <math.h>
#include <unitest.h>
#include <mazetile.h>
#include <mazefile.h>
#include <mprinput.h>
#include <mapper.h>
#include <sys/time.h>



int main()
{  return run_all_test();
}
END_OF_MAIN()

