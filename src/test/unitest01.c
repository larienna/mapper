/**
   MAPPER
   Unit test suite 01 : ???

   @author: Eric Pietrocupo
   @date: June 6th, 2021

   @license: Apache license 2.0
*/

#include <allegro.h>
#include <stdbool.h>
#include <CUnit/CUnit.h>
#include <stdlib.h>
#include <mazetile.h>
#include <mazefile.h>
#include <mprinput.h>
#include <mapper.h>
#include <mapper_private.h>
#include <mazetile.h>



int TS01_setup ( void )
{  return 0;
}

int TS01_teardown ( void )
{  return 0;
}

void TS01_mazetile_surface ( void )
{  s_mazetile tile;
   tile.surface[EASTWALL] = 0b00101000;

   set_mazetile_surface (&tile, EASTWALL, 0b00010000);
   CU_ASSERT_EQUAL ( tile.surface[EASTWALL], 0b00111000 );
   CU_ASSERT_TRUE ( is_mazetile_surface( &tile, EASTWALL, 0b00010000) );
   unset_mazetile_surface( &tile, EASTWALL, 0b00010000 );
   CU_ASSERT_EQUAL ( tile.surface[EASTWALL], 0b00101000 );
}

void TS01_mazetile_interact ( void )
{  s_mazetile tile;
   tile.interact[WESTWALL] = 0b00001010;

   set_mazetile_interact (&tile, WESTWALL, 0b00000100);
   CU_ASSERT_EQUAL ( tile.interact[WESTWALL], 0b00001110 );
   CU_ASSERT_TRUE ( is_mazetile_interact( &tile, WESTWALL, 0b00000100) );
   unset_mazetile_interact( &tile, WESTWALL, 0b00000100 );
   CU_ASSERT_EQUAL ( tile.interact[WESTWALL], 0b00001010 );
}

void TS01_mazetile_filling ( void )
{  s_mazetile tile;
   tile.filling = 0b11000000;

   set_mazetile_filling_color ( &tile, FILLING_COLOR_LIME );
   //printf ("DEBUG: after color %02x\n", tile.filling );
   CU_ASSERT_EQUAL ( tile.filling, 0b11001010 );
   CU_ASSERT_EQUAL ( get_mazetile_filling_color ( &tile ), FILLING_COLOR_LIME );
   set_mazetile_filling_density ( &tile, FILLING_DENSITY_GAS );
   //printf ("DEBUG: after density %02x\n", tile.filling );
   CU_ASSERT_EQUAL ( tile.filling, 0b11101010 );
   CU_ASSERT_EQUAL ( get_mazetile_filling_density ( &tile ), FILLING_DENSITY_GAS );
}

//this is not a legal unit test and it load and save file on disk
void TS01_save_load_mazefile ( void )
{  mpr_s_maze *maze_a = mpr_create_dummy_maze();
   mpr_save_mazefile( maze_a, "dummy.maz", COMPRESSION_NONE, MAZEFILE_V4_MAPPER_16b01 );

   mpr_s_maze *maze_b = mpr_load_mazefile("dummy.maz");

   CU_ASSERT_EQUAL( maze_a->depth, maze_b->depth);
   CU_ASSERT_EQUAL( maze_a->width, maze_b->width);

   for ( int k = 0 ; k < maze_a->depth; k++ )
   {  for ( int j = 0; j < maze_a->width; j++ )
      {  for ( int i = 0; i < maze_a->width; i++ )
         {  for ( int h = 0 ; h < NB_SURFACE; h++ )
            {  CU_ASSERT_EQUAL( maze_a->tile[k][j][i].surface[h], maze_b->tile[k][j][i].surface[h]);
               CU_ASSERT_EQUAL( maze_a->tile[k][j][i].interact[h], maze_b->tile[k][j][i].interact[h]);
            }
            CU_ASSERT_EQUAL( maze_a->tile[k][j][i].filling, maze_b->tile[k][j][i].filling);
            CU_ASSERT_EQUAL( maze_a->tile[k][j][i].palette, maze_b->tile[k][j][i].palette);
            CU_ASSERT_EQUAL( maze_a->tile[k][j][i].area, maze_b->tile[k][j][i].area);
            CU_ASSERT_EQUAL( maze_a->tile[k][j][i].reserved, maze_b->tile[k][j][i].reserved);
         }
      }
   }
}

void TS01_register_suite ( void )
{  CU_pSuite TS01 = CU_add_suite("01 Mazetile binary operation", TS01_setup, TS01_teardown);

   if (TS01 != NULL)
   {  CU_add_test (TS01, "Mazetile Surfaces", TS01_mazetile_surface );
      CU_add_test (TS01, "Mazetile Interaction", TS01_mazetile_interact );
      CU_add_test (TS01, "Mazetile Filling", TS01_mazetile_filling );
      CU_add_test (TS01, "Save Load mazefile", TS01_save_load_mazefile );


   }
}
