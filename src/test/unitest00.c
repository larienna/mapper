/**
   MAPPER

   Unit test suite 01 : Covers the projection room methods

   @author: Eric Pietrocupo
   @date: May 10th, 2021

   @license: Apache license 2.0
*/

#include <allegro.h>
#include <stdbool.h>
#include <mazetile.h>
#include <mazefile.h>
#include <mprinput.h>
#include <mapper.h>
#include <mapper_private.h>
#include <CUnit/CUnit.h>
#include <math.h>
#include <array.h>
#include <mazetile.h>

int TS00_setup ( void )
{  return 0;
}

int TS00_teardown ( void )
{  return 0;
}

void TS00_light_distance ( void )
{  mpr_s_projection_room room;

   room.wall_width = 12;
   room.pull_back = 3;
   room.size = 9;

   float distance = _calculate_distance ( &room, 7, 6 );
   //printf("\nTEST Distance=%f", distance );
   CU_ASSERT_DOUBLE_EQUAL(  distance, 37.58, 0.05 );
   float max_distance = _calculate_distance( &room, room.size, room.size );
   //printf("\nTEST Max Distance=%f", max_distance );
   CU_ASSERT_DOUBLE_EQUAL(  max_distance, 78.51, 0.05 );
   CU_ASSERT_EQUAL( _convert_distance_to_light( distance, max_distance), 140 );

}

void TS00_modulo ( void )
{  CU_ASSERT_EQUAL ( modulo( 13, 5), 3 );
   CU_ASSERT_EQUAL ( modulo( 2, 5), 2 );
   CU_ASSERT_EQUAL ( modulo( -16, 5), 4 );
   CU_ASSERT_EQUAL ( modulo( 13, -5), 3 );
}

/** Making spot checks to see if the generated nodes makes sense
*/
void TS00_build_nodes ( void )
{  mpr_s_projection_room room;
   room.wall_width = 10;
   room.wall_height = 8;
   room.size = 9;
   int node_room_size = room.size + 1;
   V3D_f ***node = node = (V3D_f***) allocate_3D_dynamic_array( sizeof (V3D_f),
                                                               NODE_HEIGHT,
                                                               node_room_size,
                                                               node_room_size);
   _build_nodes( &room, node);
   V3D_f test_node_00 = {.x=0, .y=0, .z=0 };
   CU_ASSERT_EQUAL( node[0][0][0].x, test_node_00.x);
   CU_ASSERT_EQUAL( node[0][0][0].y, test_node_00.y);
   CU_ASSERT_EQUAL( node[0][0][0].z, test_node_00.z);

   V3D_f test_node_01 = {.x=30, .y=0, .z=-10 };
   CU_ASSERT_EQUAL( node[0][1][3].x, test_node_01.x);
   CU_ASSERT_EQUAL( node[0][1][3].y, test_node_01.y);
   CU_ASSERT_EQUAL( node[0][1][3].z, test_node_01.z);

   V3D_f test_node_02 = {.x=20, .y=8, .z=-20 };
   CU_ASSERT_EQUAL( node[1][2][2].x, test_node_02.x);
   CU_ASSERT_EQUAL( node[1][2][2].y, test_node_02.y);
   CU_ASSERT_EQUAL( node[1][2][2].z, test_node_02.z);

}

void TS00_build_polygon_wall ( void )
{  //creating a test tile
   s_tile tile;
   tile.polygon_count = 0;
   int light = makecol (255, 255, 255);
   tile.node[0][0][0] = (V3D_f){ .x=0, .y=0, .z=0, .u=0, .v=0, .c=light};
   tile.node[0][0][1] = (V3D_f){ .x=10, .y=0, .z=0, .u=0, .v=0, .c=light};
   tile.node[0][1][0] = (V3D_f){ .x=0, .y=0, .z=-10, .u=0, .v=0, .c=light};
   tile.node[0][1][1] = (V3D_f){ .x=10, .y=0, .z=-10, .u=0, .v=0, .c=light};
   tile.node[1][0][0] = (V3D_f){ .x=0, .y=10, .z=0, .u=0, .v=0, .c=light};
   tile.node[1][0][1] = (V3D_f){ .x=10, .y=10, .z=0, .u=0, .v=0, .c=light};
   tile.node[1][1][0] = (V3D_f){ .x=0, .y=10, .z=-10, .u=0, .v=0, .c=light};
   tile.node[1][1][1] = (V3D_f){ .x=10, .y=10, .z=-10, .u=0, .v=0, .c=light};

   //west wall test
   s_polygon testpoly_01;
   testpoly_01.type = POLYTYPE_PTEX; //TODO: Could change eventually
   testpoly_01.nb_vertex = 4;
   testpoly_01.texture = 0;
   testpoly_01.vertex[0] = (V3D_f){ .x=0, .y=0, .z=0, .u=0, .v=TEXTURE_SIZE, .c=light};
   testpoly_01.vertex[1] = (V3D_f){ .x=0, .y=10, .z=0, .u=0, .v=0, .c=light};
   testpoly_01.vertex[2] = (V3D_f){ .x=0, .y=10, .z=-10, .u=TEXTURE_SIZE, .v=0, .c=light};
   testpoly_01.vertex[3] = (V3D_f){ .x=0, .y=0, .z=-10, .u=TEXTURE_SIZE, .v=TEXTURE_SIZE, .c=light};

   _build_polygon_wall( &tile, WESTWALL);
   CU_ASSERT_TRUE( _compare_polygon( &testpoly_01, &tile.polygon[0] ));

}

void TS00_validate_parameters ( void )
{  mpr_s_param params = {  .wall_height = -50,
                           .wall_width = -25,
                           .camera_height = -10,
                           .field_of_view = -300,
                           .pull_back = 7,
                           .room_size = 28};

   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.wall_height = 10;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.wall_width = 12;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.camera_height = 11;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.camera_height = 5;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.field_of_view = 361;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.field_of_view = 90;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.pull_back = 6;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.room_size = 1;
   CU_ASSERT_FALSE ( _validate_parameters( &params ) );
   params.room_size = 17;
   CU_ASSERT_TRUE ( _validate_parameters( &params ) );
}

void TS00_compare_polygon_vertex( void )
{  // making spot check comparison
   V3D_f va = { .x=100, .y=50, .z=220, .u=0, .v=16, .c=0};
   V3D_f vb = { .x=100, .y=50, .z=225, .u=0, .v=15, .c=0};
   V3D_f vc = { .x=100, .y=50, .z=220, .u=0, .v=16, .c=0};

   s_polygon polya = { .nb_vertex=6, .type= POLYTYPE_FLAT, .texture=5,
                      .vertex= {va, vb, vc, va, vb, vc}};
   s_polygon polyb = { .nb_vertex=6, .type= POLYTYPE_ATEX, .texture=2,
                      .vertex= {vb, vc, vc, va, va, vc}};
   s_polygon polyc = { .nb_vertex=4, .type= POLYTYPE_FLAT, .texture=5,
                      .vertex= {va, vb, vc, va, vb, va}};
   s_polygon polyd = { .nb_vertex=6, .type= POLYTYPE_FLAT, .texture=5,
                      .vertex= {va, vb, vc, va, vb, vc}};

   CU_ASSERT_TRUE ( _compare_vertex( &va, &vc));
   CU_ASSERT_FALSE ( _compare_vertex( &va, &vb));
   CU_ASSERT_TRUE ( _compare_polygon( &polya, &polyd));
   CU_ASSERT_FALSE ( _compare_polygon( &polya, &polyb));
   CU_ASSERT_FALSE ( _compare_polygon( &polyb, &polyc));
   CU_ASSERT_FALSE ( _compare_polygon( &polya, &polyc));
}

void TS00_rotate_facing ( void )
{  CU_ASSERT_EQUAL ( _left_facing_of ( NORTH ), WEST );
   CU_ASSERT_EQUAL ( _left_facing_of ( EAST ),  NORTH );
   CU_ASSERT_EQUAL ( _left_facing_of ( SOUTH ), EAST );
   CU_ASSERT_EQUAL ( _left_facing_of ( WEST ), SOUTH );
   CU_ASSERT_EQUAL ( _right_facing_of ( NORTH ), EAST );
   CU_ASSERT_EQUAL ( _right_facing_of ( EAST ), SOUTH );
   CU_ASSERT_EQUAL ( _right_facing_of ( SOUTH ), WEST );
   CU_ASSERT_EQUAL ( _right_facing_of ( WEST ), NORTH );
   CU_ASSERT_EQUAL ( _back_facing_of ( NORTH ), SOUTH );
   CU_ASSERT_EQUAL ( _back_facing_of ( EAST ), WEST );
   CU_ASSERT_EQUAL ( _back_facing_of ( SOUTH ), NORTH );
   CU_ASSERT_EQUAL ( _back_facing_of ( WEST ), EAST );
}

void TS00_position_change_movement ( void )
{  mpr_s_position pos = {.x=10, .y=10, .z=1, .f=NORTH};

   _move_in_direction ( &pos, pos.f );
   CU_ASSERT_EQUAL ( pos.y, 11 );
   pos.f = _right_facing_of( pos.f);
   _move_in_direction( &pos, _back_facing_of(pos.f));
   CU_ASSERT_EQUAL ( pos.x, 9 );
}

void TS00_simulate_maze_movement ( void )
{  mpr_s_position pos = {.x=0, .y=0, .z=0, .f=NORTH};
   mpr_s_maze *maze = mpr_create_dummy_maze();

   mpr_update ( maze, &pos, MOVE_FORWARD );
   mpr_update ( maze, &pos, MOVE_FORWARD );
   mpr_update ( maze, &pos, TURN_RIGHT );
   mpr_update ( maze, &pos, MOVE_FORWARD );
   mpr_update ( maze, &pos, MOVE_FORWARD );
   mpr_update ( maze, &pos, TURN_LEFT );
   for ( int i = 0 ; i < 5; i++ ) mpr_update ( maze, &pos, MOVE_FORWARD );
   mpr_update ( maze, &pos, SHIFT_RIGHT );
   mpr_update ( maze, &pos, TURN_RIGHT );
   mpr_update ( maze, &pos, TURN_RIGHT );
   mpr_update ( maze, &pos, MOVE_BACKWARD );
   mpr_update ( maze, &pos, MOVE_BACKWARD );
   mpr_update ( maze, &pos, MOVE_BACKWARD ); //should hit a wall
   mpr_update ( maze, &pos, SHIFT_LEFT );

   CU_ASSERT_EQUAL ( pos.x, 4 );
   CU_ASSERT_EQUAL ( pos.y, 4 );
   CU_ASSERT_EQUAL ( pos.f, SOUTH );
}


void TS00_register_suite ( void )
{  CU_pSuite TS00 = CU_add_suite("00 Projection Room", TS00_setup, TS00_teardown);

   if (TS00 != NULL)
   {  CU_add_test (TS00, "Light Distance", TS00_light_distance );
      CU_add_test (TS00, "Modulo", TS00_modulo );
      CU_add_test (TS00, "Build Nodes", TS00_build_nodes );
      CU_add_test (TS00, "Build Polygon Walls", TS00_build_polygon_wall );
      CU_add_test (TS00, "Validate parameters", TS00_validate_parameters );
      CU_add_test (TS00, "Compare vertex / polygons", TS00_compare_polygon_vertex );
      CU_add_test (TS00, "Facing rotation", TS00_rotate_facing );
      CU_add_test (TS00, "Position change movement", TS00_position_change_movement );
      CU_add_test (TS00, "Simulate maze movement", TS00_simulate_maze_movement );
   }
}
